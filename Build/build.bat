CLS

call setup.bat

call %rootdir%\build\builddir.bat

call %rootdir%\build\fixver.bat

call %rootdir%\build\renfiles.bat

call %rootdir%\build\buildpas.bat

call %rootdir%\build\makeinst.bat

call %rootdir%\build\teardown.bat

cd %rootdir%\build

echo Build Complete!