rem Rebuilds and signs
rem
rem Example programs
rem
rem all applications are compiled with each version of delphi.
rem this is a simple last-ditch effor to catch any compiler
rem incompatibilies that might jump into the code.
rem

rem ======================================================EXMLPro - C++Builder
rem cd %rootdir%\Examples\CBuildr5\EXMLPro

rem del ..\*.dcu
rem del *.dcu
rem %bcb5bin%\make -f%rootdir%\Examples\Cbuildr5\EXMLPro\EXMLPro.mak
rem if ErrorLevel 1 pause

rem ===========================================================================



rem ======================================================EXMLPro - Delphi
cd %rootdir%\Examples\Delphi\EXMLPro
del ..\*.dcu
del *.dcu
%dcc7% -B -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -u%rootdir% -i%rootdir%; EXMLPro.dpr
if errorlevel 1 pause

del ..\*.dcu
del *.dcu
%dcc6% -B -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -u%rootdir% -i%rootdir%; EXMLPro.dpr
if errorlevel 1 pause

del ..\*.dcu
del *.dcu
%dcc4% -B -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -u%rootdir% -i%rootdir%; EXMLPro.dpr
if errorlevel 1 pause

del ..\*.dcu
del *.dcu
%dcc5% -B -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -u%rootdir% -i%rootdir%; EXMLPro.dpr
if errorlevel 1 pause

rem n:\builds\common\signcode EXMLPro.exe -spc n:\builds\credentials\tps.spc -v n:\builds\credentials\tps.pvk -n "XMLPartner Professional EXMLPro Editor" -i http://www.turbopower.com -t http://timestamp.verisign.com/scripts/timstamp.dll
rem n:\builds\common\chktrust.exe EXMLPro.exe -q
rem if errorlevel 1 pause

copy EXMLPro.exe %builddir%\EXMLPro.exe
j:\util\fileset %builddir%\EXMLPro.exe -t %timestr% -d %datestr%
rem ===========================================================================


rem
rem TODO:: No Filter
rem


rem ======================================================TextView - C++Builder
rem cd %rootdir%\Examples\CBuildr3\TextView

rem del ..\*.dcu
rem del *.dcu
rem %bcb3bin%\make -f%rootdir%\Examples\Cbuildr3\TextView\TextView.bpr
rem if ErrorLevel 1 pause

rem cd %rootdir%\Examples\CBuildr4\TextView

rem del ..\*.dcu
rem del *.dcu
rem %bcb4bin%\make -f%rootdir%\Examples\Cbuildr4\TextView\TextView.bpr
rem if ErrorLevel 1 pause

rem cd %rootdir%\Examples\CBuildr5\TextView

rem del ..\*.dcu
rem del *.dcu
rem %bcb5bin%\make -f%rootdir%\Examples\Cbuildr5\TextView\TextView.bpr
rem if ErrorLevel 1 pause

rem ===========================================================================



rem ======================================================TextView - Delphi
cd %rootdir%\Examples\Delphi\TextView

del ..\*.dcu
del *.dcu
%dcc7% -B -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -u%rootdir% -i%rootdir%; TextView.dpr
if errorlevel 1 pause

del ..\*.dcu
del *.dcu
%dcc6% -B -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -u%rootdir% -i%rootdir%; TextView.dpr
if errorlevel 1 pause

del ..\*.dcu
del *.dcu
%dcc3% -B -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -u%rootdir% -i%rootdir%; TextView.dpr
if errorlevel 1 pause

del ..\*.dcu
del *.dcu
%dcc4% -B -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -u%rootdir% -i%rootdir%; TextView.dpr
if errorlevel 1 pause

del ..\*.dcu
del *.dcu
%dcc5% -B -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -u%rootdir% -i%rootdir%; TextView.dpr
if errorlevel 1 pause

rem ===========================================================================


rem ======================================================XMLTest - C++Builder
rem cd %rootdir%\Examples\CBuildr3\XMLTest

rem del ..\*.dcu
rem del *.dcu
rem %bcb3bin%\make -f%rootdir%\Examples\Cbuildr3\XMLTest\EXMLTest.bpr
rem if ErrorLevel 1 pause

rem cd %rootdir%\Examples\CBuildr4\XMLTest

rem del ..\*.dcu
rem del *.dcu
rem %bcb4bin%\make -f%rootdir%\Examples\Cbuildr4\XMLTest\EXMLTest.bpr
rem if ErrorLevel 1 pause

rem cd %rootdir%\Examples\CBuildr5\XMLTest

rem del ..\*.dcu
rem del *.dcu
rem %bcb5bin%\make -f%rootdir%\Examples\Cbuildr5\XMLTest\EXMLTest.bpr
rem if ErrorLevel 1 pause

rem ===========================================================================



rem ======================================================XMLTest - Delphi
cd %rootdir%\Examples\Delphi\XMLTest

del ..\*.dcu
del *.dcu
%dcc7% -B -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -u%rootdir% -i%rootdir%; EXMLTest.dpr
if errorlevel 1 pause

del ..\*.dcu
del *.dcu
%dcc6% -B -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -u%rootdir% -i%rootdir%; EXMLTest.dpr
if errorlevel 1 pause

del ..\*.dcu
del *.dcu
%dcc3% -B -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -u%rootdir% -i%rootdir%; EXMLTest.dpr
if errorlevel 1 pause

del ..\*.dcu
del *.dcu
%dcc4% -B -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -u%rootdir% -i%rootdir%; EXMLTest.dpr
if errorlevel 1 pause

del ..\*.dcu
del *.dcu
%dcc5% -B -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -u%rootdir% -i%rootdir%; EXMLTest.dpr
if errorlevel 1 pause

rem ===========================================================================

