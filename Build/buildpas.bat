rem Copies all required source files over to a build directory. Files include
rem   PAS, DFM, STR, RC, RES, INC
rem .
rem Resources are the only things recompiled by this batch file
rem .
rem Packages are created by BUILDPKG
rem .
rem EXEs are created by BUILDEXE

cd %rootdir%
REM =================================================================================


REM ==========================================REDIST

echo REDIST directory
echo y | del %builddir%\REDIST\*.*

cd %rootdir%\REDIST
copy deploy.hlp %builddir%\redist


REM ================================================================================




REM ==========================================Example XML files
echo Example tables directory
echo y | del %builddir%\Examples\xmldocs
echo y | del %builddir%\Examples\customer

copy %rootdir%\Examples\xmldocs\*.* %builddir%\Examples\xmldocs
%tptool%\fileset %builddir%\Examples\xmldocs\*.* -t %timestr% -d %datestr%

copy %rootdir%\Examples\customer\*.* %builddir%\Examples\customer
%tptool%\fileset %builddir%\Examples\customer\*.* -t %timestr% -d %datestr%

REM =================================================================================



REM ==========================================Delphi Examples
echo Delphi examples directory
echo y | del %builddir%\Examples\Delphi\*.*

cd %rootdir%\Examples\Delphi\EXMLPro
echo y | del *.out
echo y | del *.htm
call %tptool%\cleanDFM\CleanDFM
copy %rootdir%\Examples\Delphi\EXMLPro\*.* %builddir%\Examples\Delphi\EXMLPro
%tptool%\fileset %builddir%\Examples\Delphi\EXMLPro\*.* -t %timestr% -d %datestr%

cd %rootdir%\Examples\Delphi\NoFilter
call %tptool%\cleanDFM\CleanDFM
copy %rootdir%\Examples\Delphi\NoFilter\*.* %builddir%\Examples\Delphi\NoFilter
%tptool%\fileset %builddir%\Examples\Delphi\NoFilter\*.* -t %timestr% -d %datestr%

cd %rootdir%\Examples\Delphi\TextView
call %tptool%\cleanDFM\CleanDFM
copy %rootdir%\Examples\Delphi\TextView\*.* %builddir%\Examples\Delphi\TextView
%tptool%\fileset %builddir%\Examples\Delphi\TextView\*.* -t %timestr% -d %datestr%

cd %rootdir%\Examples\Delphi\WrkOrder
call %tptool%\cleanDFM\CleanDFM
copy %rootdir%\Examples\Delphi\WrkOrder\*.* %builddir%\Examples\Delphi\WrkORder
%tptool%\fileset %builddir%\Examples\Delphi\WrkOrder\*.* -t %timestr% -d %datestr%

cd %rootdir%\Examples\Delphi\XMLTest
call %tptool%\cleanDFM\CleanDFM
copy %rootdir%\Examples\Delphi\XMLTest\*.* %builddir%\Examples\Delphi\XMLTest
%tptool%\fileset %builddir%\Examples\Delphi\XMLTest\*.* -t %timestr% -d %datestr%

cd %rootdir%\Examples\Delphi\XSLDemo
call %tptool%\cleanDFM\CleanDFM
copy %rootdir%\Examples\Delphi\XSLDemo\*.* %builddir%\Examples\Delphi\XSLDemo
%tptool%\fileset %builddir%\Examples\Delphi\XSLDemo\*.* -t %timestr% -d %datestr%

REM =================================================================================


REM ==========================================CBuilder 3 Examples
echo Delphi examples directory
echo y | del %builddir%\Examples\CBuildr3\*.*

cd %rootdir%\Examples\CBuildr3\TextView
call %tptool%\cleanDFM\CleanDFM
copy %rootdir%\Examples\CBuildr3\TextView\*.* %builddir%\Examples\CBuildr3\TextView
%tptool%\fileset %builddir%\Examples\CBuildr3\TextView\*.* -t %timestr% -d %datestr%

rem EXMLPro example is not available for BCB3

rem TODO: NoFilter example

cd %rootdir%\Examples\CBuildr3\XMLTest
call %tptool%\cleanDFM\CleanDFM
copy %rootdir%\Examples\CBuildr3\XMLTest\*.* %builddir%\Examples\CBuildr3\XMLTest
%tptool%\fileset %builddir%\Examples\CBuildr3\XMLTest\*.* -t %timestr% -d %datestr%

REM =================================================================================


REM ==========================================CBuilder 4 Examples
echo Delphi examples directory
echo y | del %builddir%\Examples\CBuildr4\*.*

cd %rootdir%\Examples\CBuildr4\EXMLPro
call %tptool%\cleanDFM\CleanDFM
copy %rootdir%\Examples\CBuildr4\EXMLPro\*.* %builddir%\Examples\CBuildr4\EXMLPro
%tptool%\fileset %builddir%\Examples\CBuildr4\EXMLPro\*.* -t %timestr% -d %datestr%

rem TODO: NoFilter example

cd %rootdir%\Examples\CBuildr4\TextView
call %tptool%\cleanDFM\CleanDFM
copy %rootdir%\Examples\CBuildr4\TextView\*.* %builddir%\Examples\CBuildr4\TextView
%tptool%\fileset %builddir%\Examples\CBuildr4\TextView\*.* -t %timestr% -d %datestr%

cd %rootdir%\Examples\CBuildr4\XMLTest
call %tptool%\cleanDFM\CleanDFM
copy %rootdir%\Examples\CBuildr4\XMLTest\*.* %builddir%\Examples\CBuildr4\XMLTest
%tptool%\fileset %builddir%\Examples\CBuildr4\XMLTest\*.* -t %timestr% -d %datestr%

REM =================================================================================


REM ==========================================CBuilder 5 Examples
echo Delphi examples directory
echo y | del %builddir%\Examples\CBuildr5\*.*

cd %rootdir%\Examples\CBuildr5\EXMLPro
call %tptool%\cleanDFM\CleanDFM
copy %rootdir%\Examples\CBuildr5\EXMLPro\*.* %builddir%\Examples\CBuildr5\EXMLPro
%tptool%\fileset %builddir%\Examples\CBuildr5\EXMLPro\*.* -t %timestr% -d %datestr%

rem TODO: NoFilter example

cd %rootdir%\Examples\CBuildr5\TextView
call %tptool%\cleanDFM\CleanDFM
copy %rootdir%\Examples\CBuildr5\TextView\*.* %builddir%\Examples\CBuildr5\TextView
%tptool%\fileset %builddir%\Examples\CBuildr5\TextView\*.* -t %timestr% -d %datestr%

cd %rootdir%\Examples\CBuildr5\XMLTest
call %tptool%\cleanDFM\CleanDFM
copy %rootdir%\Examples\CBuildr5\XMLTest\*.* %builddir%\Examples\CBuildr5\XMLTest
%tptool%\fileset %builddir%\Examples\CBuildr5\XMLTest\*.* -t %timestr% -d %datestr%

REM =================================================================================

REM ==========================================CBuilder 6 Examples
echo Delphi examples directory
echo y | del %builddir%\Examples\CBuildr6\*.*

cd %rootdir%\Examples\CBuildr6\EXMLPro
call %tptool%\cleanDFM\CleanDFM
copy %rootdir%\Examples\CBuildr6\EXMLPro\*.* %builddir%\Examples\CBuildr6\EXMLPro
%tptool%\fileset %builddir%\Examples\CBuildr6\EXMLPro\*.* -t %timestr% -d %datestr%

rem TODO: NoFilter example

cd %rootdir%\Examples\CBuildr6\TextView
call %tptool%\cleanDFM\CleanDFM
copy %rootdir%\Examples\CBuildr6\TextView\*.* %builddir%\Examples\CBuildr6\TextView
%tptool%\fileset %builddir%\Examples\CBuildr6\TextView\*.* -t %timestr% -d %datestr%

cd %rootdir%\Examples\CBuildr6\XMLTest
call %tptool%\cleanDFM\CleanDFM
copy %rootdir%\Examples\CBuildr6\XMLTest\*.* %builddir%\Examples\CBuildr6\XMLTest
%tptool%\fileset %builddir%\Examples\CBuildr6\XMLTest\*.* -t %timestr% -d %datestr%

REM =================================================================================


REM ==========================================DOCs and Help
echo DOC and Help directories
echo y | del %builddir%\HELP\Delphi\*.*
echo y | del %builddir%\HELP\CBuilder\*.*
echo y | del %builddir%\Docs\*.*

copy %rootdir%\help\delphi\XMLPRO.hlp %builddir%\help\delphi
copy %rootdir%\help\delphi\XMLPRO.cnt %builddir%\help\delphi
copy %rootdir%\help\cbuilder\XPROBCB.hlp %builddir%\help\cbuilder
copy %rootdir%\help\cbuilder\XPROBCB.cnt %builddir%\help\cbuilder
copy %rootdir%\docs\XPDPRO.pdf %builddir%\docs
copy %rootdir%\trialrun\XPRONP.PDF %builddir%\trialrun

%tptool%\fileset %builddir%\help\delphi\*.* -t %timestr% -d %datestr%
%tptool%\fileset %builddir%\help\cbuilder\*.* -t %timestr% -d %datestr%
REM =================================================================================


REM ==========================================Main \XP directory
echo main directory
echo y | del %builddir%\*.*
echo y | del %builddir%\dcu3\*.*
echo y | del %builddir%\dcu4\*.*
echo y | del %builddir%\dcu5\*.*
echo y | del %builddir%\dcu6vtr\*.*
echo y | del %builddir%\dcu6ctr\*.*
echo y | del %builddir%\dcu6\*.*
echo y | del %builddir%\hpp3\*.*
echo y | del %builddir%\obj3\*.*
echo y | del %builddir%\hpp3tr\*.*
echo y | del %builddir%\obj3tr\*.*
echo y | del %builddir%\hpp4\*.*
echo y | del %builddir%\obj4\*.*
echo y | del %builddir%\hpp4tr\*.*
echo y | del %builddir%\obj4tr\*.*
echo y | del %builddir%\hpp5\*.*
echo y | del %builddir%\obj5\*.*
echo y | del %builddir%\hpp5tr\*.*
echo y | del %builddir%\obj5tr\*.*
echo y | del %builddir%\hpp6\*.*
echo y | del %builddir%\hpp6vtr\*.*
echo y | del %builddir%\hpp6ctr\*.*
echo y | del %builddir%\obj6\*.*
echo y | del %builddir%\obj6vtr\*.*
echo y | del %builddir%\obj6ctr\*.*

cd %rootdir%
call %tptool%\cleanDFM\CleanDFM


call %rootdir%\build\BuildPKG.bat auto

call %rootdir%\build\BuildEXE.bat auto

del *.dcu

copy %rootdir%\trialrun\trialrun.hlp %builddir%\trialrun.hlp
copy %rootdir%\license.lic %builddir%
copy %rootdir%\license.pdf %builddir%
copy %rootdir%\XpAbout.pas %builddir%
copy %rootdir%\XpAbout.dfm %builddir%
copy %rootdir%\XpAbout.xfm %builddir%
copy %rootdir%\XpAboutw.pas %builddir%
copy %rootdir%\XpAboutw.dfm %builddir%
copy %rootdir%\XpBase.pas %builddir%
copy %rootdir%\XpChrFlt.pas %builddir%
copy %rootdir%\XpDefine.inc %builddir%
copy %rootdir%\XpDom.pas %builddir%
copy %rootdir%\XpExcept.pas %builddir%
copy %rootdir%\XpExcept.inc %builddir%
copy %rootdir%\XpHash.pas %builddir%
copy %rootdir%\XpInet.pas %builddir%
copy %rootdir%\XpParser.pas %builddir%
copy %rootdir%\XpReg.pas %builddir%
copy %rootdir%\XpReg.dcr %builddir%
copy %rootdir%\XpSort.pas %builddir%
copy %rootdir%\XpTrial.dfm %builddir%
copy %rootdir%\XpTrial.pas %builddir%
copy %rootdir%\XpXSLCon.pas %builddir%
copy %rootdir%\XpXSLMsg.inc %builddir%
copy %rootdir%\XpFltBas.inc %builddir%
copy %rootdir%\XpFltHTM.inc %builddir%
copy %rootdir%\XpFltPrt.inc %builddir%
copy %rootdir%\XpFltRTF.inc %builddir%
copy %rootdir%\XpFltXML.inc %builddir%
copy %rootdir%\XpFOHash.inc %builddir%
copy %rootdir%\XpXSLFO.inc %builddir%
copy %rootdir%\XpXSLPrc.inc %builddir%
copy %rootdir%\XpXSLT.inc %builddir%

{ CLX-specific files }
copy %rootdir%\XpQReg.pas %builddir%
copy %rootdir%\XpQReg.dcr %builddir%
copy %rootdir%\XpQFlBas.pas %builddir%
copy %rootdir%\XpQFlHTM.dcr %builddir%
copy %rootdir%\XpQFlHTM.pas %builddir%
copy %rootdir%\XpQFlPrt.dcr %builddir%
copy %rootdir%\XpQFlPrt.pas %builddir%
copy %rootdir%\XpQFlRTF.dcr %builddir%
copy %rootdir%\XpQFlRTF.pas %builddir%
copy %rootdir%\XpQFlXML.dcr %builddir%
copy %rootdir%\XpQFlXML.pas %builddir%
copy %rootdir%\XpQFOHsh.pas %builddir%
copy %rootdir%\XpQXSLFO.pas %builddir%
copy %rootdir%\XpQXSLPr.dcr %builddir%
copy %rootdir%\XpQXSLPr.pas %builddir%
copy %rootdir%\XpQXSLT.pas %builddir%
copy %rootdir%\XpQTrial.pas %builddir%
copy %rootdir%\XpQTrial.dfm %builddir%


{ VCL-specific files }
copy %rootdir%\XpvFlBas.pas %builddir%
copy %rootdir%\XpvFlHTM.dcr %builddir%
copy %rootdir%\XpvFlHTM.pas %builddir%
copy %rootdir%\XpvFlPrt.dcr %builddir%
copy %rootdir%\XpvFlPrt.pas %builddir%
copy %rootdir%\XpvFlRTF.dcr %builddir%
copy %rootdir%\XpvFlRTF.pas %builddir%
copy %rootdir%\XpvFlXML.dcr %builddir%
copy %rootdir%\XpvFlXML.pas %builddir%
copy %rootdir%\XpvFOHsh.pas %builddir%
copy %rootdir%\XpvXSLFO.pas %builddir%
copy %rootdir%\XpvXSLPr.dcr %builddir%
copy %rootdir%\XpvXSLPr.pas %builddir%
copy %rootdir%\XpvXSLT.pas %builddir%

REM Copy readme.hlp
copy %rootdir%\readme.hlp %builddir%
copy %rootdir%\readme.cnt %builddir%
copy %rootdir%\INETWH32.dll %builddir%

REM Copy Packages
copy %rootdir%\X%verstr%PD30.res %builddir%
copy %rootdir%\X%verstr%PD30.rc %builddir%
copy %rootdir%\X%verstr%PD30.dpk %builddir%
copy %rootdir%\X%verstr%PD30.dpl %builddir%
copy %rootdir%\X%verstr%PD30.dcp %builddir%

copy %rootdir%\X%verstr%PR30.res %builddir%
copy %rootdir%\X%verstr%PR30.rc %builddir%
copy %rootdir%\X%verstr%PR30.dpk %builddir%
copy %rootdir%\X%verstr%PR30.dpl %builddir%
copy %rootdir%\X%verstr%PR30.dcp %builddir%

copy %rootdir%\X%verstr%PT30.res %builddir%
copy %rootdir%\X%verstr%PT30.rc %builddir%
copy %rootdir%\X%verstr%PT30.dpk %builddir%
copy %rootdir%\X%verstr%PT30.dpl %builddir%
copy %rootdir%\X%verstr%PT30.dcp %builddir%

copy %rootdir%\X%verstr%PD35.res %builddir%
copy %rootdir%\X%verstr%PD35.rc %builddir%
copy %rootdir%\X%verstr%PD35.bpk %builddir%
copy %rootdir%\X%verstr%PD35.bpl %builddir%
copy %rootdir%\X%verstr%PD35.bpi %builddir%
copy %rootdir%\X%verstr%PD35.cpp %builddir%
copy %rootdir%\X%verstr%PD35.lib %builddir%

copy %rootdir%\X%verstr%PR35.res %builddir%
copy %rootdir%\X%verstr%PR35.rc %builddir%
copy %rootdir%\X%verstr%PR35.bpk %builddir%
copy %rootdir%\X%verstr%PR35.bpl %builddir%
copy %rootdir%\X%verstr%PR35.bpi %builddir%
copy %rootdir%\X%verstr%PR35.cpp %builddir%
copy %rootdir%\X%verstr%PR35.lib %builddir%

copy %rootdir%\X%verstr%PT35.res %builddir%
copy %rootdir%\X%verstr%PT35.rc %builddir%
copy %rootdir%\X%verstr%PT35.bpk %builddir%
copy %rootdir%\X%verstr%PT35.bpl %builddir%
copy %rootdir%\X%verstr%PT35.bpi %builddir%
copy %rootdir%\X%verstr%PT35.cpp %builddir%
copy %rootdir%\X%verstr%PT35.lib %builddir%

copy %rootdir%\X%verstr%PD40.res %builddir%
copy %rootdir%\X%verstr%PD40.rc %builddir%
copy %rootdir%\X%verstr%PD40.dpk %builddir%
copy %rootdir%\X%verstr%PD40.bpl %builddir%
copy %rootdir%\X%verstr%PD40.dcp %builddir%

copy %rootdir%\X%verstr%PR40.res %builddir%
copy %rootdir%\X%verstr%PR40.rc %builddir%
copy %rootdir%\X%verstr%PR40.dpk %builddir%
copy %rootdir%\X%verstr%PR40.bpl %builddir%
copy %rootdir%\X%verstr%PR40.dcp %builddir%

copy %rootdir%\X%verstr%PT40.res %builddir%
copy %rootdir%\X%verstr%PT40.rc %builddir%
copy %rootdir%\X%verstr%PT40.dpk %builddir%
copy %rootdir%\X%verstr%PT40.bpl %builddir%
copy %rootdir%\X%verstr%PT40.dcp %builddir%

copy %rootdir%\X%verstr%PD41.res %builddir%
copy %rootdir%\X%verstr%PD41.rc %builddir%
copy %rootdir%\X%verstr%PD41.bpk %builddir%
copy %rootdir%\X%verstr%PD41.bpl %builddir%
copy %rootdir%\X%verstr%PD41.bpi %builddir%
copy %rootdir%\X%verstr%PD41.cpp %builddir%
copy %rootdir%\X%verstr%PD41.lib %builddir%

copy %rootdir%\X%verstr%PR41.res %builddir%
copy %rootdir%\X%verstr%PR41.rc %builddir%
copy %rootdir%\X%verstr%PR41.bpk %builddir%
copy %rootdir%\X%verstr%PR41.bpl %builddir%
copy %rootdir%\X%verstr%PR41.bpi %builddir%
copy %rootdir%\X%verstr%PR41.cpp %builddir%
copy %rootdir%\X%verstr%PR41.lib %builddir%

copy %rootdir%\X%verstr%PT41.res %builddir%
copy %rootdir%\X%verstr%PT41.rc %builddir%
copy %rootdir%\X%verstr%PT41.bpk %builddir%
copy %rootdir%\X%verstr%PT41.bpl %builddir%
copy %rootdir%\X%verstr%PT41.bpi %builddir%
copy %rootdir%\X%verstr%PT41.cpp %builddir%
copy %rootdir%\X%verstr%PT41.lib %builddir%

copy %rootdir%\X%verstr%PD50.res %builddir%
copy %rootdir%\X%verstr%PD50.rc %builddir%
copy %rootdir%\X%verstr%PD50.dpk %builddir%
copy %rootdir%\X%verstr%PD50.bpl %builddir%
copy %rootdir%\X%verstr%PD50.dcp %builddir%

copy %rootdir%\X%verstr%PR50.res %builddir%
copy %rootdir%\X%verstr%PR50.rc %builddir%
copy %rootdir%\X%verstr%PR50.dpk %builddir%
copy %rootdir%\X%verstr%PR50.bpl %builddir%
copy %rootdir%\X%verstr%PR50.dcp %builddir%

copy %rootdir%\X%verstr%PT50.res %builddir%
copy %rootdir%\X%verstr%PT50.rc %builddir%
copy %rootdir%\X%verstr%PT50.dpk %builddir%
copy %rootdir%\X%verstr%PT50.bpl %builddir%
copy %rootdir%\X%verstr%PT50.dcp %builddir%

copy %rootdir%\X%verstr%PD51.res %builddir%
copy %rootdir%\X%verstr%PD51.rc %builddir%
copy %rootdir%\X%verstr%PD51.mak %builddir%
copy %rootdir%\X%verstr%PD51.bpk %builddir%
copy %rootdir%\X%verstr%PD51.bpl %builddir%
copy %rootdir%\X%verstr%PD51.bpi %builddir%
copy %rootdir%\X%verstr%PD51.cpp %builddir%
copy %rootdir%\X%verstr%PD51.lib %builddir%

copy %rootdir%\X%verstr%PR51.res %builddir%
copy %rootdir%\X%verstr%PR51.rc %builddir%
copy %rootdir%\X%verstr%PR51.mak %builddir%
copy %rootdir%\X%verstr%PR51.bpk %builddir%
copy %rootdir%\X%verstr%PR51.bpl %builddir%
copy %rootdir%\X%verstr%PR51.bpi %builddir%
copy %rootdir%\X%verstr%PR51.cpp %builddir%
copy %rootdir%\X%verstr%PR51.lib %builddir%

copy %rootdir%\X%verstr%PT51.res %builddir%
copy %rootdir%\X%verstr%PT51.rc %builddir%
copy %rootdir%\X%verstr%PT51.mak %builddir%
copy %rootdir%\X%verstr%PT51.bpk %builddir%
copy %rootdir%\X%verstr%PT51.bpl %builddir%
copy %rootdir%\X%verstr%PT51.bpi %builddir%
copy %rootdir%\X%verstr%PT51.cpp %builddir%
copy %rootdir%\X%verstr%PT51.lib %builddir%

copy %rootdir%\X%verstr%PD60.res %builddir%
copy %rootdir%\X%verstr%PD60.rc %builddir%
copy %rootdir%\X%verstr%PD60.dpk %builddir%
copy %rootdir%\X%verstr%PD60.bpl %builddir%
copy %rootdir%\X%verstr%PD60.dcp %builddir%

copy %rootdir%\X%verstr%PR60.res %builddir%
copy %rootdir%\X%verstr%PR60.rc %builddir%
copy %rootdir%\X%verstr%PR60.dpk %builddir%
copy %rootdir%\X%verstr%PR60.bpl %builddir%
copy %rootdir%\X%verstr%PR60.dcp %builddir%

copy %rootdir%\X%verstr%PT60.res %builddir%
copy %rootdir%\X%verstr%PT60.rc %builddir%
copy %rootdir%\X%verstr%PT60.dpk %builddir%
copy %rootdir%\X%verstr%PT60.bpl %builddir%
copy %rootdir%\X%verstr%PT60.dcp %builddir%

copy %rootdir%\X%verstr%PD61.res %builddir%
copy %rootdir%\X%verstr%PD61.rc %builddir%
copy %rootdir%\X%verstr%PD61.mak %builddir%
copy %rootdir%\X%verstr%PD61.bpk %builddir%
copy %rootdir%\X%verstr%PD61.bpl %builddir%
copy %rootdir%\X%verstr%PD61.bpi %builddir%
copy %rootdir%\X%verstr%PD61.cpp %builddir%
copy %rootdir%\X%verstr%PD61.lib %builddir%

copy %rootdir%\X%verstr%PR61.res %builddir%
copy %rootdir%\X%verstr%PR61.rc %builddir%
copy %rootdir%\X%verstr%PR61.mak %builddir%
copy %rootdir%\X%verstr%PR61.bpk %builddir%
copy %rootdir%\X%verstr%PR61.bpl %builddir%
copy %rootdir%\X%verstr%PR61.bpi %builddir%
copy %rootdir%\X%verstr%PR61.cpp %builddir%
copy %rootdir%\X%verstr%PR61.lib %builddir%

copy %rootdir%\X%verstr%PT61.res %builddir%
copy %rootdir%\X%verstr%PT61.rc %builddir%
copy %rootdir%\X%verstr%PT61.mak %builddir%
copy %rootdir%\X%verstr%PT61.bpk %builddir%
copy %rootdir%\X%verstr%PT61.bpl %builddir%
copy %rootdir%\X%verstr%PT61.bpi %builddir%
copy %rootdir%\X%verstr%PT61.cpp %builddir%
copy %rootdir%\X%verstr%PT61.lib %builddir%

copy %rootdir%\X%verstr%PD70.res %builddir%
copy %rootdir%\X%verstr%PD70.rc %builddir%
copy %rootdir%\X%verstr%PD70.dpk %builddir%
copy %rootdir%\X%verstr%PD70.bpl %builddir%
copy %rootdir%\X%verstr%PD70.dcp %builddir%

copy %rootdir%\X%verstr%PR70.res %builddir%
copy %rootdir%\X%verstr%PR70.rc %builddir%
copy %rootdir%\X%verstr%PR70.dpk %builddir%
copy %rootdir%\X%verstr%PR70.bpl %builddir%
copy %rootdir%\X%verstr%PR70.dcp %builddir%

rem CLX packages

copy %rootdir%\X%verstr%QD60.res %builddir%
copy %rootdir%\X%verstr%QD60.rc %builddir%
copy %rootdir%\X%verstr%QD60.dpk %builddir%
copy %rootdir%\X%verstr%QD60.bpl %builddir%
copy %rootdir%\X%verstr%QD60.dcp %builddir%

copy %rootdir%\X%verstr%QR60.res %builddir%
copy %rootdir%\X%verstr%QR60.rc %builddir%
copy %rootdir%\X%verstr%QR60.dpk %builddir%
copy %rootdir%\X%verstr%QR60.bpl %builddir%
copy %rootdir%\X%verstr%QR60.dcp %builddir%

copy %rootdir%\X%verstr%QT60.res %builddir%
copy %rootdir%\X%verstr%QT60.rc %builddir%
copy %rootdir%\X%verstr%QT60.dpk %builddir%
copy %rootdir%\X%verstr%QT60.bpl %builddir%
copy %rootdir%\X%verstr%QT60.dcp %builddir%

copy %rootdir%\X%verstr%CR60.res %builddir%
copy %rootdir%\X%verstr%CR60.rc %builddir%
copy %rootdir%\X%verstr%CR60.dpk %builddir%
copy %rootdir%\X%verstr%CR60.bpl %builddir%
copy %rootdir%\X%verstr%CR60.dcp %builddir%

copy %rootdir%\X%verstr%QD61.res %builddir%
copy %rootdir%\X%verstr%QD61.rc %builddir%
copy %rootdir%\X%verstr%QD61.mak %builddir%
copy %rootdir%\X%verstr%QD61.bpk %builddir%
copy %rootdir%\X%verstr%QD61.bpl %builddir%
copy %rootdir%\X%verstr%QD61.bpi %builddir%
copy %rootdir%\X%verstr%QD61.cpp %builddir%
copy %rootdir%\X%verstr%QD61.lib %builddir%

copy %rootdir%\X%verstr%QR61.res %builddir%
copy %rootdir%\X%verstr%QR61.rc %builddir%
copy %rootdir%\X%verstr%QR61.mak %builddir%
copy %rootdir%\X%verstr%QR61.bpk %builddir%
copy %rootdir%\X%verstr%QR61.bpl %builddir%
copy %rootdir%\X%verstr%QR61.bpi %builddir%
copy %rootdir%\X%verstr%QR61.cpp %builddir%
copy %rootdir%\X%verstr%QR61.lib %builddir%

copy %rootdir%\X%verstr%QT61.res %builddir%
copy %rootdir%\X%verstr%QT61.rc %builddir%
copy %rootdir%\X%verstr%QT61.mak %builddir%
copy %rootdir%\X%verstr%QT61.bpk %builddir%
copy %rootdir%\X%verstr%QT61.bpl %builddir%
copy %rootdir%\X%verstr%QT61.bpi %builddir%
copy %rootdir%\X%verstr%QT61.cpp %builddir%
copy %rootdir%\X%verstr%QT61.lib %builddir%

copy %rootdir%\X%verstr%CR61.res %builddir%
copy %rootdir%\X%verstr%CR61.rc %builddir%
copy %rootdir%\X%verstr%CR61.MAK %builddir%
copy %rootdir%\X%verstr%CR61.bpK %builddir%
copy %rootdir%\X%verstr%CR61.bpL %builddir%
copy %rootdir%\X%verstr%CR61.bpI %builddir%
copy %rootdir%\X%verstr%CR61.CPp %builddir%
copy %rootdir%\X%verstr%CR61.lib %builddir%

copy %rootdir%\X%verstr%QD70.res %builddir%
copy %rootdir%\X%verstr%QD70.rc %builddir%
copy %rootdir%\X%verstr%QD70.dpk %builddir%
copy %rootdir%\X%verstr%QD70.bpl %builddir%
copy %rootdir%\X%verstr%QD70.dcp %builddir%

copy %rootdir%\X%verstr%QR70.res %builddir%
copy %rootdir%\X%verstr%QR70.rc %builddir%
copy %rootdir%\X%verstr%QR70.dpk %builddir%
copy %rootdir%\X%verstr%QR70.bpl %builddir%
copy %rootdir%\X%verstr%QR70.dcp %builddir%

copy %rootdir%\X%verstr%CR70.res %builddir%
copy %rootdir%\X%verstr%CR70.rc %builddir%
copy %rootdir%\X%verstr%CR70.dpk %builddir%
copy %rootdir%\X%verstr%CR70.bpl %builddir%
copy %rootdir%\X%verstr%CR70.dcp %builddir%

%tptool%\fileset %builddir%\*.* -t %timestr% -d %datestr%

copy %rootdir%\hpp3\*.* %builddir%\hpp3
copy %rootdir%\obj3\*.* %builddir%\obj3
%tptool%\fileset %builddir%\hpp3\*.* -t %timestr% -d %datestr%
%tptool%\fileset %builddir%\obj3\*.* -t %timestr% -d %datestr%

copy %rootdir%\hpp4\*.* %builddir%\hpp4
copy %rootdir%\obj4\*.* %builddir%\obj4
%tptool%\fileset %builddir%\hpp4\*.* -t %timestr% -d %datestr%
%tptool%\fileset %builddir%\obj4\*.* -t %timestr% -d %datestr%

copy %rootdir%\hpp5\*.* %builddir%\hpp5
copy %rootdir%\obj5\*.* %builddir%\obj5
%tptool%\fileset %builddir%\hpp5\*.* -t %timestr% -d %datestr%
%tptool%\fileset %builddir%\obj5\*.* -t %timestr% -d %datestr%

copy %rootdir%\hpp6\*.* %builddir%\hpp6
copy %rootdir%\obj6\*.* %builddir%\obj6
%tptool%\fileset %builddir%\hpp6\*.* -t %timestr% -d %datestr%
%tptool%\fileset %builddir%\obj6\*.* -t %timestr% -d %datestr%

rem
rem Trial run header files
rem

copy %rootdir%\dcu6vtr\*.* %builddir%\dcu6vtr
%tptool%\fileset %builddir%\dcu6vtr\*.* -t %timestr% -d %datestr%

copy %rootdir%\dcu6ctr\*.* %builddir%\dcu6ctr
%tptool%\fileset %builddir%\dcu6ctr\*.* -t %timestr% -d %datestr%

copy %rootdir%\hpp3tr\*.* %builddir%\hpp3tr
copy %rootdir%\obj3tr\*.* %builddir%\obj3tr
%tptool%\fileset %builddir%\hpp3tr\*.* -t %timestr% -d %datestr%

copy %rootdir%\hpp3tr\*.* %builddir%\hpp3tr
copy %rootdir%\obj3tr\*.* %builddir%\obj3tr
%tptool%\fileset %builddir%\hpp3tr\*.* -t %timestr% -d %datestr%

copy %rootdir%\hpp4tr\*.* %builddir%\hpp4tr
copy %rootdir%\obj4tr\*.* %builddir%\obj4tr
%tptool%\fileset %builddir%\hpp4tr\*.* -t %timestr% -d %datestr%

copy %rootdir%\hpp5tr\*.* %builddir%\hpp5tr
copy %rootdir%\obj5tr\*.* %builddir%\obj5tr
%tptool%\fileset %builddir%\hpp5tr\*.* -t %timestr% -d %datestr%

copy %rootdir%\hpp6vtr\*.* %builddir%\hpp6vtr
copy %rootdir%\obj6vtr\*.* %builddir%\obj6vtr
%tptool%\fileset %builddir%\hpp6vtr\*.* -t %timestr% -d %datestr%

copy %rootdir%\hpp6ctr\*.* %builddir%\hpp6ctr
copy %rootdir%\obj6ctr\*.* %builddir%\obj6ctr
%tptool%\fileset %builddir%\hpp6ctr\*.* -t %timestr% -d %datestr%

copy %rootdir%\dcu3\*.* %builddir%\dcu3
%tptool%\fileset %builddir%\dcu3\*.* -t %timestr% -d %datestr%

copy %rootdir%\dcu4\*.* %builddir%\dcu4
%tptool%\fileset %builddir%\dcu4\*.* -t %timestr% -d %datestr%

copy %rootdir%\dcu5\*.* %builddir%\dcu5
%tptool%\fileset %builddir%\dcu5\*.* -t %timestr% -d %datestr%

copy %rootdir%\dcu6\*.* %builddir%\dcu6
%tptool%\fileset %builddir%\dcu6\*.* -t %timestr% -d %datestr%
