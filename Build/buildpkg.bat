cd %rootdir%

REM Compile any and all resource files
for %%a in (X%verstr%*.rc) do %d5bin%\brcc32 %%a

REM Remove unnecessary properties
call %tptool%\cleanDFM\CleanDFM

REM ============================Compile Delphi 3 packages
del *.dcu
%dcc3% %rootdir%\x%verstr%pr30.DPK -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -N%rootdir% -E%rootdir% -O%rootdir%
if ErrorLevel 1 pause

%dcc3% %rootdir%\x%verstr%pd30.DPK -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -N%rootdir% -E%rootdir% -O%rootdir%
if ErrorLevel 1 pause

REM ==========================================Set up for Trial Run
set trialBuildFail=NO
copy %rootdir%\xpdefine.inc %rootdir%\xpdefine.xxx
copy %rootdir%\xpdefine.trl %rootdir%\xpdefine.inc
del %rootdir%\*.dcu

%dcc3% %rootdir%\x%verstr%pt30.DPK -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -N%rootdir% -B -E%rootdir% -O%rootdir%
if ErrorLevel 1 set trialBuildFail=YES

REM =========================================Restore xpdefine.INC
copy %rootdir%\xpdefine.xxx %rootdir%\xpdefine.inc
if %trialBuildFail%==YES pause

REM copy dcu files to appropriate directory
md %rootdir%\dcu3
echo y|del %rootdir%\dcu3\*.*
move *.dcu %rootdir%\dcu3

REM =======================================================


REM ============================Compile Delphi 4 packages
del *.dcu
%dcc4% %rootdir%\x%verstr%pr40.DPK -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -N%rootdir% -E%rootdir% -O%rootdir% -LN%rootdir%
if ErrorLevel 1 pause

%dcc4% %rootdir%\x%verstr%pd40.DPK -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -N%rootdir% -E%rootdir% -O%rootdir% -LN%rootdir%
if ErrorLevel 1 pause

REM =======================================================


REM ==========================================Set up for Trial Run
set trialBuildFail=NO
copy %rootdir%\xpdefine.inc %rootdir%\xpdefine.xxx
copy %rootdir%\xpdefine.trl %rootdir%\xpdefine.inc
del %rootdir%\*.dcu

%dcc4% %rootdir%\x%verstr%pt40.DPK -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -N%rootdir% -B -E%rootdir% -O%rootdir%
if ErrorLevel 1 set trialBuildFail=YES

REM =========================================Restore xpdefine.INC
copy %rootdir%\xpdefine.xxx %rootdir%\xpdefine.inc
if %trialBuildFail%==YES pause

REM copy dcu files to appropriate directory
md %rootdir%\dcu4
echo y|del %rootdir%\dcu4\*.*
move *.dcu %rootdir%\dcu4

REM =======================================================

REM ============================Compile Delphi 5 packages
del *.dcu

%dcc5% %rootdir%\x%verstr%pr50.DPK -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -N%rootdir% -E%rootdir% -O%rootdir% -LN%rootdir%
if ErrorLevel 1 pause

%dcc5% %rootdir%\x%verstr%pd50.DPK -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -N%rootdir% -E%rootdir% -O%rootdir% -LN%rootdir%
if ErrorLevel 1 pause

REM ==========================================Set up for Trial Run
set trialBuildFail=NO
copy %rootdir%\xpdefine.inc %rootdir%\xpdefine.xxx
copy %rootdir%\xpdefine.trl %rootdir%\xpdefine.inc
del %rootdir%\*.dcu

%dcc5% %rootdir%\x%verstr%pt50.DPK -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -N%rootdir% -B -E%rootdir% -O%rootdir%
if ErrorLevel 1 set trialBuildFail=YES

REM =========================================Restore xpdefine.INC
copy %rootdir%\xpdefine.xxx %rootdir%\xpdefine.inc
if %trialBuildFail%==YES pause

REM copy dcu files to appropriate directory
md %rootdir%\dcu5
echo y|del %rootdir%\dcu5\*.*
move *.dcu %rootdir%\dcu5

REM =======================================================


REM ============================Compile Delphi 6 packages
del *.dcu

%dcc6% %rootdir%\X%verstr%CR60.DPK -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -N%rootdir% -E%rootdir% -O%rootdir% -LN%rootdir% -LE%rootdir% -B
if ErrorLevel 1 pause

%dcc6% %rootdir%\X%verstr%PR60.DPK -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -N%rootdir% -E%rootdir% -O%rootdir% -LN%rootdir% -LE%rootdir% -B
if ErrorLevel 1 pause

%dcc6% %rootdir%\X%verstr%PD60.DPK -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -N%rootdir% -E%rootdir% -O%rootdir% -LN%rootdir% -LE%rootdir% -B
if ErrorLevel 1 pause

%dcc6% %rootdir%\X%verstr%QR60.DPK -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -N%rootdir% -E%rootdir% -O%rootdir% -LN%rootdir% -LE%rootdir% -B -DUsingCLX
if ErrorLevel 1 pause

%dcc6% %rootdir%\X%verstr%QD60.DPK -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -N%rootdir% -E%rootdir% -O%rootdir% -LN%rootdir% -LE%rootdir% -B -DUsingCLX
if ErrorLevel 1 pause

REM ==========================================Set up for Trial Run
set trialBuildFail=NO
copy %rootdir%\xpdefine.inc %rootdir%\xpdefine.xxx
copy %rootdir%\xpdefine.trl %rootdir%\xpdefine.inc
del %rootdir%\*.dcu

%dcc6% %rootdir%\x%verstr%PT60.DPK -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -N%rootdir% -B -E%rootdir% -O%rootdir%  -LN%rootdir% -LE%rootdir% -B
if ErrorLevel 1 set trialBuildFail=YES

REM copy dcu files to appropriate directory
md %rootdir%\dcu6vtr
echo y|del %rootdir%\dcu6vtr\*.*
move *.dcu %rootdir%\dcu6vtr

%dcc6% %rootdir%\x%verstr%QT60.DPK -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -N%rootdir% -B -E%rootdir% -O%rootdir%  -LN%rootdir% -LE%rootdir% -B -DUsingCLX
if ErrorLevel 1 set trialBuildFail=YES

REM =========================================Restore xpdefine.INC
copy %rootdir%\xpdefine.xxx %rootdir%\xpdefine.inc
if %trialBuildFail%==YES pause

REM copy dcu files to appropriate directory
md %rootdir%\dcu6ctr
echo y|del %rootdir%\dcu6ctr\*.*
move *.dcu %rootdir%\dcu6ctr

REM =======================================================


REM ============================Compile Delphi 7 packages
del *.dcu

%dcc7% %rootdir%\X%verstr%CR70.DPK -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -N%rootdir% -E%rootdir% -O%rootdir% -LN%rootdir% -LE%rootdir% -B
if ErrorLevel 1 pause

%dcc7% %rootdir%\X%verstr%PR70.DPK -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -N%rootdir% -E%rootdir% -O%rootdir% -LN%rootdir% -LE%rootdir% -B
if ErrorLevel 1 pause

%dcc7% %rootdir%\X%verstr%PD70.DPK -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -N%rootdir% -E%rootdir% -O%rootdir% -LN%rootdir% -LE%rootdir% -B
if ErrorLevel 1 pause

%dcc7% %rootdir%\X%verstr%QR70.DPK -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -N%rootdir% -E%rootdir% -O%rootdir% -LN%rootdir% -LE%rootdir% -B -DUsingCLX
if ErrorLevel 1 pause 

%dcc7% %rootdir%\X%verstr%QD70.DPK -$C- -$D- -$L- -$O+ -$Q- -$R- -$Y- -N%rootdir% -E%rootdir% -O%rootdir% -LN%rootdir% -LE%rootdir% -B -DUsingCLX
if ErrorLevel 1 pause



REM =======================================================


REM ============================Compile Builder 3 packages
del *.dcu
del *.hpp
del *.obj

%bcb3bin%\make -B -f%rootdir%\x%verstr%pr35.BPK
if ErrorLevel 1 pause

%bcb3bin%\make -B -f%rootdir%\x%verstr%pd35.BPK
if ErrorLevel 1 pause

md %rootdir%\hpp3
md %rootdir%\obj3
echo y|del %rootdir%\hpp3\*.*
echo y|del %rootdir%\obj3\*.*
move *.hpp %rootdir%\hpp3
move *.obj %rootdir%\obj3

REM ==========================================Set up for Trial Run
set trialBuildFail=NO
copy %rootdir%\xpdefine.inc %rootdir%\xpdefine.xxx
copy %rootdir%\xpdefine.trl %rootdir%\xpdefine.inc
del %rootdir%\*.dcu
del %rootdir%\*.hpp
del %rootdir%\*.obj

%bcb3bin%\make -B -f%rootdir%\x%verstr%pt35.BPK
if ErrorLevel 1 set trialBuildFail=YES

REM =========================================Restore xpdefine.INC
copy %rootdir%\xpdefine.xxx %rootdir%\xpdefine.inc
if %trialBuildFail%==YES pause

  REM copy header files to appropriate directory
  md %rootdir%\hpp3tr
  md %rootdir%\obj3tr
  echo y|del %rootdir%\hpp3tr\*.*
  echo y|del %rootdir%\obj3tr\*.*
  move *.hpp %rootdir%\hpp3tr
  move *.obj %rootdir%\obj3tr

REM =======================================================


REM ============================Compile Builder 4 packages
del *.dcu
del *.hpp
del *.obj

%bcb4bin%\make -B -f%rootdir%\x%verstr%pr41.BPK
if ErrorLevel 1 pause

%bcb4bin%\make -B -f%rootdir%\x%verstr%pd41.BPK
if ErrorLevel 1 pause

REM copy header files to appropriate directory
md %rootdir%\hpp4
md %rootdir%\obj4
echo y|del %rootdir%\hpp4\*.*
echo y|del %rootdir%\obj4\*.*
move *.hpp %rootdir%\hpp4
move *.obj %rootdir%\obj4

REM ==========================================Set up for Trial Run
set trialBuildFail=NO
copy %rootdir%\xpdefine.inc %rootdir%\xpdefine.xxx
copy %rootdir%\xpdefine.trl %rootdir%\xpdefine.inc
del %rootdir%\*.dcu
del %rootdir%\*.hpp
del %rootdir%\*.obj

%bcb4bin%\make -B -f%rootdir%\x%verstr%pt41.BPK
if ErrorLevel 1 set trialBuildFail=YES

REM =========================================Restore xpdefine.INC
copy %rootdir%\xpdefine.xxx %rootdir%\xpdefine.inc
if %trialBuildFail%==YES pause

  REM copy header files to appropriate directory
  md %rootdir%\hpp4tr
  md %rootdir%\obj4tr
  echo y|del %rootdir%\hpp4tr\*.*
  echo y|del %rootdir%\obj4tr\*.*
  move *.hpp %rootdir%\hpp4tr
  move *.obj %rootdir%\obj4tr

REM =======================================================


REM ============================Compile Builder 5 packages
del *.dcu
del *.hpp
del *.obj

%bcb5bin%\make -B -f%rootdir%\x%verstr%pr51.mak
if ErrorLevel 1 pause

%bcb5bin%\make -B -f%rootdir%\x%verstr%pd51.mak
if ErrorLevel 1 pause

REM copy header files to appropriate directory
md %rootdir%\hpp5
md %rootdir%\obj5
echo y|del %rootdir%\hpp5\*.*
echo y|del %rootdir%\obj5\*.*
move *.hpp %rootdir%\hpp5
move *.obj %rootdir%\obj5


REM ==========================================Set up for Trial Run
set trialBuildFail=NO
copy %rootdir%\xpdefine.inc %rootdir%\xpdefine.xxx
copy %rootdir%\xpdefine.trl %rootdir%\xpdefine.inc
del %rootdir%\*.dcu
del %rootdir%\*.hpp
del %rootdir%\*.obj

%bcb5bin%\make -B -f%rootdir%\x%verstr%pt51.mak
if ErrorLevel 1 set trialBuildFail=YES
y
REM =========================================Restore xpdefine.INC
copy %rootdir%\xpdefine.xxx %rootdir%\xpdefine.inc
if %trialBuildFail%==YES pause

  REM copy header files to appropriate directory
  md %rootdir%\hpp5tr
  md %rootdir%\obj5tr
  echo y|del %rootdir%\hpp5tr\*.*
  echo y|del %rootdir%\obj5tr\*.*
  move *.hpp %rootdir%\hpp5tr
  move *.obj %rootdir%\obj5tr
REM =======================================================


REM ============================Compile Builder 6 packages
del *.dcu
del *.hpp
del *.obj

%bcb6bin%\make -B -f%rootdir%\x%verstr%cr61.MAK
if ErrorLevel 1 pause

%bcb6bin%\make -B -f%rootdir%\x%verstr%pr61.MAK
if ErrorLevel 1 pause

%bcb6bin%\make -B -f%rootdir%\x%verstr%pd61.MAK
if ErrorLevel 1 pause

%bcb6bin%\make -B -f%rootdir%\x%verstr%qr61.MAK -DUsingCLX
if ErrorLevel 1 pause

%bcb6bin%\make -B -f%rootdir%\x%verstr%qd61.MAK -DUsingCLX
if ErrorLevel 1 pause

REM copy header files to appropriate directory
md %rootdir%\hpp6
md %rootdir%\obj6
echo y|del %rootdir%\hpp6\*.*
echo y|del %rootdir%\obj6\*.*
move *.hpp %rootdir%\hpp6
move *.obj %rootdir%\obj6

REM ==========================================Set up for VCL Trial Run
set trialBuildFail=NO
copy %rootdir%\xpdefine.inc %rootdir%\xpdefine.xxx
copy %rootdir%\xpdefine.trl %rootdir%\xpdefine.inc
del %rootdir%\*.dcu
del %rootdir%\*.hpp
del %rootdir%\*.obj

%bcb6bin%\make -B -f%rootdir%\x%verstr%pt61.mak
if ErrorLevel 1 set trialBuildFail=YES

REM =========================================Restore xpdefine.INC
copy %rootdir%\xpdefine.xxx %rootdir%\xpdefine.inc
if %trialBuildFail%==YES pause

  REM copy header files to appropriate directory
md %rootdir%\hpp6vtr
md %rootdir%\obj6vtr
echo y|del %rootdir%\hpp6vtr\*.*
echo y|del %rootdir%\obj6vtr\*.*
move *.hpp %rootdir%\hpp6vtr
move *.obj %rootdir%\obj6vtr

REM =======================================================

REM ==========================================Set up for CLX Trial Run
set trialBuildFail=NO
copy %rootdir%\xpdefine.inc %rootdir%\xpdefine.xxx
copy %rootdir%\xpdefine.trl %rootdir%\xpdefine.inc
del %rootdir%\*.dcu
del %rootdir%\*.hpp
del %rootdir%\*.obj

%bcb6bin%\make -B -f%rootdir%\x%verstr%qt61.mak -DUsingCLX
if ErrorLevel 1 set trialBuildFail=YES

REM =========================================Restore xpdefine.INC
copy %rootdir%\xpdefine.xxx %rootdir%\xpdefine.inc
if %trialBuildFail%==YES pause

  REM copy header files to appropriate directory
md %rootdir%\hpp6ctr
md %rootdir%\obj6ctr
echo y|del %rootdir%\hpp6ctr\*.*
echo y|del %rootdir%\obj6ctr\*.*
move *.hpp %rootdir%\hpp6ctr
move *.obj %rootdir%\obj6ctr

REM =======================================================

:sign

REM ============================Sign Delphi 3 packages
rem n:\builds\common\signcode x%verstr%pr30.dpl -spc n:\builds\credentials\tps.spc -v n:\builds\credentials\tps.pvk -n "XMLPartner Professional Run-time Package" -i http://www.turbopower.com -t http://timestamp.verisign.com/scripts/timstamp.dll
rem n:\builds\common\chktrust x%verstr%pr30.dpl -q
rem if ErrorLevel 1 pause

REM =======================================================


REM ============================Sign Builder 3 packages
rem n:\builds\common\signcode x%verstr%pr35.bpl -spc n:\builds\credentials\tps.spc -v n:\builds\credentials\tps.pvk -n "XMLPartner Professional Run-time Package" -i http://www.turbopower.com -t http://timestamp.verisign.com/scripts/timstamp.dll
rem n:\builds\common\chktrust x%verstr%pr35.bpl -q
rem if ErrorLevel 1 pause
REM =======================================================


REM ============================Sign Delphi 4 packages
rem n:\builds\common\signcode x%verstr%pr40.bpl -spc n:\builds\credentials\tps.spc -v n:\builds\credentials\tps.pvk -n "XMLPartner Professional Run-time Package" -i http://www.turbopower.com -t http://timestamp.verisign.com/scripts/timstamp.dll
rem n:\builds\common\chktrust x%verstr%pr40.bpl -q
rem if ErrorLevel 1 pause

REM =======================================================


REM ============================Sign Builder 4 packages
rem n:\builds\common\signcode x%verstr%pr41.bpl -spc n:\builds\credentials\tps.spc -v n:\builds\credentials\tps.pvk -n "XMLPartner Professional Run-time Package" -i http://www.turbopower.com -t http://timestamp.verisign.com/scripts/timstamp.dll
rem n:\builds\common\chktrust x%verstr%pr41.bpl -q
rem if ErrorLevel 1 pause

REM =======================================================


REM ============================Sign Delphi 5 packages
rem n:\builds\common\signcode x%verstr%pr50.bpl -spc n:\builds\credentials\tps.spc -v n:\builds\credentials\tps.pvk -n "XMLPartner Professional Run-time Package" -i http://www.turbopower.com -t http://timestamp.verisign.com/scripts/timstamp.dll
rem n:\builds\common\chktrust x%verstr%pr50.bpl -q
rem if ErrorLevel 1 pause
REM =======================================================


REM ============================Sign Builder 5 packages
rem n:\builds\common\signcode x%verstr%pr51.bpl -spc n:\builds\credentials\tps.spc -v n:\builds\credentials\tps.pvk -n "XMLPartner Professional Run-time Package" -i http://www.turbopower.com -t http://timestamp.verisign.com/scripts/timstamp.dll
rem n:\builds\common\chktrust x%verstr%pr51.bpl
rem if ErrorLevel 1 pause
REM =======================================================


REM ============================Sign Delphi 6 packages
rem n:\builds\common\signcode x%verstr%PR60.bpl -spc n:\builds\credentials\tps.spc -v n:\builds\credentials\tps.pvk -n "XMLPartner Professional Run-time Package" -i http://www.turbopower.com -t http://timestamp.verisign.com/scripts/timstamp.dll
rem n:\builds\common\chktrust x%verstr%PR60.bpl

rem n:\builds\common\signcode x%verstr%QR60.bpl -spc n:\builds\credentials\tps.spc -v n:\builds\credentials\tps.pvk -n "XMLPartner Professional Run-time Package" -i http://www.turbopower.com -t http://timestamp.verisign.com/scripts/timstamp.dll
rem n:\builds\common\chktrust x%verstr%QR60.bpl

rem if ErrorLevel 1 pause
REM =======================================================


REM ============================Sign Builder 6 packages
rem n:\builds\common\signcode x%verstr%pr61.bpl -spc n:\builds\credentials\tps.spc -v n:\builds\credentials\tps.pvk -n "XMLPartner Professional Run-time Package" -i http://www.turbopower.com -t http://timestamp.verisign.com/scripts/timstamp.dll
rem n:\builds\common\chktrust x%verstr%pr61.bpl
rem if ErrorLevel 1 pause
REM =======================================================

%tptool%\fileset %builddir%\*.* -t %timestr% -d %datestr%
