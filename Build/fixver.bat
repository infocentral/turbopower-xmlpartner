cd %rootdir%
%tptool%\sreplace "%oldver% " "%newver% " /n /r /s *.pas
%tptool%\sreplace "%oldver% " "%newver% " /n /r /s *.trl
%tptool%\sreplace "%oldver% " "%newver% " /n /r /s *.str
%tptool%\sreplace "%oldver% " "%newver% " /n /r /s *.inc
%tptool%\sreplace "%oldver% " "%newver% " /n /r /s *.dpr
%tptool%\sreplace "%oldver% " "%newver% " /n /r /s *.bpr
%tptool%\sreplace "%oldver2%" "%newver2%" /n /r /s *.dof

%tptool%\sreplace "%oldver% " "%newver% " /n /r /s *.dpk
%tptool%\sreplace "%oldintver%" "%newintver%" /n /r /s *.dpk
%tptool%\sreplace "%oldver2%" "%newver2%" /n /r /s *.dpk

%tptool%\sreplace "%oldver% " "%newver% " /n /r /s *.bpk
%tptool%\sreplace "=%oldver%" "=%newver%" /n /r /s *.bpk
%tptool%\sreplace "%oldintver%" "%newintver%" /n /r /s *.bpk
%tptool%\sreplace "%oldver2%" "%newver2%" /n /r /s *.bpk

%tptool%\sreplace "%oldintver%" "%newintver%" /n /r /s *.bpr

%tptool%\sreplace "%oldver% " "%newver% " /n /r /s *.mak
%tptool%\sreplace "=%oldver%" "=%newver%" /n /r /s *.mak
%tptool%\sreplace "X%oldintver%" "X%newintver%" /n /r /s *.mak
%tptool%\sreplace "x%oldintver%" "x%newintver%" /n /r /s *.mak

%tptool%\sreplace "%oldintver%" "%newintver%" /n /r /s *.cpp

%tptool%\sreplace "%oldver%" "%newver%" /n /r /s *.rc
%tptool%\sreplace "%oldintver%" "%newintver%" /n /r /s *.rc
%tptool%\sreplace "%oldver2%" "%newver2%" /n /r /s *.rc
%tptool%\sreplace "%oldver3%" "%newver3%" /n /r /s *.rc

%tptool%\sreplace "MinorVer=%oldminorverA%" "MinorVer=%newminorverA%" /n /r /s *.bpk
%tptool%\sreplace "Release=%oldminorverB%" "Release=%newminorverB%" /n /r /s *.bpk

rem Corrective actions
%tptool%\sreplace "Prerelease=%newminorverB%" "Prerelease=0" /n /r /s *.bpk
%tptool%\sreplace "CodePage=125%newminorverB%" "CodePage=1252" /n /r /s *.bpk
%tptool%\sreplace "CodePage=125%newminorverB%" "CodePage=1252" /n /r /s *.bpr
