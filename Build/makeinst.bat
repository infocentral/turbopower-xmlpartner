rem
rem Runs the Wise installer to create the various installation Executables.
rem

rem Change to the SETUP directory.
cd %rootdir%\setup

rem Create full installation
setwdisk XPV.wse 1420
%wisedir%\wise32 /d _output_path_=%builddir%\setup\full /d _output_exe_=setup /d _patching_=NO /d _trialrun_=0 /c XPV.wse

Create single-file patch installation
setwdisk XPV.wse 0
%wisedir%\wise32 /d _output_path_=%builddir%\setup\singlefilepatch /d _output_exe_=XPV%verstr%P /d _patching_=YES /d _trialrun_=0 /c XPV.wse

Create multi-file patch
setwdisk XPV.wse 1420
%wisedir%\wise32 /d _output_path_=%builddir%\setup\diskpatch /d _output_exe_=XPV%verstr%PD /d _patching_=YES /d _trialrun_=0 /c XPV.wse

setwdisk XPV.wse 0
%wisedir%\wise32 /d _output_path_=%builddir%\setup\D3Trial /d _output_exe_=XPVTR3 /d _patching_=NO /d _trialrun_=3 /c XPV.wse

rem Create D4 trial installation
setwdisk XPV.wse 0
%wisedir%\wise32 /d _output_path_=%builddir%\setup\D4Trial /d _output_exe_=XPVTR4 /d _patching_=NO /d _trialrun_=4 /c XPV.wse

rem Create D5 trial installation
setwdisk XPV.wse 0
%wisedir%\wise32 /d _output_path_=%builddir%\setup\D5Trial /d _output_exe_=XPVTR5 /d _patching_=NO /d _trialrun_=5 /c XPV.wse

rem Create D6 VCL trial installation
setwdisk XPV.wse 0
%wisedir%\wise32 /d _output_path_=%builddir%\setup\D6VCLTrial /d _output_exe_=XPVTR6V /d _patching_=NO /d _trialrun_=6 /c XPV.wse

rem Create D6 CLX trial installation
setwdisk XPV.wse 0
%wisedir%\wise32 /d _output_path_=%builddir%\setup\D6CLXTrial /d _output_exe_=XPVTR6C /d _patching_=NO /d _trialrun_=7 /c XPV.wse

rem Create C3 trial installation
setwdisk XPV.wse 0
%wisedir%\wise32 /d _output_path_=%builddir%\setup\C3Trial /d _output_exe_=XPVTRC /d _patching_=NO /d _trialrun_=C /c XPV.wse

rem Create C4 trial installation
setwdisk XPV.wse 0
%wisedir%\wise32 /d _output_path_=%builddir%\setup\C4Trial /d _output_exe_=XPVTRD /d _patching_=NO /d _trialrun_=D /c XPV.wse

rem Create C5 trial installation
setwdisk XPV.wse 0
%wisedir%\wise32 /d _output_path_=%builddir%\setup\C5Trial /d _output_exe_=XPVTRE /d _patching_=NO /d _trialrun_=E /c XPV.wse

rem Create C6 VCL trial installation
setwdisk XPV.wse 0
%wisedir%\wise32 /d _output_path_=%builddir%\setup\C6VCLTrial /d _output_exe_=XPVTRFV /d _patching_=NO /d _trialrun_=F /c XPV.wse

rem Create CD6 CLX trial installation
setwdisk XPV.wse 0
%wisedir%\wise32 /d _output_path_=%builddir%\setup\C6CLXTrial /d _output_exe_=XPVTRFC /d _patching_=NO /d _trialrun_=G /c XPV.wse

