rem ****************************************************************************
rem ****************************************************************************
rem Initializes common environment variables required for build
rem ****************************************************************************

:Initialize

rem ****************************************************************************
rem Initialize Delphi Directories
rem ****************************************************************************

verify on

set d3bin=e:\dev\delphi\3.0\bin
set d4bin=e:\dev\delphi\4.0\bin
set d5bin=e:\dev\delphi\5.0\bin
set d6bin=e:\dev\delphi\6.0\bin
set d7bin=e:\dev\delphi\7.0\bin

rem ****************************************************************************
rem Initialize BCB Directories
rem ****************************************************************************

set bcb3bin=e:\dev\bcb\3.0\bin
set bcb4bin=e:\dev\bcb\4.0\bin
set bcb5bin=e:\dev\bcb\5.0\bin
set bcb6bin=e:\dev\bcb\6.0\bin

rem ****************************************************************************
rem Initialize Compilers
rem ****************************************************************************

set dcc3=%d3bin%\dcc32
set dcc4=%d4bin%\dcc32
set dcc5=%d5bin%\dcc32
set dcc6=%d6bin%\dcc32
set dcc7=%d7bin%\dcc32

rem ****************************************************************************
rem Initialize code/tool directories
rem ****************************************************************************

set srmgr=e:\srmgr
set builddir=d:\build\xpro
set rootdir=d:\dev\xpro
set tptool=j:\util
set WiseDir=d:\util\wise

rem ****************************************************************************
rem Initialize version, date stamp information
rem ****************************************************************************

set oldmajorver=2
set oldminorverA=5
set oldminorverB=6
set oldminorver=%oldminorverA%%oldminorverB%

set newmajorver=2
set newminorverA=5
set newminorverB=7
set newminorver=%newminorverA%%newminorverB%

set oldver=%oldmajorver%.%oldminorver%
set oldintver=%oldmajorver%%oldminorver%
set oldver2=%oldmajorver%.%oldminorverA%.%oldminorverB%
set oldver3=%oldmajorver%, %oldminorverA%, %oldminorverB%

set newver=%newmajorver%.%newminorver%
set newintver=%newmajorver%%newminorver%
set newver2=%newmajorver%.%newminorverA%.%newminorverB%
set newver3=%newmajorver%, %newminorverA%, %newminorverB%

set verstr=%newintver%
set timestr=%newmajorver%:%newminorver%:00
set datestr=10/17/2002

REM This batch successful
ECHO Setup Complete!

GOTO END

:BREAK
REM An error has occured while configuring the environment
ECHO Environment could not be initialized
ECHO  - Likely error, out of environment space
ECHO .
PAUSE

:END
 
