rem ****************************************************************************
rem ****************************************************************************
rem Cleans up the common environment variables required for build
rem ****************************************************************************

:Cleanup

verify off

set dbase=
set d3bin=
set d4bin=
set d5bin=
set d6bin=
set d7bin=

set cbase=
set bcb3bin=
set bcb4bin=
set bcb5bin=
set bcb6bin=

set dcc=
set dcc2=
set dcc3=
set dcc4=
set dcc5=
set dcc6=
set dcc7=

set srmgr=
set builddir=
set rootdir=
set tptool=

set verstr=
set timestr=
set datestr=

set oldmajorver=
set oldminorver=

set newmajorver=
set newminorver=

set oldver=
set oldintver=

set newver=
set newintver=