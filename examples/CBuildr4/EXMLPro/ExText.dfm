object TextForm: TTextForm
  Left = 172
  Top = 363
  Width = 400
  Height = 300
  Caption = 'Enter Text Form'
  Color = clBtnFace
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poDefault
  Scaled = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 0
    Width = 392
    Height = 13
    Align = alTop
    Caption = 'Text data (this can be blank):'
  end
  object Panel1: TPanel
    Left = 0
    Top = 232
    Width = 392
    Height = 41
    Align = alBottom
    TabOrder = 0
    object OkBtn: TButton
      Left = 228
      Top = 8
      Width = 75
      Height = 25
      Caption = '&OK'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object CancelBtn: TButton
      Left = 308
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = '&Cancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
  object TextEdit: TMemo
    Left = 0
    Top = 13
    Width = 392
    Height = 219
    Align = alClient
    ScrollBars = ssVertical
    TabOrder = 1
  end
end
