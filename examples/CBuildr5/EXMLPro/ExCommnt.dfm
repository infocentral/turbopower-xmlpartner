object CommentForm: TCommentForm
  Left = 417
  Top = 499
  BorderStyle = bsDialog
  Caption = 'Enter comment'
  ClientHeight = 86
  ClientWidth = 375
  Color = clBtnFace
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  Scaled = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 3
    Top = 8
    Width = 47
    Height = 13
    Caption = '&Comment:'
    FocusControl = CommentEdit
  end
  object CommentEdit: TEdit
    Left = 3
    Top = 28
    Width = 369
    Height = 21
    TabOrder = 0
  end
  object OkBtn: TButton
    Left = 215
    Top = 56
    Width = 75
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
  end
  object CancelBtn: TButton
    Left = 295
    Top = 56
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
