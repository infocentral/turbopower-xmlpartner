object ElementForm: TElementForm
  Left = 274
  Top = 499
  BorderStyle = bsDialog
  Caption = 'Enter element name'
  ClientHeight = 94
  ClientWidth = 242
  Color = clBtnFace
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  Scaled = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 4
    Top = 8
    Width = 175
    Height = 13
    Caption = '&Element name (this cannot be blank):'
    FocusControl = NameEdit
  end
  object NameEdit: TEdit
    Left = 4
    Top = 28
    Width = 237
    Height = 21
    TabOrder = 0
  end
  object OkBtn: TButton
    Left = 84
    Top = 64
    Width = 75
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = OkBtnClick
  end
  object CancelBtn: TButton
    Left = 164
    Top = 64
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
