object URLForm: TURLForm
  Left = 240
  Top = 508
  BorderStyle = bsDialog
  Caption = 'Specify URL'
  ClientHeight = 174
  ClientWidth = 377
  Color = clBtnFace
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  Scaled = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 274
    Height = 13
    Caption = '&Enter URL: (e.g. "http://www.turbopower.com/email.xml")'
    FocusControl = UrlEdit
  end
  object Label2: TLabel
    Left = 20
    Top = 80
    Width = 60
    Height = 13
    Caption = 'FTP &User Id:'
    FocusControl = FtpUserIdEdit
  end
  object Label3: TLabel
    Left = 8
    Top = 112
    Width = 72
    Height = 13
    Caption = 'FTP &Password:'
    FocusControl = FtpPasswordEdit
  end
  object Label4: TLabel
    Left = 20
    Top = 24
    Width = 243
    Height = 13
    Caption = '(e.g. "ftp://ftp.turbopower.com/xmldocs/email.xml")'
  end
  object UrlEdit: TEdit
    Left = 8
    Top = 44
    Width = 365
    Height = 21
    TabOrder = 0
  end
  object FtpUserIdEdit: TEdit
    Left = 88
    Top = 76
    Width = 121
    Height = 21
    TabOrder = 1
  end
  object FtpPasswordEdit: TEdit
    Left = 88
    Top = 108
    Width = 121
    Height = 21
    TabOrder = 2
  end
  object OkBtn: TButton
    Left = 220
    Top = 144
    Width = 75
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 3
    OnClick = OkBtnClick
  end
  object CancelBtn: TButton
    Left = 300
    Top = 144
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 4
  end
end
