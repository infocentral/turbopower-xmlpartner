object frmErrors: TfrmErrors
  Left = 206
  Top = 184
  Width = 542
  Height = 268
  Caption = 'Load errors for %s'
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnClose = FormClose
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object memErrors: TMemo
    Left = 0
    Top = 33
    Width = 534
    Height = 175
    Align = alClient
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 208
    Width = 534
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    Caption = 'pnlBottom'
    TabOrder = 1
    object pbOK: TButton
      Left = 224
      Top = 5
      Width = 75
      Height = 25
      Caption = '&OK'
      TabOrder = 0
      OnClick = pbOKClick
    end
  end
  object pnlMsg: TPanel
    Left = 0
    Top = 0
    Width = 534
    Height = 33
    Align = alTop
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Caption = 
      '  The following errors were encountered while loading the docume' +
      'nt:'
    TabOrder = 2
  end
end
