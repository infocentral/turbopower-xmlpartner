object AttributeForm: TAttributeForm
  Left = 432
  Top = 467
  ActiveControl = AttrNameEdit
  BorderStyle = bsDialog
  Caption = 'Attribute Edit'
  ClientHeight = 153
  ClientWidth = 249
  Color = clBtnFace
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 176
    Height = 13
    Caption = 'Attribute &name (this cannot be blank):'
    FocusControl = AttrNameEdit
  end
  object Label2: TLabel
    Left = 8
    Top = 68
    Width = 71
    Height = 13
    Caption = 'Attribute &value:'
    FocusControl = AttrValueEdit
  end
  object OkBtn: TButton
    Left = 90
    Top = 124
    Width = 75
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
    OnClick = OkBtnClick
  end
  object CancelBtn: TButton
    Left = 170
    Top = 124
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 3
  end
  object AttrNameEdit: TEdit
    Left = 8
    Top = 28
    Width = 237
    Height = 21
    TabOrder = 0
  end
  object AttrValueEdit: TEdit
    Left = 8
    Top = 88
    Width = 237
    Height = 21
    TabOrder = 1
  end
end
