object PIForm: TPIForm
  Left = 187
  Top = 374
  Width = 252
  Height = 172
  Caption = 'Processing Instruction'
  Color = clBtnFace
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 3
    Top = 8
    Width = 240
    Height = 13
    Caption = 'Processing instruction &name (this cannot be blank):'
    FocusControl = PINameEdit
  end
  object Label2: TLabel
    Left = 3
    Top = 68
    Width = 135
    Height = 13
    Caption = 'Processing instruction &value:'
    FocusControl = PIValueEdit
  end
  object PINameEdit: TEdit
    Left = 3
    Top = 28
    Width = 237
    Height = 21
    TabOrder = 0
  end
  object PIValueEdit: TEdit
    Left = 3
    Top = 88
    Width = 237
    Height = 21
    TabOrder = 1
  end
  object OkBtn: TButton
    Left = 84
    Top = 117
    Width = 75
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
    OnClick = OkBtnClick
  end
  object CancelBtn: TButton
    Left = 164
    Top = 117
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 3
  end
end
