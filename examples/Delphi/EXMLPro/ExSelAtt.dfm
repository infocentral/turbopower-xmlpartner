object SelAttrsForm: TSelAttrsForm
  Left = 397
  Top = 226
  BorderStyle = bsDialog
  Caption = 'Select Attributes'
  ClientHeight = 348
  ClientWidth = 247
  Color = clBtnFace
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  Scaled = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 4
    Width = 146
    Height = 13
    Caption = '&Select the attributes to include:'
    FocusControl = AttrsListView
  end
  object AttrsListView: TListView
    Left = 8
    Top = 20
    Width = 229
    Height = 293
    Checkboxes = True
    Columns = <
      item
        AutoSize = True
      end>
    ColumnClick = False
    ReadOnly = True
    RowSelect = True
    ShowColumnHeaders = False
    TabOrder = 0
    ViewStyle = vsReport
    OnData = AttrsListViewData
  end
  object OkBtn: TButton
    Left = 82
    Top = 320
    Width = 75
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = OkBtnClick
  end
  object CancelBtn: TButton
    Left = 162
    Top = 320
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
