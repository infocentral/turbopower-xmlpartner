object frmMain: TfrmMain
  Left = 251
  Top = 162
  Width = 559
  Height = 198
  Caption = 'XMLPartner Pro No Filter Demo'
  Color = clBtnFace
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  PixelsPerInch = 96
  TextHeight = 13
  object btnConvert: TButton
    Left = 8
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Convert'
    TabOrder = 0
    OnClick = btnConvertClick
  end
  object CSVFile: TMemo
    Left = 8
    Top = 40
    Width = 537
    Height = 129
    ScrollBars = ssBoth
    TabOrder = 1
  end
  object DOM: TXpObjModel
    BufferSize = 8192
    IdAttribute = 'id'
    OutCharSet = ceUnknown
    Password = 'xmlpartner@turbopower.com'
    UserName = 'anonymous'
    WriteUTF8Signature = True
    Left = 184
    Top = 8
  end
  object XSLProc: TXpXSLProcessor
    IgnoreCase = False
    OnApplyStyleEnd = XslProcApplyStyleEnd
    OnApplyStyleStart = XslProcApplyStyleStart
    OnElementEnd = XSLProcElementEnd
    OnElementStart = XSLProcElementStart
    OnText = XSLProcText
    XmlObjModel = DOM
    Left = 216
    Top = 8
  end
end
