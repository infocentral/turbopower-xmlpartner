object frmMain: TfrmMain
  Left = 246
  Top = 145
  BorderStyle = bsDialog
  Caption = 'XMLPartner XML Text Viewer'
  ClientHeight = 238
  ClientWidth = 366
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 0
    Width = 93
    Height = 13
    Caption = 'Document to parse:'
  end
  object btnParse: TButton
    Left = 288
    Top = 14
    Width = 73
    Height = 25
    Caption = 'Parse'
    TabOrder = 0
    OnClick = btnParseClick
  end
  object memo: TRichEdit
    Left = 8
    Top = 48
    Width = 353
    Height = 185
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
  end
  object edtFile: TEdit
    Left = 8
    Top = 16
    Width = 153
    Height = 21
    TabOrder = 2
  end
  object btnOpenFile: TButton
    Left = 160
    Top = 16
    Width = 17
    Height = 20
    Caption = '...'
    TabOrder = 3
    OnClick = btnOpenFileClick
  end
  object fodXMLDoc: TOpenDialog
    Filter = 'XML Documents (*.xml)|*.xml'
    Left = 24
    Top = 208
  end
  object Parser: TXpParser
    Password = 'xmlpartner@turbopower.com'
    UserName = 'anonymous'
    OnAttribute = ParserAttribute
    OnCharData = ParserCharData
    OnEndElement = ParserEndElement
    OnStartElement = ParserStartElement
    Left = 64
    Top = 208
  end
end
