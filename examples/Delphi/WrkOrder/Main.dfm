object WorkOrders: TWorkOrders
  Left = 156
  Top = 140
  Width = 766
  Height = 526
  Caption = 'WorkOrders'
  Color = clBtnFace
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Pages: TPageControl
    Left = 0
    Top = 0
    Width = 758
    Height = 499
    ActivePage = Confirm
    Align = alClient
    TabOrder = 0
    object Query: TTabSheet
      Caption = 'Query'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 750
        Height = 89
        Align = alTop
        Caption = 'Panel1'
        TabOrder = 0
        object rgSearchType: TRadioGroup
          Left = 1
          Top = 1
          Width = 168
          Height = 87
          Align = alLeft
          Caption = 'Search By'
          ItemIndex = 0
          Items.Strings = (
            'Category'
            'Date'
            'Technician')
          TabOrder = 0
          OnClick = rgSearchTypeClick
        end
        object gbCriteria: TGroupBox
          Left = 169
          Top = 1
          Width = 580
          Height = 87
          Align = alClient
          Caption = 'Search Criteria'
          TabOrder = 1
          object pnlCatCrit: TPanel
            Left = 2
            Top = 15
            Width = 576
            Height = 70
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            Visible = False
            object rbLev1: TRadioButton
              Left = 8
              Top = 3
              Width = 145
              Height = 17
              Caption = '1 - Technicians'
              Checked = True
              TabOrder = 0
              TabStop = True
              OnClick = rbLev1Click
            end
            object rbLev3: TRadioButton
              Left = 8
              Top = 47
              Width = 137
              Height = 17
              Caption = '3 - Network Architects'
              TabOrder = 1
              OnClick = rbLev1Click
            end
            object rbLev2: TRadioButton
              Left = 8
              Top = 23
              Width = 145
              Height = 17
              Caption = '2 - Network Administrators'
              TabOrder = 2
              OnClick = rbLev1Click
            end
          end
        end
      end
      object statusBar: TStatusBar
        Left = 0
        Top = 446
        Width = 750
        Height = 25
        Panels = <
          item
            Text = 'Status'
            Width = 250
          end
          item
            Text = '0 nodes found'
            Width = 150
          end
          item
            Text = 'Search Time'
            Width = 150
          end>
        SimplePanel = False
      end
      object cmbTechs: TComboBox
        Left = 184
        Top = 40
        Width = 193
        Height = 21
        ItemHeight = 13
        TabOrder = 2
        OnChange = cmbTechsChange
        Items.Strings = (
          '<none>'
          'Bill Dundee'
          'Joe Netty'
          'Becky Newman'
          'Drew Pits'
          'Dink Redrum'
          'Pete Sommers')
      end
      object memWorkOrder: TMemo
        Left = 250
        Top = 121
        Width = 500
        Height = 295
        Align = alClient
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssBoth
        TabOrder = 3
        WordWrap = False
      end
      object Panel2: TPanel
        Left = 0
        Top = 89
        Width = 750
        Height = 32
        Align = alTop
        Caption = 'Panel2'
        TabOrder = 4
        object edtExpression: TEdit
          Left = 8
          Top = 4
          Width = 529
          Height = 24
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object btnExecute: TButton
          Left = 544
          Top = 4
          Width = 75
          Height = 25
          Caption = '&Execute'
          Default = True
          TabOrder = 1
          OnClick = btnExecuteClick
        end
      end
      object DateEdit: TDateTimePicker
        Left = 184
        Top = 40
        Width = 186
        Height = 21
        CalAlignment = dtaLeft
        Date = 37092.598423692100000000
        Time = 37092.598423692100000000
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        TabOrder = 5
        OnChange = DateEditChange
      end
      object lbNodes: TListView
        Left = 0
        Top = 121
        Width = 250
        Height = 295
        Align = alLeft
        Columns = <
          item
            AutoSize = True
            Caption = 'Elements found'
          end>
        HideSelection = False
        ReadOnly = True
        RowSelect = True
        TabOrder = 6
        ViewStyle = vsReport
        OnSelectItem = lbNodesSelectItem
      end
      object Panel3: TPanel
        Left = 0
        Top = 416
        Width = 750
        Height = 30
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 7
        object btnConfirm: TButton
          Left = 664
          Top = 3
          Width = 75
          Height = 25
          Caption = '&Confirm'
          TabOrder = 0
          OnClick = btnConfirmClick
        end
      end
    end
    object Confirm: TTabSheet
      Caption = 'Confirm - Text && Print'
      ImageIndex = 1
      object Splitter1: TSplitter
        Left = 281
        Top = 0
        Width = 3
        Height = 471
        Cursor = crHSplit
      end
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 281
        Height = 471
        Align = alLeft
        Caption = 'Panel4'
        TabOrder = 0
        object Panel6: TPanel
          Left = 1
          Top = 1
          Width = 279
          Height = 24
          Align = alTop
          BevelOuter = bvNone
          Caption = 'Email confirmation body'
          TabOrder = 0
        end
        object memConfirm: TMemo
          Left = 1
          Top = 25
          Width = 279
          Height = 445
          Align = alClient
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          ScrollBars = ssVertical
          TabOrder = 1
        end
      end
      object Panel5: TPanel
        Left = 284
        Top = 0
        Width = 466
        Height = 471
        Align = alClient
        Caption = 'Panel5'
        TabOrder = 1
        object Panel7: TPanel
          Left = 1
          Top = 1
          Width = 464
          Height = 24
          Align = alTop
          BevelOuter = bvNone
          Caption = 'Hardcopy Printout'
          TabOrder = 0
          object btnPrint: TButton
            Left = 376
            Top = 6
            Width = 65
            Height = 17
            Hint = 'Print the work order'
            Anchors = [akLeft, akTop, akRight]
            Caption = '&Print'
            TabOrder = 0
            OnClick = btnPrintClick
          end
        end
        object PrintFilter: TXpFilterPrint
          Left = 1
          Top = 25
          Width = 464
          Height = 445
          Password = 'xmlpartner@turbopower.com'
          UserName = 'anonymous'
          Align = alClient
          BevelInner = bvNone
          BevelOuter = bvNone
          HSlidePosition = 0
          PageCurrent = 1
          VSlidePosition = 0
        end
        object TextFilter: TXpFilterText
          Left = 232
          Top = 32
          Width = 28
          Height = 28
          Password = 'xmlpartner@turbopower.com'
          UserName = 'anonymous'
        end
      end
    end
    object Status: TTabSheet
      Caption = 'Status - HTML'
      ImageIndex = 2
      object webBrowser: TWebBrowser
        Left = 0
        Top = 41
        Width = 750
        Height = 430
        Align = alClient
        TabOrder = 0
        ControlData = {
          4C000000844D0000712C00000000000000000000000000000000000000000000
          000000004C000000000000000000000001000000E0D057007335CF11AE690800
          2B2E126201000000000000004C0000000114020000000000C000000000000046
          8000000000000000000000000000000000000000000000000000000000000000
          00000000000000000100000000000000000000000000000000000000}
      end
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 750
        Height = 41
        Align = alTop
        TabOrder = 1
        object lblStatusOrderBy: TLabel
          Left = 8
          Top = 14
          Width = 40
          Height = 13
          Caption = '&Order by'
          FocusControl = cmbStatusOrderBy
        end
        object btnRefresh: TButton
          Left = 304
          Top = 8
          Width = 75
          Height = 25
          Caption = '&Refresh'
          TabOrder = 0
          OnClick = btnRefreshClick
        end
        object cmbStatusOrderBy: TComboBox
          Left = 56
          Top = 10
          Width = 145
          Height = 21
          ItemHeight = 13
          TabOrder = 1
          Items.Strings = (
            'Category'
            'WorkOrder'
            'AssignedTo')
        end
        object cbStatusAscending: TCheckBox
          Left = 216
          Top = 12
          Width = 81
          Height = 17
          Caption = '&Ascending'
          Checked = True
          State = cbChecked
          TabOrder = 2
        end
      end
      object HTMLFilter: TXpFilterHTML
        Left = 448
        Top = 64
        Width = 28
        Height = 28
        Password = 'xmlpartner@turbopower.com'
        UserName = 'anonymous'
        OutputMode = xphomBodyOnly
      end
    end
    object Report: TTabSheet
      Caption = 'Report - RTF'
      ImageIndex = 3
      object rtfMemo: TRichEdit
        Left = 0
        Top = 33
        Width = 750
        Height = 438
        Align = alClient
        TabOrder = 0
      end
      object Panel9: TPanel
        Left = 0
        Top = 0
        Width = 750
        Height = 33
        Align = alTop
        TabOrder = 1
        object pbRefresh: TButton
          Left = 8
          Top = 5
          Width = 75
          Height = 25
          Caption = '&Refresh'
          TabOrder = 0
          OnClick = pbRefreshClick
        end
        object btnOpen: TButton
          Left = 88
          Top = 5
          Width = 75
          Height = 25
          Hint = 
            'Open the status report using the application registered for RTF ' +
            'files'
          Caption = '&Open'
          TabOrder = 1
          OnClick = btnOpenClick
        end
      end
      object RTFFilter: TXpFilterRTF
        Left = 482
        Top = 64
        Width = 28
        Height = 28
        Password = 'xmlpartner@turbopower.com'
        UserName = 'anonymous'
      end
    end
  end
  object DOM: TXpObjModel
    BufferSize = 8192
    FormattedOutput = True
    IdAttribute = 'id'
    Password = 'xmlpartner@turbopower.com'
    UserName = 'anonymous'
    Left = 424
    Top = 56
  end
  object XSLProcessor: TXpXSLProcessor
    IgnoreCase = False
    Left = 455
    Top = 56
  end
  object wrkDOM: TXpObjModel
    BufferSize = 8192
    IdAttribute = 'id'
    Password = 'xmlpartner@turbopower.com'
    UserName = 'anonymous'
    Left = 487
    Top = 56
  end
end
