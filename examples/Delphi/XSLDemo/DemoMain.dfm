object Form1: TForm1
  Left = 195
  Top = 107
  BorderStyle = bsDialog
  Caption = 'XMLPartner Professional Print Filter demo'
  ClientHeight = 473
  ClientWidth = 575
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblPage: TLabel
    Left = 3
    Top = 357
    Width = 3
    Height = 13
  end
  object gbXML: TGroupBox
    Left = 3
    Top = 0
    Width = 281
    Height = 145
    Caption = 'XML document'
    TabOrder = 0
    object dirXML: TDirectoryListBox
      Left = 8
      Top = 35
      Width = 142
      Height = 102
      FileList = fileXML
      ItemHeight = 16
      TabOrder = 0
      OnChange = drvXMLChange
    end
    object fileXML: TFileListBox
      Left = 152
      Top = 16
      Width = 121
      Height = 121
      ItemHeight = 13
      Mask = '*.xml'
      TabOrder = 1
      OnChange = drvXMLChange
    end
    object drvXML: TDriveComboBox
      Left = 8
      Top = 16
      Width = 143
      Height = 19
      DirList = dirXML
      TabOrder = 2
      OnChange = drvXMLChange
    end
  end
  object gbXSL: TGroupBox
    Left = 290
    Top = 0
    Width = 279
    Height = 145
    Caption = 'XSL Document'
    TabOrder = 1
    object dirXSL: TDirectoryListBox
      Left = 8
      Top = 35
      Width = 142
      Height = 102
      FileList = fileXSL
      ItemHeight = 16
      TabOrder = 0
      OnChange = drvXMLChange
    end
    object fileXSL: TFileListBox
      Left = 152
      Top = 16
      Width = 121
      Height = 121
      ItemHeight = 13
      Mask = '*.xsl'
      TabOrder = 1
      OnChange = drvXMLChange
    end
    object drvXSL: TDriveComboBox
      Left = 8
      Top = 16
      Width = 145
      Height = 19
      DirList = dirXSL
      TabOrder = 2
      OnChange = drvXMLChange
    end
  end
  object Panel1: TPanel
    Left = 3
    Top = 152
    Width = 566
    Height = 289
    Caption = 'Panel1'
    TabOrder = 2
    object PrintFilter: TXpFilterPrint
      Left = 1
      Top = 1
      Width = 564
      Height = 287
      Password = 'xmlpartner@turbopower.com'
      UserName = 'anonymous'
      Align = alClient
      HSlidePosition = 0
      PageCurrent = 1
      VSlidePosition = 0
    end
    object ScrollBar: TScrollBar
      Left = 549
      Top = 1
      Width = 16
      Height = 287
      Kind = sbVertical
      LargeChange = 25
      TabOrder = 1
      OnKeyDown = ScrollBarKeyDown
      OnScroll = ScrollBarScroll
    end
  end
  object btnShow: TButton
    Left = 496
    Top = 445
    Width = 75
    Height = 25
    Caption = 'Show Me!'
    Enabled = False
    TabOrder = 4
    OnClick = btnShowClick
  end
  object btnPrint: TButton
    Left = 408
    Top = 445
    Width = 81
    Height = 25
    Caption = 'Print'
    Enabled = False
    TabOrder = 3
    OnClick = btnPrintClick
  end
  object XpObjModel: TXpObjModel
    BufferSize = 8192
    IdAttribute = 'id'
    LineBreakCharReplace = False
    LineBreakMode = lbmDefault
    OutCharSet = ceUnknown
    Password = 'xmlpartner@turbopower.com'
    UserName = 'anonymous'
    WriteUTF8Signature = True
    Top = 440
  end
  object XSLProc: TXpXSLProcessor
    Filter = PrintFilter
    IgnoreCase = False
    XmlObjModel = XpObjModel
    Left = 32
    Top = 440
  end
end
