object ElementForm: TElementForm
  Left = 275
  Top = 515
  Width = 242
  Height = 94
  HorzScrollBar.Range = 241
  VertScrollBar.Range = 89
  ActiveControl = NameEdit
  Caption = 'Enter element name'
  Color = clButton
  Font.CharSet = fcsAnyCharSet
  Font.Color = clText
  Font.Height = 13
  Font.Name = 'MS Sans Serif'
  Font.Pitch = fpVariable
  Font.Style = []
  ParentFont = False
  Position = poScreenCenter
  Scaled = False
  OnShow = FormShow
  PixelsPerInch = 75
  object Label1: TLabel
    Left = 4
    Top = 8
    Width = 206
    Height = 15
    Caption = '&Element name (this cannot be blank):'
    FocusControl = NameEdit
    Layout = tlCenter
  end
  object NameEdit: TEdit
    Left = 4
    Top = 28
    Width = 237
    Height = 23
    MaxLength = 32767
    TabOrder = 0
  end
  object OkBtn: TButton
    Left = 84
    Top = 64
    Width = 75
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = OkBtnClick
  end
  object CancelBtn: TButton
    Left = 164
    Top = 64
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
