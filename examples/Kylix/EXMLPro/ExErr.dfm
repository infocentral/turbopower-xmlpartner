object frmErrors: TfrmErrors
  Left = 306
  Top = 314
  Width = 542
  Height = 268
  VertScrollBar.Range = 66
  ActiveControl = pbOK
  Caption = 'Load errors for %s'
  Color = clButton
  Font.CharSet = fcsAnyCharSet
  Font.Color = clText
  Font.Height = 13
  Font.Name = 'MS Sans Serif'
  Font.Pitch = fpVariable
  Font.Style = []
  ParentFont = False
  Position = poScreenCenter
  OnClose = FormClose
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 15
  TextWidth = 7
  object pnlMsg: TPanel
    Left = 0
    Top = 0
    Width = 542
    Height = 33
    Align = alTop
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Caption = 
      '  The following errors were encountered while loading the docume' +
      'nt:'
    TabOrder = 0
  end
  object pnlBottom: TPanel
    Left = 0
    Top = 235
    Width = 542
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object pbOK: TButton
      Left = 224
      Top = 5
      Width = 75
      Height = 25
      Caption = '&OK'
      TabOrder = 0
      OnClick = pbOKClick
    end
  end
  object memErrors: TMemo
    Left = 0
    Top = 33
    Width = 542
    Height = 202
    Align = alClient
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 2
  end
end
