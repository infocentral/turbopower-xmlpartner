object PrefsForm: TPrefsForm
  Left = 150
  Top = 237
  Width = 464
  Height = 304
  HorzScrollBar.Range = 459
  VertScrollBar.Range = 301
  ActiveControl = PageControl1
  Caption = 'Preferences'
  Color = clButton
  Font.CharSet = fcsAnyCharSet
  Font.Color = clText
  Font.Height = 13
  Font.Name = 'MS Sans Serif'
  Font.Pitch = fpVariable
  Font.Style = []
  ParentFont = False
  Position = poScreenCenter
  Scaled = False
  OnShow = FormShow
  PixelsPerInch = 75
  object OkBtn: TButton
    Left = 305
    Top = 276
    Width = 75
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = OkBtnClick
  end
  object CancelBtn: TButton
    Left = 384
    Top = 276
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 2
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 464
    Height = 273
    ActivePage = GeneralTab
    Align = alTop
    TabOrder = 0
    object GeneralTab: TTabSheet
      Caption = 'General'
      object NormalizeCheckBox: TCheckBox
        Left = 12
        Top = 12
        Width = 453
        Height = 17
        Caption = 
          'Normalize text while parsing document (applied to new/opened doc' +
          'uments)'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object FormattedCheckBox: TCheckBox
        Left = 12
        Top = 36
        Width = 381
        Height = 17
        Caption = 'Generate formatted output (applied to new/opened documents)'
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
      object btnXMLDefault: TButton
        Left = 12
        Top = 180
        Width = 277
        Height = 25
        Caption = 'Make this the default editor for XML documents'
        TabOrder = 5
        OnClick = btnXMLDefaultClick
      end
      object btnXSLDefault: TButton
        Left = 12
        Top = 212
        Width = 277
        Height = 25
        Caption = 'Make this the default editor for XSL documents'
        TabOrder = 6
        OnClick = btnXSLDefaultClick
      end
      object BackupCheckBox: TCheckBox
        Left = 12
        Top = 60
        Width = 337
        Height = 17
        Caption = 'Save backup of file when saving'
        Checked = True
        State = cbChecked
        TabOrder = 2
      end
      object TextWinCheckBox: TCheckBox
        Left = 12
        Top = 84
        Width = 329
        Height = 17
        Caption = 'Save position of text entry dialog between uses'
        TabOrder = 3
      end
      object AppWinCheckBox: TCheckBox
        Left = 12
        Top = 108
        Width = 357
        Height = 17
        Caption = 'Save position of application window between sessions'
        TabOrder = 4
      end
    end
  end
end
