object PIForm: TPIForm
  Left = 450
  Top = 346
  Width = 252
  Height = 172
  ActiveControl = PINameEdit
  Caption = 'Processing Instruction'
  Color = clButton
  Font.CharSet = fcsAnyCharSet
  Font.Color = clText
  Font.Height = 13
  Font.Name = 'MS Sans Serif'
  Font.Pitch = fpVariable
  Font.Style = []
  ParentFont = False
  Position = poScreenCenter
  Scaled = False
  PixelsPerInch = 75
  object Label1: TLabel
    Left = 3
    Top = 8
    Width = 282
    Height = 15
    Caption = 'Processing insruction &name (this cannot be blank):'
    FocusControl = PINameEdit
    Layout = tlCenter
  end
  object Label2: TLabel
    Left = 3
    Top = 68
    Width = 161
    Height = 15
    Caption = 'Processing instruction &value:'
    FocusControl = PIValueEdit
    Layout = tlCenter
  end
  object PINameEdit: TEdit
    Left = 3
    Top = 28
    Width = 237
    Height = 23
    MaxLength = 32767
    TabOrder = 0
  end
  object PIValueEdit: TEdit
    Left = 3
    Top = 88
    Width = 237
    Height = 23
    MaxLength = 32767
    TabOrder = 1
  end
  object OkBtn: TButton
    Left = 84
    Top = 117
    Width = 75
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
    OnClick = OkBtnClick
  end
  object CancelBtn: TButton
    Left = 164
    Top = 117
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 3
  end
end
