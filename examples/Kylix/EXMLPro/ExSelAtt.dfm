object SelAttrsForm: TSelAttrsForm
  Left = 397
  Top = 226
  Width = 247
  Height = 348
  ActiveControl = AttrsListView
  Caption = 'Select Attributes'
  Color = clButton
  Font.CharSet = fcsAnyCharSet
  Font.Color = clText
  Font.Height = 13
  Font.Name = 'MS Sans Serif'
  Font.Pitch = fpVariable
  Font.Style = []
  ParentFont = False
  Position = poScreenCenter
  Scaled = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 75
  object Label1: TLabel
    Left = 8
    Top = 4
    Width = 172
    Height = 15
    Caption = '&Select the attributes to include:'
    FocusControl = AttrsListView
    Layout = tlCenter
  end
  object AttrsListView: TListView
    Left = 8
    Top = 20
    Width = 229
    Height = 293
    CheckBoxes = True
    ColumnClick = False
    ColumnMove = False
    Columns = <
      item
        AllowResize = True
        AutoSize = True
        Width = 50
      end>
    RowSelect = True
    ReadOnly = True
    TabOrder = 0
    ViewStyle = vsReport
  end
  object OkBtn: TButton
    Left = 82
    Top = 320
    Width = 75
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 1
    OnClick = OkBtnClick
  end
  object CancelBtn: TButton
    Left = 162
    Top = 320
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 2
  end
end
