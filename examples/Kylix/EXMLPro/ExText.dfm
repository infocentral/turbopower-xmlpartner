object TextForm: TTextForm
  Left = 178
  Top = 336
  Width = 400
  Height = 300
  VertScrollBar.Range = 56
  ActiveControl = TextEdit
  Caption = 'Enter Text Form'
  Color = clButton
  Font.CharSet = fcsAnyCharSet
  Font.Color = clText
  Font.Height = 13
  Font.Name = 'MS Sans Serif'
  Font.Pitch = fpVariable
  Font.Style = []
  ParentFont = False
  Position = poDefault
  Scaled = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 75
  object Label1: TLabel
    Left = 0
    Top = 0
    Width = 400
    Height = 15
    Align = alTop
    Caption = 'Text data (this can be blank):'
    Layout = tlCenter
  end
  object TextEdit: TMemo
    Left = 0
    Top = 15
    Width = 400
    Height = 244
    Align = alClient
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object Panel1: TPanel
    Left = 0
    Top = 259
    Width = 400
    Height = 41
    Align = alBottom
    TabOrder = 1
    object OkBtn: TButton
      Left = 228
      Top = 8
      Width = 75
      Height = 25
      Caption = '&OK'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object CancelBtn: TButton
      Left = 308
      Top = 8
      Width = 75
      Height = 25
      Cancel = True
      Caption = '&Cancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
end
