object URLForm: TURLForm
  Left = 240
  Top = 508
  Width = 377
  Height = 174
  ActiveControl = UrlEdit
  Caption = 'Specify URL'
  Color = clButton
  Font.CharSet = fcsAnyCharSet
  Font.Color = clText
  Font.Height = 13
  Font.Name = 'MS Sans Serif'
  Font.Pitch = fpVariable
  Font.Style = []
  ParentFont = False
  Position = poScreenCenter
  Scaled = False
  OnShow = FormShow
  PixelsPerInch = 75
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 308
    Height = 15
    Caption = '&Enter URL: (e.g. "http://www.turbopower.com/email.xml")'
    FocusControl = UrlEdit
    Layout = tlCenter
  end
  object Label2: TLabel
    Left = 20
    Top = 80
    Width = 69
    Height = 15
    Caption = 'FTP &User Id:'
    FocusControl = FtpUserIdEdit
    Layout = tlCenter
  end
  object Label3: TLabel
    Left = 8
    Top = 112
    Width = 84
    Height = 15
    Caption = 'FTP &Password:'
    FocusControl = FtpPasswordEdit
    Layout = tlCenter
  end
  object Label4: TLabel
    Left = 20
    Top = 24
    Width = 273
    Height = 15
    Caption = '(e.g. "ftp://ftp.turbopower.com/xmldocs/email.xml")'
    Layout = tlCenter
  end
  object UrlEdit: TEdit
    Left = 8
    Top = 44
    Width = 365
    Height = 23
    MaxLength = 32767
    TabOrder = 0
  end
  object FtpUserIdEdit: TEdit
    Left = 88
    Top = 76
    Width = 121
    Height = 23
    MaxLength = 32767
    TabOrder = 1
  end
  object FtpPasswordEdit: TEdit
    Left = 88
    Top = 108
    Width = 121
    Height = 23
    MaxLength = 32767
    TabOrder = 2
  end
  object OkBtn: TButton
    Left = 220
    Top = 144
    Width = 75
    Height = 25
    Caption = '&OK'
    Default = True
    ModalResult = 1
    TabOrder = 3
    OnClick = OkBtnClick
  end
  object CancelBtn: TButton
    Left = 300
    Top = 144
    Width = 75
    Height = 25
    Cancel = True
    Caption = '&Cancel'
    ModalResult = 2
    TabOrder = 4
  end
end
