program DOMTests;

uses
{$IFDEF LINUX}
  QForms,
  uProgress in '../Oasis/uProgress.pas' {dlgProgress},
{$ELSE}
  Forms,
  uProgressW in '..\Oasis\uProgressW.pas' {dlgProgress},
{$ENDIF}
  GUITestRunner {DUnitDialog},
  TestFramework,
  DOMmain in 'DOMmain.pas';

{$R *.res}

begin
  GUITestRunner.runRegisteredTests;
end.

