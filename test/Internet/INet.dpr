program INet;

uses
{$IFDEF WIN32}
  Forms,
{$ENDIF}
{$IFDEF LINUX}
  QForms,
{$ENDIF}
  GUITestRunner {DUnitDialog},
  TestFramework,
  uINetTest in 'uINetTest.pas';

{$R *.res}

begin
  GUITestRunner.runRegisteredTests;
end.
