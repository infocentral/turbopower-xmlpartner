unit uINetTest;

interface
uses
{$IFDEF WIN32}
  Windows,
  Forms,
{$ENDIF}
{$IFDEF LINUX}
  QForms,
{$ENDIF}
  TestFramework,
  SysUtils,
  Classes,
  XpParser,
  XpDOM,
  XpBase;


type
  TXpINetTests = class(TTestCase)
    public
    published
      procedure TestHTTP;
      procedure TestFTP;
  end;

implementation
uses
  XpChrFlt,
  XpExcept;

{--------}
{ TXpINetTests }

procedure TXpINetTests.TestFTP;
var
  DOM : TXpObjModel;
begin
  DOM := TXpObjModel.Create(nil);
  try
    DOM.UserName := 'anonymous';
    DOM.Password := 'bluto@disney.com';
    DOM.FormattedOutput := True;
    Assert(DOM.LoadDataSource('ftp://192.168.10.119/pub/xml/dream.xml'));
    Assert(DOM.SaveToFile('ftp://192.168.10.119/pub/xml/dreamin.xml'));

    Assert(DOM.LoadDataSource('ftp://192.168.10.119/pub/xml/Email.xml'));
    Assert(DOM.SaveToFile('ftp://192.168.10.119/pub/xml/Emailin.xml'));

    Assert(DOM.LoadDataSource('ftp://192.168.10.119/pub/xml/memo.xml'));
    Assert(DOM.SaveToFile('ftp://192.168.10.119/pub/xml/memoin.xml'));

    Assert(DOM.LoadDataSource('ftp://192.168.10.119/pub/xml/tire.xml'));
    Assert(DOM.SaveToFile('ftp://192.168.10.119/pub/xml/tirein.xml'));

    Assert(DOM.LoadDataSource('ftp://192.168.10.119/pub/xml/Weather.xml'));
    Assert(DOM.SaveToFile('ftp://192.168.10.119/pub/xml/Weatherin.xml'));

    { Test documents with relative paths. }
    Assert(DOM.LoadDataSource('ftp://192.168.10.119/pub/xml/john.xml'));
    Assert(DOM.LoadDataSource('ftp://192.168.10.119/pub/xml/john2.xml'));
    Assert(DOM.LoadDataSource('ftp://192.168.10.119/pub/xml/john3.xml'));
  finally
    DOM.Free;
  end;
end;

procedure TXpINetTests.TestHTTP;
var
  DOM : TXpObjModel;
begin
  DOM := TXpObjModel.Create(nil);
  try
    Assert(DOM.LoadDataSource('http://192.168.10.119/pub/xml/dream.xml'));
//    Assert(DOM.SaveToFile('http://www.tpx.turbopower.com/~Ben.Oram/xmltest/dreamin.xml'));

    Assert(DOM.LoadDataSource('http://192.168.10.119/pub/xml/Email.xml'));
//    Assert(DOM.SaveToFile('http://www.tpx.turbopower.com/~Ben.Oram/xmltest/Emailin.xml'));

    Assert(DOM.LoadDataSource('http://192.168.10.119/pub/xml/memo.xml'));
//    Assert(DOM.SaveToFile('http://www.tpx.turbopower.com/~Ben.Oram/xmltest/memoin.xml'));

    Assert(DOM.LoadDataSource('http://192.168.10.119/pub/xml/tire.xml'));
//    Assert(DOM.SaveToFile('http://www.tpx.turbopower.com/~Ben.Oram/xmltest/tirein.xml'));

    Assert(DOM.LoadDataSource('http://192.168.10.119/pub/xml/Weather.xml'));
//    Assert(DOM.SaveToFile('http://www.tpx.turbopower.com/~Ben.Oram/xmltest/Weatherin.xml'));

    { Test documents with relative paths. }
    Assert(DOM.LoadDataSource('http://192.168.10.119/pub/xml/john.xml'));
    Assert(DOM.LoadDataSource('http://192.168.10.119/pub/xml/john2.xml'));
    Assert(DOM.LoadDataSource('http://192.168.10.119/pub/xml/john3.xml'));
  finally
    DOM.Free;
  end;
end;

initialization
  RegisterTest('INet Tests', TXpINetTests);
end.
