program DocTest;

uses
{$IFDEF WIN32}
  Forms,
{$ENDIF}
{$IFDEF LINUX}
  QForms,
{$ENDIF}
  GUITestRunner {DUnitDialog},
  TestFramework,
  uDocTest in 'uDocTest.pas';

{$R *.res}

begin
  GUITestRunner.runRegisteredTests;
end.
