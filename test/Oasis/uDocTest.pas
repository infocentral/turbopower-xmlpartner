unit uDocTest;

interface
uses
{$IFDEF WIN32}
  Windows,
  Forms,
  uProgressW,
{$ENDIF}
{$IFDEF LINUX}
  QForms,
  uProgress,
{$ENDIF}
  TestFramework,
  SysUtils,
  Classes,
  XpParser,
  XpDom,
  XpBase;


type
  TXpDocTests = class(TTestCase)
    public
      procedure DoAttributes(oOwner     : TObject;
                             sName,
                             sValue     : DOMString;
                             bSpecified : Boolean);
    published
      procedure TestBadDocs;
      procedure TestBadDOM;
      procedure TestGoodDocs;
      procedure TestGoodDOM;
  end;


implementation
uses
  XpChrFlt,
  XpExcept;

procedure TXpDocTests.DoAttributes(oOwner     : TObject;
                                   sName,
                                   sValue     : DOMString;
                                   bSpecified : Boolean);
begin

end;
{--------}
procedure TXpDocTests.TestBadDocs;
var
  SR         : TSearchRec;
  FilePath   : string;
  FindResult : Boolean;
  Parser     : TXpParser;
  ParsedOK   : Boolean;
  FailedTest : Boolean;
  Log        : TStringList;
  LogName    : string;
  FileName   : string;
  FileCount  : Integer;
  Dlg        : TDlgProgress;

begin
  LogName := '';
  Log := nil;
  FailedTest := False;

{$IFDEF WIN32}
  FilePath := '..\Data\OasisBad\*.xml';
{$ENDIF}
{$IFDEF LINUX}
  FilePath := '../data/oasisbad/*.xml';
{$ENDIF}
  FindResult := FindFirst(FilePath, faAnyFile, SR) = 0;
  try
    Log := TStringList.Create;
    FileCount := 0;
    if FindResult then
      repeat
        inc(FileCount);
      until FindNext(SR) <> 0;
    SysUtils.FindClose(SR);
    Dlg := TDlgProgress.Create(nil);
    try
      Dlg.Show;
      Dlg.Max := FileCount;
      Application.ProcessMessages;

{$IFDEF WIN32}
      LogName := 'c:\BadGoneBad.txt';
{$ENDIF}
{$IFDEF LINUX}
      LogName := '/tmp/BadGoneBad.txt';
{$ENDIF}
      FindResult := FindFirst(FilePath, faAnyFile, SR) = 0;
      if FindResult then begin
        repeat
          if AnsiCompareText(ExtractFileExt(Sr.Name), '.xml') = 0 then begin
           {Update progressbar}
            Dlg.FileName := FileName;
            Dlg.Position := Dlg.Position + 1;

            {Verify that XML document is OK}
            Parser := TXpParser.Create(nil);
            Parser.OnAttribute := DoAttributes;
            try
{$IFDEF WIN32}
              FileName := '..\Data\OasisBad\' + Sr.Name;
{$ENDIF}
{$IFDEF LINUX}
              FileName := '../data/oasisbad/' + Sr.Name;
{$ENDIF}
              try
                ParsedOK := Parser.ParseDataSource(FileName);
              except
                ParsedOK := False;
              end;
              if ParsedOK then begin
                FailedTest := True;
                {Store error messages}
                Log.Add('-----------------------------------------------');
                Log.Add('  Parsed ok : ' + FileName);
                Log.Add('');
                Log.Add('-----------------------------------------------');
                Log.Add('');
                Log.Add('');
                Log.SaveToFile(LogName);

                {Update progress bar error messages}
                Dlg.AddError('-----------------------------------------------');
                Dlg.AddError('  Parsed ok : ' + FileName);
                Dlg.AddError('');
                Dlg.AddError('-----------------------------------------------');
                Dlg.AddError('');
                Dlg.AddError('');
              end;
            finally
              Parser.Free;
            end;
          end;
        until FindNext(SR) <> 0;
      end else
        Fail('Bad docs not found');
    finally
      Dlg.Hide;
      Dlg.Free;
    end;
  finally
    Log.Free;
    SysUtils.FindClose(SR);
  end;
  Assert(not FailedTest, 'At least one bad doc parsed well. See ' +
                         LogName + 'for info');
end;
{--------}
procedure TXpDocTests.TestBadDOM;
var
  SR         : TSearchRec;
  FilePath   : string;
  FindResult : Boolean;
  DOM        : TXpObjModel;
  ParsedOK   : Boolean;
  FailedTest : Boolean;
  Log        : TStringList;
  LogName    : string;
  FileName   : string;
  FileCount  : Integer;
  Dlg        : TDlgProgress;
begin
  LogName := '';
  Log := nil;
  FailedTest := False;
{$IFDEF WIN32}
  FilePath := '..\Data\OasisBad\*.xml';
{$ENDIF}
{$IFDEF LINUX}
  FilePath := '../data/oasisbad/*.xml';
{$ENDIF}
  FindResult := FindFirst(FilePath, faAnyFile, SR) = 0;
  try
    Log := TStringList.Create;
    FileCount := 0;
    if FindResult then
      repeat
        inc(FileCount);
      until FindNext(SR) <> 0;
    SysUtils.FindClose(SR);
    Dlg := TDlgProgress.Create(nil);
    try
      Dlg.Show;
      Dlg.Max := FileCount;
      Application.ProcessMessages;

{$IFDEF WIN32}
      LogName := 'c:\BadDOMGoneBad.txt';
{$ENDIF}
{$IFDEF LINUX}
      LogName := '/tmp/BadDOMGoneBad.txt';
{$ENDIF}
      FindResult := FindFirst(FilePath, faAnyFile, SR) = 0;
      if FindResult then begin
        repeat
          if AnsiCompareText(ExtractFileExt(Sr.Name), '.xml') = 0 then begin
           {Update progressbar}
            Dlg.FileName := FileName;
            Dlg.Position := Dlg.Position + 1;

            {Verify that XML document is OK}
            DOM := TXpObjModel.Create(nil);
            try
{$IFDEF WIN32}
              FileName := '..\Data\OasisBad\' + Sr.Name;
{$ENDIF}
{$IFDEF LINUX}
              FileName := '../data/oasisbad/' + Sr.Name;
{$ENDIF}
              try
                ParsedOK := DOM.LoadDataSource(FileName);
              except
                ParsedOK := False;
              end;
              if ParsedOK then begin
                FailedTest := True;
                {Store error messages}
                Log.Add('-----------------------------------------------');
                Log.Add('  Parsed ok : ' + FileName);
                Log.Add('');
                Log.Add('-----------------------------------------------');
                Log.Add('');
                Log.Add('');
                Log.SaveToFile(LogName);

                {Update progress bar error messages}
                Dlg.AddError('-----------------------------------------------');
                Dlg.AddError('  Parsed ok : ' + FileName);
                Dlg.AddError('');
                Dlg.AddError('-----------------------------------------------');
                Dlg.AddError('');
                Dlg.AddError('');
              end;
            finally
              DOM.Free;
            end;
          end;
        until FindNext(SR) <> 0;
      end else
        Fail('Bad docs not found');
    finally
      Dlg.Hide;
      Dlg.Free;
    end;
  finally
    Log.Free;
    SysUtils.FindClose(SR);
  end;
  Assert(not FailedTest, 'At least one bad doc parsed well. See ' +
                         LogName + 'for info');
end;
{--------}
procedure TXpDocTests.TestGoodDocs;
var
  SR         : TSearchRec;
  FilePath   : string;
  FindResult : Boolean;
  Parser     : TXpParser;
  ParsedOK   : Boolean;
  FailedTest : Boolean;
  Log        : TStringList;
  LogName    : string;
  Idx        : Integer;
  FileName   : string;
  FileCount  : Integer;
  Dlg        : TDlgProgress;

begin
  ParsedOK := False;
  LogName := '';
  Log := nil;
  FailedTest := False;
{$IFDEF WIN32}
  FilePath := '..\Data\OasisGood\*.xml';
{$ENDIF}
{$IFDEF LINUX}
  FilePath := '../data/oasisgood/*.xml';
{$ENDIF}
  FindResult := FindFirst(FilePath, faAnyFile, SR) = 0;
  try
    Log := TStringList.Create;
    FileCount := 0;
    if FindResult then
      repeat
        inc(FileCount);
      until FindNext(SR) <> 0;
    SysUtils.FindClose(SR);

    Dlg := TDlgProgress.Create(nil);
    try
      Dlg.Show;
      Dlg.Max := FileCount;
      Application.ProcessMessages;
{$IFDEF WIN32}
      LogName := 'c:\GoodGoneBad.txt';
{$ENDIF}
{$IFDEF LINUX}
      LogName := '/tmp/GoodGoneBad.txt';
{$ENDIF}

      FindResult := FindFirst(FilePath, faAnyFile, SR) = 0;
      if FindResult then begin
        repeat
          if AnsiCompareText(ExtractFileExt(Sr.Name), '.xml') = 0 then begin
            {Update progressbar}
            Dlg.FileName := FileName;
            Dlg.Position := Dlg.Position + 1;

            {Verify that XML document is OK}
            Parser := TXpParser.Create(nil);
            try
{$IFDEF WIN32}
              FileName :=   '..\Data\OasisGood\' + Sr.Name;
{$ENDIF}
{$IFDEF LINUX}
              FileName :=   '../data/oasisgood/' + Sr.Name;
{$ENDIF}
              try
                ParsedOK := Parser.ParseDataSource(FileName);
              except
                on E:Exception do
                  ParsedOK := False;
              end;
              if not ParsedOK then begin
                FailedTest := True;
                {Store error messages}
                Log.Add('-----------------------------------------------');
                Log.Add('  Error(s) parsing : ' + FileName);
                Log.Add('');
                for Idx := 0 to Pred(Parser.Errors.Count) do
                  Log.Add(Parser.Errors.Strings[Idx]);
                Log.Add('');
                Log.Add('-----------------------------------------------');
                Log.Add('');
                Log.Add('');
                Log.SaveToFile(LogName);

                Dlg.AddError('-----------------------------------------------');
                Dlg.AddError('  Error(s) parsing : ' + FileName);
                Dlg.AddError('');
                for Idx := 0 to Pred(Parser.Errors.Count) do
                  Dlg.AddError(Parser.Errors.Strings[Idx]);
                Dlg.AddError('');
                Dlg.AddError('-----------------------------------------------');
                Dlg.AddError('');
                Dlg.AddError('');
              end;
            finally
              Parser.Free;
            end;
          end;
        until FindNext(SR) <> 0;
      end else
        Fail('Good docs not found');
    finally
      Dlg.Hide;
      Dlg.Free;
    end;
  finally
    Log.Free;
    SysUtils.FindClose(SR);
  end;
  Assert(not FailedTest, 'At least one good doc didn''t parse well. See ' +
                         LogName + 'for info');
end;
{--------}
procedure TXpDocTests.TestGoodDOM;
var
  SR         : TSearchRec;
  FilePath   : string;
  FindResult : Boolean;
  DOM        : TXpObjModel;
  ParsedOK   : Boolean;
  FailedTest : Boolean;
  Log        : TStringList;
  LogName    : string;
  Idx        : Integer;
  FileName   : string;
  FileCount  : Integer;
  Dlg        : TDlgProgress;
begin
  ParsedOK := False;
  LogName := '';
  Log := nil;
  FailedTest := False;
{$IFDEF WIN32}
  FilePath := '..\Data\OasisGood\*.xml';
{$ENDIF}
{$IFDEF LINUX}
  FilePath := '../data/oasisgood/*.xml';
{$ENDIF}
  FindResult := FindFirst(FilePath, faAnyFile, SR) = 0;
  try
    Log := TStringList.Create;
    FileCount := 0;
    if FindResult then
      repeat
        inc(FileCount);
      until FindNext(SR) <> 0;
    SysUtils.FindClose(SR);

    Dlg := TDlgProgress.Create(nil);
    try
      Dlg.Show;
      Dlg.Max := FileCount;
      Application.ProcessMessages;
{$IFDEF WIN32}
      LogName := 'c:\GoodDOMGoneBad.txt';
{$ENDIF}
{$IFDEF LINUX}
      LogName := '/tmp/GoodDOMGoneBad.txt';
{$ENDIF}

      FindResult := FindFirst(FilePath, faAnyFile, SR) = 0;
      if FindResult then begin
        repeat
          if AnsiCompareText(ExtractFileExt(Sr.Name), '.xml') = 0 then begin
            {Update progressbar}
            Dlg.FileName := FileName;
            Dlg.Position := Dlg.Position + 1;

            {Verify that XML document is OK}
            DOM := TXpObjModel.Create(nil);
            try
{$IFDEF WIN32}
              FileName :=   '..\Data\OasisGood\' + Sr.Name;
{$ENDIF}
{$IFDEF LINUX}
              FileName :=   '../data/oasisgood/' + Sr.Name;
{$ENDIF}
              try
                ParsedOK := DOM.LoadDataSource(FileName);
              except
                on E:Exception do
                  ParsedOK := False;
              end;
              if not ParsedOK then begin
                FailedTest := True;
                {Store error messages}
                Log.Add('-----------------------------------------------');
                Log.Add('  Error(s) parsing : ' + FileName);
                Log.Add('');
                for Idx := 0 to Pred(DOM.Errors.Count) do
                  Log.Add(DOM.Errors.Strings[Idx]);
                Log.Add('');
                Log.Add('-----------------------------------------------');
                Log.Add('');
                Log.Add('');
                Log.SaveToFile(LogName);

                Dlg.AddError('-----------------------------------------------');
                Dlg.AddError('  Error(s) parsing : ' + FileName);
                Dlg.AddError('');
                for Idx := 0 to Pred(DOM.Errors.Count) do
                  Dlg.AddError(DOM.Errors.Strings[Idx]);
                Dlg.AddError('');
                Dlg.AddError('-----------------------------------------------');
                Dlg.AddError('');
                Dlg.AddError('');
              end;
            finally
              DOM.Free;
            end;
          end;
        until FindNext(SR) <> 0;
      end else
        Fail('Good docs not found');
    finally
      Dlg.Hide;
      Dlg.Free;
    end;
  finally
    Log.Free;
    SysUtils.FindClose(SR);
  end;
  Assert(not FailedTest, 'At least one good doc didn''t parse well. See ' +
                         LogName + 'for info');
end;

initialization
  RegisterTest('Doc Tests', TXpDocTests);
end.
