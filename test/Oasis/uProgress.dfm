object dlgProgress: TdlgProgress
  Left = 412
  Top = 244
  Width = 328
  Height = 376
  ActiveControl = Button1
  Caption = 'dlgProgress'
  Color = clButton
  Font.CharSet = fcsAnyCharSet
  Font.Color = clText
  Font.Height = 13
  Font.Name = 'MS Sans Serif'
  Font.Pitch = fpVariable
  Font.Style = []
  ParentFont = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 15
  TextWidth = 7
  object barProgress: TProgressBar
    Left = 8
    Top = 8
    Width = 305
    Height = 16
    Orientation = pbHorizontal
  end
  object lblProgress: TLabel
    Left = 8
    Top = 32
    Width = 52
    Height = 15
    Caption = 'Progress:'
    Layout = tlCenter
  end
  object lblFileName: TLabel
    Left = 8
    Top = 56
    Width = 56
    Height = 15
    Caption = 'FileName:'
    Layout = tlCenter
  end
  object Button1: TButton
    Left = 240
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Abort'
    TabOrder = 1
    OnClick = Button1Click
  end
  object memErrors: TMemo
    Left = 8
    Top = 112
    Width = 305
    Height = 233
    MaxLength = -1
    ScrollBars = ssVertical
    TabOrder = 2
  end
end
