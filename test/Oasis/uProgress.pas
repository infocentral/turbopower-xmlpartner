unit uProgress;

interface

uses
{$IFDEF WIN32}
  Windows,
  Forms,
  Messages,
  Graphics,
  Controls,
  Dialogs,
  StdCtrls,
  ComCtrls, 
{$ENDIF}
{$IFDEF LINUX}
  QForms,
  QStdCtrls,
  QControls,
  QComCtrls,
{$ENDIF}
  SysUtils,
  Classes;

type
  TdlgProgress = class(TForm)
    barProgress: TProgressBar;
    lblProgress: TLabel;
    Button1: TButton;
    lblFileName: TLabel;
    memErrors: TMemo;
    procedure Button1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FAbort: Boolean;
    function GetPosition : Integer;
    function GetMax : Integer;
    procedure SetPosition(const Value : Integer);
    procedure SetMax(const Value: Integer);
    procedure UpdateDisplay;
    function GetFileName: string;
    procedure SetFileName(const Value: string);
  public
    procedure AddError(const Line : string);
    property Position : Integer
       read GetPosition
       write SetPosition;
    property Max : Integer
       read GetMax
       write SetMax;
    property Abort : Boolean
       read FAbort;
    property FileName : string
       read GetFileName
       write SetFileName;   
  end;

var
  dlgProgress: TdlgProgress;

implementation

{$R *.dfm}

procedure TdlgProgress.Button1Click(Sender: TObject);
begin
  FAbort := False;
end;

function TdlgProgress.GetMax: Integer;
begin
  Result := barProgress.Max;
end;

function TdlgProgress.GetPosition: Integer;
begin
  Result := barProgress.Position;
end;

procedure TdlgProgress.SetMax(const Value: Integer);
begin
  barProgress.Max := Value;
  UpdateDisplay;
end;

procedure TdlgProgress.SetPosition(const Value: Integer);
begin
  barProgress.Position := Value;
  UpdateDisplay;
end;

procedure TdlgProgress.FormCreate(Sender: TObject);
begin
  FAbort := False;
  barProgress.Max := 0;
  barProgress.Position := 0;
end;

procedure TdlgProgress.UpdateDisplay;
begin
  lblProgress.Caption := Format('Completed %d of %d',
                           [barProgress.Position, barProgress.Max]);
  Application.ProcessMessages;
end;

function TdlgProgress.GetFileName: string;
begin
  Result := lblFileName.Caption;
end;

procedure TdlgProgress.SetFileName(const Value: string);
begin
  lblFileName.Caption := Format('FileName: %s', [Value]);
end;

procedure TdlgProgress.AddError(const Line: string);
begin
  memErrors.Lines.Add(Line);
end;

end.
