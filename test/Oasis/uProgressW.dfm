object dlgProgress: TdlgProgress
  Left = 412
  Top = 244
  Width = 328
  Height = 376
  ActiveControl = Button1
  Caption = 'dlgProgress'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = 13
  Font.Name = 'MS Sans Serif'
  Font.Pitch = fpVariable
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lblProgress: TLabel
    Left = 7
    Top = 28
    Width = 44
    Height = 13
    Caption = 'Progress:'
    Layout = tlCenter
  end
  object lblFileName: TLabel
    Left = 7
    Top = 49
    Width = 47
    Height = 13
    Caption = 'FileName:'
    Layout = tlCenter
  end
  object barProgress: TProgressBar
    Left = 7
    Top = 7
    Width = 264
    Height = 14
    Min = 0
    Max = 100
    TabOrder = 0
  end
  object Button1: TButton
    Left = 208
    Top = 62
    Width = 65
    Height = 22
    Caption = 'Abort'
    TabOrder = 1
    OnClick = Button1Click
  end
  object memErrors: TMemo
    Left = 7
    Top = 97
    Width = 264
    Height = 202
    MaxLength = -1
    ScrollBars = ssVertical
    TabOrder = 2
  end
end
