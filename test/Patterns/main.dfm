object Form1: TForm1
  Left = 189
  Top = 103
  Width = 342
  Height = 347
  ActiveControl = efPattern
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lblTokens: TLabel
    Left = 8
    Top = 8
    Width = 36
    Height = 13
    Caption = 'Tokens'
  end
  object lblEnter: TLabel
    Left = 8
    Top = 240
    Width = 73
    Height = 13
    Caption = 'Enter a pattern:'
  end
  object pbNewParser: TButton
    Left = 86
    Top = 288
    Width = 75
    Height = 25
    Caption = '&New parser'
    Default = True
    TabOrder = 2
    OnClick = pbNewParserClick
  end
  object lbTokens: TListBox
    Left = 8
    Top = 24
    Width = 321
    Height = 201
    ItemHeight = 13
    TabOrder = 0
  end
  object efPattern: TEdit
    Left = 8
    Top = 256
    Width = 321
    Height = 21
    TabOrder = 1
  end
  object pbParserOld: TButton
    Left = 166
    Top = 288
    Width = 75
    Height = 25
    Caption = '&Old parser'
    TabOrder = 3
    OnClick = pbParserOldClick
  end
  object DOM: TXpObjModel
    BufferSize = 8192
    IdAttribute = 'id'
    Password = 'xmlpartner@turbopower.com'
    UserName = 'anonymous'
    Left = 192
    Top = 32
  end
end
