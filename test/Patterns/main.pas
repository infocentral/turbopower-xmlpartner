unit main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, XpBase, XpDom;

type
  TForm1 = class(TForm)
    DOM: TXpObjModel;
    pbNewParser: TButton;
    lbTokens: TListBox;
    lblTokens: TLabel;
    efPattern: TEdit;
    lblEnter: TLabel;
    pbParserOld: TButton;
    procedure pbNewParserClick(Sender: TObject);
    procedure pbParserOldClick(Sender: TObject);
  private
    { Private declarations }
    procedure Load(aList : TXpNodeList);
    function MapTokenToString(oNode : TXpXQLToken) : string;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

function TForm1.MapTokenToString(oNode : TXpXQLToken) : string;
begin
  case oNode.TokenId of
    TOK_NUMBER : Result := 'TOK_NUMBER';
    TOK_STRING_TYPE : Result := 'TOK_STRING_TYPE';
    TOK_ELEMENT : Result := 'TOK_ELEMENT';
    TOK_ATTRIBUTE : Result := 'TOK_ATTRIBUTE';
    TOK_OPSUM : Result := 'TOK_OPSUM';
    TOK_OPDIFF : Result := 'TOK_OPDIFF';
    TOK_OPMUL : Result := 'TOK_OPMUL';
    TOK_OPDIV : Result := 'TOK_OPDIV';
    TOK_OPCONCAT : Result := 'TOK_OPCONCAT';
    TOK_OPMOD : Result := 'TOK_OPMOD';
    TOK_WILD : Result := 'TOK_WILD';
    TOK_COMMA : Result := 'TOK_COMMA';
    TOK_LPAREN : Result := 'TOK_LPAREN';
    TOK_RPAREN : Result := 'TOK_RPAREN';
    TOK_BANG : Result := 'TOK_BANG';
    TOK_SLASH : Result := 'TOK_SLASH';
    TOK_2SLASH : Result := 'TOK_2SLASH';
    TOK_DOT : Result := 'TOK_DOT';
    TOK_2DOT : Result := 'TOK_2DOT';
    TOK_LFRAME : Result := 'TOK_LFRAME';
    TOK_RFRAME : Result := 'TOK_RFRAME';
    TOK_LBRACK : Result := 'TOK_LBRACK';
    TOK_RBRACK : Result := 'TOK_RBRACK';
    TOK_TO : Result := 'TOK_TO';
    TOK_AND : Result := 'TOK_AND';
    TOK_OR : Result := 'TOK_OR';
    TOK_NOT : Result := 'TOK_NOT';
    TOK_EQ : Result := 'TOK_EQ';
    TOK_NE : Result := 'TOK_NE';
    TOK_LT : Result := 'TOK_LT';
    TOK_LE : Result := 'TOK_LE';
    TOK_GT : Result := 'TOK_GT';
    TOK_GE : Result := 'TOK_GE';
    TOK_UNION : Result := 'TOK_UNION';
    TOK_INTERSECT : Result := 'TOK_INTERSECT';
    TOK_CIEQ : Result := 'TOK_CIEQ';
    TOK_CINE : Result := 'TOK_CINE';
    TOK_CILT : Result := 'TOK_CILT';
    TOK_CILE : Result := 'TOK_CILE';
    TOK_CIGT : Result := 'TOK_CIGT';
    TOK_CIGE : Result := 'TOK_CIGE';
    TOK_ANCESTOR : Result := 'TOK_ANCESTOR';
    TOK_ANCESTOR_OR_SELF : Result := 'TOK_ANCESTOR_OR_SELF';
    TOK_ID : Result := 'TOK_ID';
    TOK_TRUE : Result := 'TOK_TRUE';
    TOK_FALSE : Result := 'TOK_FALSE';
    TOK_DATE : Result := 'TOK_DATE';
    TOK_IVALUE : Result := 'TOK_IVALUE';
    TOK_INODETYPE : Result := 'TOK_INODETYPE';
    TOK_INODENAME : Result := 'TOK_INODENAME';
    TOK_IINDEX : Result := 'TOK_IINDEX';
    TOK_ICOUNT : Result := 'TOK_ICOUNT';
    TOK_INODETYPESTRING : Result := 'TOK_INODETYPESTRING';
    TOK_CMTEXTNODE : Result := 'TOK_CMTEXTNODE';
    TOK_CMCOMMENT : Result := 'TOK_CMCOMMENT';
    TOK_CMPI : Result := 'TOK_CMPI';
    TOK_CMELEMENT : Result := 'TOK_CMELEMENT';
    TOK_CMATTRIBUTE : Result := 'TOK_CMATTRIBUTE';
    TOK_CMNODE : Result := 'TOK_CMNODE';
    TOK_FARG : Result := 'TOK_FARG';
    TOK_FCONSTANT : Result := 'TOK_FCONSTANT';
    TOK_FVAR : Result := 'TOK_FVAR';
    TOK_FIRSTOFTYPE : Result := 'TOK_FIRSTOFTYPE';
    TOK_NOTFIRSTOFTYPE : Result := 'TOK_NOTFIRSTOFTYPE';
    TOK_LASTOFTYPE : Result := 'TOK_LASTOFTYPE';
    TOK_NOTLASTOFTYPE : Result := 'TOK_NOTLASTOFTYPE';
    TOK_ONLYOFTYPE : Result := 'TOK_ONLYOFTYPE';
    TOK_NOTONLYOFTYPE : Result := 'TOK_NOTONLYOFTYPE';
    TOK_FIRSTOFANY : Result := 'TOK_FIRSTOFANY';
    TOK_NOTFIRSTOFANY : Result := 'TOK_NOTFIRSTOFANY';
    TOK_LASTOFANY : Result := 'TOK_LASTOFANY';
    TOK_NOTLASTOFANY : Result := 'TOK_NOTLASTOFANY';
    TOK_ONLYOFANY : Result := 'TOK_ONLYOFANY';
    TOK_NOTONLYOFANY : Result := 'TOK_NOTONLYOFANY';
    TOK_AXIS_ANCESTOR : Result := 'TOK_AXIS_ANCESTOR';
    TOK_AXIS_ANCESTOR_OR_SELF : Result := 'TOK_AXIS_ANCESTOR_OR_SELF';
    TOK_AXIS_ATTRIBUTE : Result := 'TOK_AXIS_ATTRIBUTE';
    TOK_AXIS_CHILD : Result := 'TOK_AXIS_CHILD';
    TOK_AXIS_DESCENDANT : Result := 'TOK_AXIS_DESCENDANT';
    TOK_AXIS_DESCENDANT_OR_SELF : Result := 'TOK_AXIS_DESCENDANT_OR_SELF';
    TOK_AXIS_FOLLOWING : Result := 'TOK_AXIS_FOLLOWING';
    TOK_AXIS_FOLLOWING_SIBLING : Result := 'TOK_AXIS_FOLLOWING_SIBLING';
    TOK_AXIS_NAMESPACE : Result := 'TOK_AXIS_NAMESPACE';
    TOK_AXIS_PARENT : Result := 'TOK_AXIS_PARENT';
    TOK_AXIS_PRECEDING : Result := 'TOK_AXIS_PRECEDING';
    TOK_AXIS_PRECEDING_SIBLING : Result := 'TOK_AXIS_PRECEDING_SIBLING';
    TOK_AXIS_SELF : Result := 'TOK_AXIS_SELF';
    TOK_LAST : Result := 'TOK_LAST';
    TOK_POSITION : Result := 'TOK_POSITION';
    TOK_KEY : Result := 'TOK_KEY';
    TOK_DOC : Result := 'TOK_DOC';
    TOK_DOCREF : Result := 'TOK_DOCREF';
    TOK_LOCAL_PART : Result := 'TOK_LOCAL_PART';
    TOK_NAMESPACE : Result := 'TOK_NAMESPACE';
    TOK_QNAME : Result := 'TOK_QNAME';
    TOK_NAMESPACEURI : Result := 'TOK_NAMESPACEURI';
    TOK_GENERATEID : Result := 'TOK_GENERATEID';
    TOK_BOOLEAN : Result := 'TOK_BOOLEAN';
    TOK_LANG : Result := 'TOK_LANG';
    TOK_OPQUO : Result := 'TOK_OPQUO';
    TOK_FLOOR : Result := 'TOK_FLOOR';
    TOK_CEILING : Result := 'TOK_CEILING';
    TOK_ROUND : Result := 'TOK_ROUND';
    TOK_STRING_CAST : Result := 'TOK_STRING_CAST';
    TOK_STARTS_WITH : Result := 'TOK_STARTS_WITH';
    TOK_CONTAINS : Result := 'TOK_CONTAINS';
    TOK_STRING_LENGTH : Result := 'TOK_STRING_LENGTH';
    TOK_SUBSTRING : Result := 'TOK_SUBSTRING';
    TOK_SUBSTRING_BEFORE : Result := 'TOK_SUBSTRING_BEFORE';
    TOK_SUBSTRING_AFTER : Result := 'TOK_SUBSTRING_AFTER';
    TOK_NORMALIZE : Result := 'TOK_NORMALIZE';
    TOK_TRANSLATE : Result := 'TOK_TRANSLATE';
    TOK_FORMAT_NUMBER : Result := 'TOK_FORMAT_NUMBER';
    TOK_FUNCTION_AVAILABLE : Result := 'TOK_FUNCTION_AVAILABLE';
    TOK_OPUNARY : Result := 'TOK_OPUNARY';
    TOK_VARIABLE : Result := 'TOK_VARIABLE';
    TOK_NUMBER_CAST : Result := 'TOK_NUMBER_CAST';
    TOK_SUM : Result := 'TOK_SUM';
    TOK_FCONCAT : Result := 'TOK_FCONCAT';
    TOK_SYSTEM_PROPERTY : Result := 'TOK_SYSTEM_PROPERTY';
    TOK_FUNCTION : Result := 'TOK_FUNCTION';
    TOK_VARIABLE_INDIRECT : Result := 'TOK_VARIABLE_INDIRECT';
    else
      Result := format('<Unrecognized token: %d>',[oNode.TokenID]);
  end;  { case }
  Result := Result + ' : ' + oNode.NodeName;
end;

procedure TForm1.pbNewParserClick(Sender: TObject);
var
  aList : TXpNodeList;
  aParser : TXpXPathParser;
begin
  aParser := TXpXPathParser.Create;
  try
    aList := aParser.Tokenize(efPattern.Text);
    Load(aList);
  finally
    aParser.Free;
  end;
end;

procedure TForm1.Load(aList : TXpNodeList);
var
  anInx : Integer;
begin
  lbTokens.Clear;
  for anInx := 0 to Pred(aList.Length) do
    lbTokens.Items.Add(MapTokenToString(TXpXQLToken(aList.Item(anInx))));
end;

procedure TForm1.pbParserOldClick(Sender: TObject);
var
  aList : TXpNodeList;
  aExpr : DOMString;
begin
  aExpr := efPattern.Text;
  aList := ParseXQLExpr(aExpr);
  Load(aList);
end;

end.
