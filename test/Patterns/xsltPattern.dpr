program xsltPattern;

uses
{$IFDEF WIN32}
  Forms,
{$ENDIF}
{$IFDEF LINUX}
  QForms,
{$ENDIF}
  GUITestRunner {DUnitDialog},
  TestFramework,
  uXSLTPattern in 'uXSLTPattern.pas';

{$R *.res}

begin
  GUITestRunner.runRegisteredTests;
end.
