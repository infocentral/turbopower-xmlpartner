program testXpath;

uses
{$IFDEF WIN32}
  Forms,
{$ENDIF}
{$IFDEF LINUX}
  QForms,
{$ENDIF}
  GUITestRunner {DUnitDialog},
  TestFramework,
  xpathmain in 'xpathmain.pas';

{$R *.res}

begin
  GUITestRunner.runRegisteredTests;
end.
