program XSLTTest;

{$I XpDefine.inc}

uses
{$IFDEF UsingCLX}
  QForms,
{$ELSE}
  Forms,
{$ENDIF}
{$IFDEF LINUX}
  testhash in '../hash/testhash.pas',
{$ELSE}
  testhash in '..\hash\testhash.pas',
{$ENDIF}
  GUITestRunner {DUnitDialog},
  TestFramework,
  uXSLTTest in 'uXSLTTest.pas',
  uDocCacheTest in 'uDocCacheTest.pas';

{$R *.res}

begin
  GUITestRunner.runRegisteredTests;
end.
