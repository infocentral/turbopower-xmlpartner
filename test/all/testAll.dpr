program testAll;

uses
{$IFDEF WIN32}
  Forms,
{$ENDIF}
{$IFDEF LINUX}
  QForms,
{$ENDIF}
  GUITestRunner {DUnitDialog},
  TestFramework,
{$IFDEF WIN32}
  DOMmain in '..\dom\DOMmain.pas',
  xpathmain in '..\xpath\xpathmain.pas',
  uBugTest in '..\bugtests\uBugTest.pas',
  uDocTest in '..\oasis\uDocTest.pas',
  uFltrTst in '..\charfilt\uFltrTst.pas',
  uINetTest in '..\internet\uINetTest.pas',
  UXSLTPattern in '..\patterns\uXSLTPattern.pas',
  uXSLTTest in '..\xslt\uXSLTTest.pas',
  uProgressW in '..\oasis\uProgressW.pas' {dlgProgress},
  uXSLFOTest in '..\xslfo\uXSLFOTest.pas',
  uDocCacheTest in '..\xslt\uDocCacheTest.pas';
{$ENDIF}
{$IFDEF LINUX}
  xpathmain in '../xpath/xpathmain.pas',
  uBugTest in '../bugtests/uBugTest.pas',
  uDocTest in '../oasis/uDocTest.pas',
  uFltrTst in '../charfilt/uFltrTst.pas',
  uINetTest in '../internet/uINetTest.pas',
  UXSLTPattern in '../patterns/uXSLTPattern.pas',
  uXSLTTest in '../xslt/uXSLTTest.pas',
  uProgress in '../oasis/uProgress.pas' {dlgProgress},
  DOMmain in '../dom/DOMmain.pas',
  uDocCacheTest in '../xslt/uDocCacheTest.pas';
{$ENDIF}

{$R *.res}

begin
  GUITestRunner.runRegisteredTests;
end.
