program bugtests;

uses
{$IFDEF WIN32}
  Forms,
{$ENDIF}
{$IFDEF LINUX}
  QForms,
{$ENDIF}
  GUITestRunner {DUnitDialog},
  TestFramework,
  uBugTest in 'uBugTest.pas';

{$R *.res}

begin
  GUITestRunner.runRegisteredTests;
end.
