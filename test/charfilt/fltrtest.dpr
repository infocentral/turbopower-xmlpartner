program fltrtest;

uses
{$IFDEF WIN32}
  Forms,
{$ENDIF}
{$IFDEF LINUX}
  QForms,
{$ENDIF}
  GUITestRunner {DUnitDialog},
  TestFramework,
  uFltrTst in 'uFltrTst.pas';

{$R *.res}

begin
  GUITestRunner.runRegisteredTests;
end.
