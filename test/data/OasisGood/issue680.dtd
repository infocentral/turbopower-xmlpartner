<!-- 
"Copyright The HR-XML Consortium. All Rights Reserved. http://www.hr-xml.org"

This HR-XML Consortium Work (including specifications, documents, 
software, and related items) is provided by the copyright holders 
under the following license. By obtaining, using and/or copying 
this work, you (the licensee) agree that you have read, understood, 
and will comply with the following terms and conditions.

Permission to use, copy, modify, or redistribute this Work and 
its documentation, with or without modification, for any purpose 
and without fee or royalty is hereby granted, provided that you 
include the following on ALL copies of the software and 
documentation or portions thereof, including modifications, 
that you make: 

1. This notice: "Copyright The HR-XML Consortium. All Rights 
   Reserved. http://www.hr-xml.org" 
2. Notice of any changes or modifications to the The HR-XML 
   Consortium files.
   
THIS WORK, INCLUDING SPECIFICATIONS, DOCUMENTS, SOFTWARE, OR OTHER 
RELATED ITEMS, IS PROVIDED "AS IS," AND COPYRIGHT HOLDERS MAKE NO 
REPRESENTATIONS OR WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT 
NOT LIMITED TO, WARRANTIES OF MERCHANTABILITY OR FITNESS FOR ANY 
PARTICULAR PURPOSE OR THAT THE USE OF THE SOFTWARE OR DOCUMENTATION 
WILL NOT INFRINGE ANY THIRD PARTY PATENTS, COPYRIGHTS, TRADEMARKS 
OR OTHER RIGHTS. 

COPYRIGHT HOLDERS WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, 
SPECIAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF ANY USE OF THE 
SOFTWARE OR DOCUMENTATION. 

TITLE TO COPYRIGHT IN THIS WORK AND ANY ASSOCIATED DOCUMENTATION 
WILL AT ALL TIMES REMAIN WITH COPYRIGHT HOLDERS. 
-->
<!--8/17/2000 Included PersonName module -->
<!--8/17/2000 Removed DT4DTD syntax per TSC decision -->
<!--9/05/2000 

1. Made Changes With Respect to Job/Position Dependencies:

JobSeeker        to    JobPositionSeeker
jobSeekerId      to    jobPositionSeekerId


2. Per decision at 8/24/2000 workgroup meetings

* Changed content model of JobPositionLocation to:

  Address | LocationSummary | summaryText 

* Create following content model for "LocationSummary":

city? , countyDistrictRegion? , stateProvince? , country?, postalCode?
        
3. Inserted PersonName Module in-place, removed parameter entity

4. Fixed typo. Changed locationDecription to locationDescription 
-->
<!--9/06/2000 
Removed tech-specific enumerations from type attribute of 
Qualification tag...

Was:
skill | experience | software | prgmLang | education | license |
certification | hardware | equipment | operatingSystem | other

Changed to:
skill | experience | education | license | certification | equipment | other
-->
<!--9/08/2000 
added status attribute to JobPositionSeeker
-->
<!--9/29/2000 
added category attribute to qualification and skills elements
-->
<!--10/06/2000 
added Application element with JobPositionId attribute to tie the seeker to a posting.
-->
<!--10/26/2000 Added approved CPO PostalAddress and PersonName v01 modules 
-->
<!--11/5/2000 Changed all elements to UpperCamelCase 
-->
<!-- A collection of information describing a JobPositionSeeker. A JobPositionSeeker is a
person who is seeking, or is being considered for, a position. -->
<!---->
<!--A collection of information describing a person who is seeking, or is being considered for a job/position. -->
<!ELEMENT JobPositionSeeker  (JobPositionSeekerId , JobPositionApplication* , Supplier? , PostDetail? , PersonalData , Profile? , Resume? , EmploymentReferences? )>
<!ATTLIST JobPositionSeeker  status  (active | inactive )  'active' >
<!--A unique code identifying the job/position seeker.-->
<!ELEMENT JobPositionSeekerId  (#PCDATA )>

<!--A method of tying a seeker to a position.-->
<!ELEMENT JobPositionApplication  (DateApplied? , SummaryText? )>

<!ELEMENT DateApplied  (Date )>

<!--An employment qualification.
The "type" attribute describes the qualification.
"description" is used to provide qualifying metadata when the type attribute has a value of "other."
"yearsOfExperience" can be used to specify the job/position seeker's years experience with regard to the qualification.
"level" is used to specify the job/position seeker's degree of expertise with respect to the qualification. This expertise is expressed on a scale of "1" to "5". "5" represents the highest level of expertise and "1" represents the lowest level.
"interest" is used to specify the job/position seeker's interest with respect to the qualification. The level of interest is expressed on a scale of "1" to "5". "5" represents the highest level of interest and "1" represents the lowest level.
"yearLastUsed" is used to specify the year the job/position seeker last used the qualification.-->
<!ELEMENT Qualification  (#PCDATA )>
<!ATTLIST Qualification  type               (skill | 
                                             experience | 
                                             education | 
                                             license | 
                                             certification | 
                                             equipment | 
                                             other )  #IMPLIED
                         description       CDATA  #IMPLIED
                         yearsOfExperience CDATA  #IMPLIED
                         level              (1 | 2 | 3 | 4 | 5 )  #IMPLIED
                         interest           (1 | 2 | 3 | 4 | 5 )  #IMPLIED
                         yearLastUsed      CDATA  #IMPLIED
                         source            CDATA  #IMPLIED
                         category          CDATA  #IMPLIED >
<!--CPO PostalAddress Module. -->
<!--The extended addressType attribute describes the type of an address. For example, "home", "work" and "temporary" are possible values for addressType.-->
<!ELEMENT PostalAddress  (CountryCode , PostalCode? , Region* , Municipality? , DeliveryAddress? , Recipient* )>
<!ATTLIST PostalAddress  type         (postOfficeBoxAddress | streetAddress | undefined )  'undefined'
                         addressType CDATA  #IMPLIED >
<!ELEMENT PostalCode  (#PCDATA )>

<!ELEMENT CountryCode  (#PCDATA )>

<!ELEMENT Region  (#PCDATA )>

<!ELEMENT Municipality  (#PCDATA )>

<!ELEMENT DeliveryAddress  (AddressLine* )>

<!ELEMENT AddressLine  (#PCDATA )>

<!ELEMENT Recipient  (PersonName? , AdditionalText* , Organization? )>

<!ELEMENT AdditionalText  (#PCDATA )>

<!ELEMENT Organization  (#PCDATA )>

<!--End CPO PostalAddress Module. -->
<!--A descriptive field.-->
<!ELEMENT SummaryText  (#PCDATA | Link )*>

<!--A paragraph.-->
<!ELEMENT P  (#PCDATA | Link | Img | Qualification )*>

<!--An unordered list. -->
<!ELEMENT UL  (LI+ )>

<!--A list item.-->
<!ELEMENT LI  (#PCDATA | Link | Img | Qualification )*>

<!--A hyperlink to an URL or an e-mail.-->
<!ELEMENT Link  (#PCDATA )>
<!ATTLIST Link  linkEnd CDATA  #IMPLIED
                mailTo  CDATA  #IMPLIED
                idRef   IDREF  #IMPLIED >
<!--An image, such as a .gif.-->
<!ELEMENT Img EMPTY>
<!ATTLIST Img  src       CDATA  #REQUIRED
               width     CDATA  #IMPLIED
               height    CDATA  #IMPLIED
               alt       CDATA  #IMPLIED
               mediaType CDATA  #IMPLIED >
<!--A collection of information of the type customarily provided in a Resume. -->
<!ELEMENT Resume  (StructuredResume | FreeFormResume | TextOrNonXMLResume )+>

<!--Accomodate plain-text resumes or links to non-XML resumes. For example, an HTML resume or a resume formatted using word processing or publishing software. -->
<!ELEMENT TextOrNonXMLResume  (TextResume | ResumeLink )>

<!--An HTML resume or a resume formatted using word processing or publishing software. -->
<!ELEMENT TextResume  (#PCDATA )>

<!--A link to a non-XML resume. For example, an HTML resume or a resume formatted using word processing or publishing software. -->
<!ELEMENT ResumeLink  (Link , SummaryText? )>

<!--Designed for tightly structured XML resumes. It may be most appropriate for "create-your-resume-online" or "apply-on-line" applications.-->
<!ELEMENT StructuredResume  (Objective | EmploymentHistory | EducationQualifs | CertificationQualifs | LicenseQualifs | QualifSummary | SkillQualifs | ProfessionalAssociations | SummaryText )+>

<!--Describes the job/position seeker's professional objectives.-->
<!ELEMENT Objective  (#PCDATA )>

<!--A job/position seeker's employment history. -->
<!ELEMENT EmploymentHistory  (Position+ )>

<!--Used in a structured resume to record information about the job/position seekers' education. -->
<!ELEMENT EducationQualifs  (SchoolOrInstitution+ )>

<!--Contains information regarding an educational qualification of a job/position seeker. -->
<!ELEMENT SchoolOrInstitution  (SchoolName , LocationSummary? , Department? , EduDegree? , EduMajor? , EduMinor? , GPA? , EffectiveDate? , SummaryText? )>

<!--The name of a school or educational institution.-->
<!ELEMENT SchoolName  (#PCDATA )>

<!--Information on where an employer or job/position was located. This information typically would include a city and state name.-->
<!ELEMENT LocationSummary  (Municipality? , Region* , CountryCode? , PostalCode? )>

<!--The name of the applicable departmental office.-->
<!ELEMENT Department  (#PCDATA )>

<!--The name of a degree granted by an educational institution.-->
<!ELEMENT EduDegree  (#PCDATA )>

<!--The name of "major" degree or field of study at an educational institution.-->
<!ELEMENT EduMajor  (#PCDATA )>

<!--The name of "minor" degree or field of study at an educational institution.-->
<!ELEMENT EduMinor  (#PCDATA )>

<!--Grade point average. A scale attribute can be used to give context to the gpa.-->
<!ELEMENT GPA  (#PCDATA )>

<!--Used in a structured resume to record information about certifications held by the job/position seeker. -->
<!ELEMENT CertificationQualifs  (Certification+ )>

<!--Used in a structured resume to record information about a single certification held by the job/position seeker; e.g., Microsoft Certified System Engineer. -->
<!ELEMENT Certification  (CertificationName , EffectiveDate? , SummaryText? )>

<!--Contains information on licenses held by the job/position seeker. -->
<!ELEMENT LicenseQualifs  (License+ )>

<!--A brief summary of the job/position seeker's qualifications.-->
<!ELEMENT QualifSummary  (#PCDATA )>

<!--Information about a job/position seeker's skill qualifications. -->
<!ELEMENT SkillQualifs  (Skill+ )>

<!--Used in a structured resume to record information about professional associations in which the job/position seeker holds membership. -->
<!ELEMENT ProfessionalAssociations  (Association+ )>

<!--A job/position title.-->
<!ELEMENT PositionTitle  (#PCDATA )>

<!--Information about a job/position held by the job/position seeker. -->
<!ELEMENT Position  (EmployerName , JobPositionLocation? , PositionTitle? , Industry? , EmployerSize? , EffectiveDate , SummaryText? )>

<!--Information about where a job/position is located or location-related requirements. -->
<!ELEMENT JobPositionLocation  (PostalAddress | LocationSummary | SummaryText )>

<!--The name of a certification. For example, "Certified Employee Benefits Specialist" or "Microsoft Certified System Engineer."-->
<!ELEMENT CertificationName  (#PCDATA )>

<!--The name of a skill.-->
<!ELEMENT SkillName  (#PCDATA )>

<!--The name of a license held by the job/position seeker.-->
<!ELEMENT LicenseName  (#PCDATA )>

<!--Information regarding a license, including the license name and optionally an effective date and summary text. -->
<!ELEMENT License  (LicenseName , EffectiveDate? , SummaryText? )>

<!--Information relating to a single skill qualification. -->
<!ELEMENT Skill  (SkillName , SummaryText? )>
<!ATTLIST Skill  yearsOfExperience CDATA  #IMPLIED
                 level              (1 | 2 | 3 | 4 | 5 )  #IMPLIED
                 interest           (1 | 2 | 3 | 4 | 5 )  #IMPLIED
                 yearLastUsed      CDATA  #IMPLIED
                 category          CDATA  #IMPLIED >
<!--Used in a structured resume to record information about association memberships. -->
<!ELEMENT Association  (AssociationName , EffectiveDate? , SummaryText? )>

<!--The name of a trade or professional association.-->
<!ELEMENT AssociationName  (#PCDATA )>

<!--Designed to accommodate "legacy" or "unstructured" resumes. -->
<!ELEMENT FreeFormResume  (RevisionDate? , ResumeSection+ )>

<!--The FreeFormResume is designed to accomodate many unstructured legacy resumes. A ResumeSection is the major structural component of the FreeFormResume-->
<!ELEMENT ResumeSection  (SectionTitle? | SubTitle? | SecBody )>
<!ATTLIST ResumeSection  secType  (objective | 
                                   experience | 
                                   personal | 
                                   education | 
                                   certifications | 
                                   licenses | 
                                   qualifSummary | 
                                   skills | 
                                   profAssociations | 
                                   unspecified )  'unspecified' >
<!--The title of a ResumeSection.-->
<!ELEMENT SectionTitle  (#PCDATA | PositionTitle | PostalAddress | VoiceNumber | FaxNumber | PagerNumber | TTDNumber | E-mail | EmployerName | Qualification | StartDate | EndDate | Date )*>

<!--A secondary title within a ResumeSection -->
<!ELEMENT SubTitle  (#PCDATA | PositionTitle | PostalAddress | VoiceNumber | FaxNumber | PagerNumber | TTDNumber | E-mail | EmployerName | Qualification | StartDate | EndDate | Date )*>

<!--The body of a ResumeSection . Contains descriptive sections. -->
<!ELEMENT SecBody  (P | UL | ResumeSection )>

<!-- CPO PersonName Module. The name of a person.-->
<!ELEMENT PersonName  (FormattedName* , GivenName* , PreferredGivenName? , MiddleName? , FamilyName* , Affix* )>

<!ELEMENT FormattedName  (#PCDATA )>
<!ATTLIST FormattedName  type  (presentation | legal | sortOrder )  'presentation' >
<!ELEMENT GivenName  (#PCDATA )>

<!ELEMENT PreferredGivenName  (#PCDATA )>

<!ELEMENT MiddleName  (#PCDATA )>

<!ELEMENT FamilyName  (#PCDATA )>
<!ATTLIST FamilyName  primary  (true | false | undefined )  'undefined' >
<!ELEMENT Affix  (#PCDATA )>
<!ATTLIST Affix  type  (academicGrade | 
                        aristocraticPrefix | 
                        aristocraticTitle | 
                        familyNamePrefix | 
                        familyNameSuffix | 
                        formOfAddress | 
                        generation )  #REQUIRED >
<!-- End CPO PersonName Module. -->
<!--The name of a person and sufficient information to contact him or her. -->
<!ELEMENT Contact  (PersonName? , PositionTitle? , PostalAddress* ,  (VoiceNumber | FaxNumber | PagerNumber | TTDNumber )* , E-mail* , WebSite* )>
<!ATTLIST Contact  type CDATA  #IMPLIED >
<!--An e-mail address.-->
<!ELEMENT E-mail  (#PCDATA )>

<!--An URL for a web site.-->
<!ELEMENT WebSite  (#PCDATA )>

<!--A date. Dates are represented in accordance with ISO 8601. 
Date: 			YYYY-MM-DD: 	
Time: 			HH:MM:SS: 	
International Time : 	HH:MM:SSZ: 	UTC (Universal Coordinated Time)
Time zone: 		HH:MM:SS-HHMM: 	Includes addition/subtraction to bring the time to UTC
Combined date/time: 	YYYY-MM-DDTHH:MM:SS: 	
-->
<!ELEMENT Date  (#PCDATA )>

<!--A start date, such as the date an individual began employment at a particular employer. -->
<!ELEMENT StartDate  (Date )>

<!--An end date. For instance, the date marking the end of a period during which a job/position should be advertise or the end of a period of employment for a particular employer.-->
<!ELEMENT EndDate  ( (Date | CurrentFlag ) , SummaryText? )>

<!--Contains the date the document or section was last revised. -->
<!ELEMENT RevisionDate  (Date )>

<!--The earliest date that the job/position seeker will be available to accept a new position or assignment. -->
<!ELEMENT AvailabilityDate  (SummaryText | Date )>

<!--An empty element within an end date that indicates "current status".  For example, a job/position that is currently held.-->
<!ELEMENT CurrentFlag EMPTY>

<!--A date or a period of time delimited by a start date and end date. -->
<!ELEMENT EffectiveDate  (StartDate? , EndDate? , Date? )>

<!--The rate of pay per hour.-->
<!ELEMENT RatePerHour  (#PCDATA )>
<!ATTLIST RatePerHour  currency  (ATS | 
                                  AUD | 
                                  BEF | 
                                  BRL | 
                                  CHF | 
                                  DEM | 
                                  DKK | 
                                  ESP | 
                                  EUR | 
                                  FIM | 
                                  FRF | 
                                  GBP | 
                                  GRD | 
                                  IEP | 
                                  ILS | 
                                  ITL | 
                                  JOD | 
                                  JPY | 
                                  LUF | 
                                  MXN | 
                                  MYR | 
                                  NLG | 
                                  NOK | 
                                  NZD | 
                                  PTE | 
                                  RUR | 
                                  SAR | 
                                  SEK | 
                                  SGD | 
                                  TRL | 
                                  USD | 
                                  ZAR )  #REQUIRED >
<!--The rate of pay per day.-->
<!ELEMENT RatePerDay  (#PCDATA )>
<!ATTLIST RatePerDay  currency  (ATS | 
                                 AUD | 
                                 BEF | 
                                 BRL | 
                                 CHF | 
                                 DEM | 
                                 DKK | 
                                 ESP | 
                                 EUR | 
                                 FIM | 
                                 FRF | 
                                 GBP | 
                                 GRD | 
                                 IEP | 
                                 ILS | 
                                 ITL | 
                                 JOD | 
                                 JPY | 
                                 LUF | 
                                 MXN | 
                                 MYR | 
                                 NLG | 
                                 NOK | 
                                 NZD | 
                                 PTE | 
                                 RUR | 
                                 SAR | 
                                 SEK | 
                                 SGD | 
                                 TRL | 
                                 USD | 
                                 ZAR )  #REQUIRED >
<!--An annual salary.-->
<!ELEMENT SalaryAnnual  (#PCDATA )>
<!ATTLIST SalaryAnnual  currency  (ATS | 
                                   AUD | 
                                   BEF | 
                                   BRL | 
                                   CHF | 
                                   DEM | 
                                   DKK | 
                                   ESP | 
                                   EUR | 
                                   FIM | 
                                   FRF | 
                                   GBP | 
                                   GRD | 
                                   IEP | 
                                   ILS | 
                                   ITL | 
                                   JOD | 
                                   JPY | 
                                   LUF | 
                                   MXN | 
                                   MYR | 
                                   NLG | 
                                   NOK | 
                                   NZD | 
                                   PTE | 
                                   RUR | 
                                   SAR | 
                                   SEK | 
                                   SGD | 
                                   TRL | 
                                   USD | 
                                   ZAR )  #REQUIRED >
<!--A monthly salary.-->
<!ELEMENT SalaryMonthly  (#PCDATA )>
<!ATTLIST SalaryMonthly  currency  (ATS | 
                                    AUD | 
                                    BEF | 
                                    BRL | 
                                    CHF | 
                                    DEM | 
                                    DKK | 
                                    ESP | 
                                    EUR | 
                                    FIM | 
                                    FRF | 
                                    GBP | 
                                    GRD | 
                                    IEP | 
                                    ILS | 
                                    ITL | 
                                    JOD | 
                                    JPY | 
                                    LUF | 
                                    MXN | 
                                    MYR | 
                                    NLG | 
                                    NOK | 
                                    NZD | 
                                    PTE | 
                                    RUR | 
                                    SAR | 
                                    SEK | 
                                    SGD | 
                                    TRL | 
                                    USD | 
                                    ZAR )  #REQUIRED >
<!--Information reported about a job/position seeker by the job/position seeker's employment references. -->
<!ELEMENT EmploymentReferenceReports  (ReferenceReport+ )>

<!--Information reported about a job/position seeker from a single employment reference. -->
<!ELEMENT ReferenceReport  (Contact , SummaryText? )>
<!ATTLIST ReferenceReport  source CDATA  #IMPLIED
                           date   CDATA  #IMPLIED >
<!--Information about a job/position seeker that customarily would not be provided in a resume, but may be collected as part of the recruiting process. For instance, AvailabilityDate and DesiredCompensation. 
		-->
<!ELEMENT Profile  (AvailabilityDate? , DesiredCompensation? , DesiredSchedule? , DesiredEmployer? , DistributionRestrictions? , DemographicInfo? , Qualification* , EmploymentReferenceReports? )>
<!ATTLIST Profile  source CDATA  #IMPLIED >
<!--A description of the compensation sought by the job/position seeker. -->
<!ELEMENT DesiredCompensation  ( (RatePerHour | RatePerDay | SalaryAnnual | SalaryMonthly )+ | SummaryText )>
<!ATTLIST DesiredCompensation  source CDATA  #IMPLIED >
<!--The work schedule desired by the job/position seeker.-->
<!ELEMENT DesiredSchedule  (#PCDATA )>
<!ATTLIST DesiredSchedule  source CDATA  #IMPLIED >
<!--Identification of the employer or type of employer with which the job/position seeker desires employment. DesiredEmployer may contain: 
	summary text, the name of a particular employer, or a standard industry classification code.-->
<!ELEMENT DesiredEmployer  (SummaryText | EmployerName | NAICS | EmployerSize )*>
<!ATTLIST DesiredEmployer  source CDATA  #IMPLIED >
<!--Allows the job/position seeker to list employers or recruiters to which the resume should not be distributed. -->
<!ELEMENT DistributionRestrictions  (DontDistributeTo+ )>
<!ATTLIST DistributionRestrictions  source CDATA  #IMPLIED >
<!--Contains the name of a single employer or recruiter to which the resume should not be distributed.-->
<!ELEMENT DontDistributeTo  (#PCDATA )>
<!ATTLIST DontDistributeTo  source CDATA  #IMPLIED >
<!--This tag provides a place to record demographic information that may be used for selection criteria or government reporting requirements. For example:<demographicInfo type="VeteranStatus"> Vietnam-era Veteran </demographicInfo type="VeteranStatus"> -->
<!ELEMENT DemographicInfo  (#PCDATA )>
<!ATTLIST DemographicInfo  type  CDATA  #REQUIRED
                           label CDATA  #IMPLIED >
<!--A telephone number for voice communications (versus fax, pager, etc). -->
<!ELEMENT VoiceNumber  (IntlCode? , AreaCode? , TelNumber , Extension? )>
<!ATTLIST VoiceNumber  type   (primary | secondary )  #IMPLIED
                       label CDATA  #IMPLIED >
<!--A telephone number for facsimile or "fax" transmissions. -->
<!ELEMENT FaxNumber  (IntlCode? , AreaCode? , TelNumber , Extension? )>
<!ATTLIST FaxNumber  type   (primary | secondary )  #IMPLIED
                     label CDATA  #IMPLIED >
<!--A telephone number for pager. -->
<!ELEMENT PagerNumber  (IntlCode? , AreaCode? , TelNumber , Extension? )>
<!ATTLIST PagerNumber  type   (primary | secondary )  #IMPLIED
                       label CDATA  #IMPLIED >
<!--A telephone number for teletype devices, which may be used by the hearing impaired. -->
<!ELEMENT TTDNumber  (IntlCode? , AreaCode? , TelNumber , Extension? )>
<!ATTLIST TTDNumber  type   (primary | secondary )  #IMPLIED
                     label CDATA  #IMPLIED >
<!--The "country code" that must be dialed to place an international call.-->
<!ELEMENT IntlCode  (#PCDATA )>

<!--A location prefix for a telephone number.-->
<!ELEMENT AreaCode  (#PCDATA )>

<!--A telephone number, not including a country code or area code.-->
<!ELEMENT TelNumber  (#PCDATA )>

<!--A telephone extension.-->
<!ELEMENT Extension  (#PCDATA )>

<!--Information about the supplier of the resume or job/position posting. -->
<!ELEMENT Supplier  (SupplierId , SupplierName , Contact )>

<!--The ID for the resume or job/position posting supplier.-->
<!ELEMENT SupplierId  (#PCDATA )>
<!ATTLIST SupplierId  idOwner CDATA  #REQUIRED >
<!--The name of the resume or job/position posting supplier.-->
<!ELEMENT SupplierName  (#PCDATA )>

<!--North American Industry Classification System code.-->
<!ELEMENT NAICS  (#PCDATA )>
<!ATTLIST NAICS  primaryIndicator  (primary | secondary | unknown )  'primary' >
<!--Employer size given as number of employees.-->
<!ELEMENT EmployerSize  (#PCDATA )>

<!--An employer name.-->
<!ELEMENT EmployerName  (#PCDATA )>

<!--Identifies an industry by Standard Industrial Classification (sic) Code or in summary text. -->
<!ELEMENT Industry  (NAICS | SummaryText )*>

<!--Contains the contact information for the person who posted the resume or job/position posting and the effective date of the posting. -->
<!ELEMENT PostDetail  (StartDate , EndDate? , PostedBy? )>

<!--Contact information for the person who posted the resume or job/position posting. -->
<!ELEMENT PostedBy  (Contact )>

<!--Contains the job/position seeker's name and contact information. -->
<!ELEMENT PersonalData  (RevisionDate? , PersonName , PositionTitle? , EmployerName? , PostalAddress? ,  (VoiceNumber | FaxNumber | PagerNumber | TTDNumber )* , E-mail? , SummaryText? )>

<!--Contains the names and contact information for a job/position seeker's employment references. -->
<!ELEMENT EmploymentReferences  (Reference+ )>

<!--Contains the name and contact information for a single employment reference.-->
<!ELEMENT Reference  (Contact , SummaryText? )>

