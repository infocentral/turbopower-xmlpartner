<span>
  <table>
    <tbody>
      <tr>
        <td width="113">
          <div>
            <b>
              <font size="5">Name</font>
            </b>
          </div>
        </td>
        <td width="113">
          <div>
            <b>
              <font size="5">Mass</font>
            </b>
          </div>
        </td>
        <td width="113">
          <div>
            <b>
              <font size="5">Day</font>
            </b>
          </div>
        </td>
        <td width="113">
          <div>
            <b>
              <font size="5">Radius</font>
            </b>
          </div>
        </td>
        <td width="113">
          <div>
            <b>
              <font size="5">Density</font>
            </b>
          </div>
        </td>
        <td width="113">
          <div>
            <b>
              <font size="5">Distance</font>
            </b>
          </div>
        </td>
      </tr>
      <tr>
        <td width="113">
          <div>
            <font size="5">Mercury</font>
          </div>
        </td>
        <td width="113">
          <div>
            <font size="5">.0553</font>
          </div>
        </td>
        <td width="113">
          <div>
            <font size="5">58.65</font>
          </div>
        </td>
        <td width="113">
          <div>
            <font size="5">1516</font>
          </div>
        </td>
        <td width="113">
          <div>
            <font size="5">.983</font>
          </div>
        </td>
        <td width="113">
          <div>
            <font size="5">43.4</font>
          </div>
        </td>
      </tr>
      <tr>
        <td width="113">
          <div>
            <font size="5">Venus</font>
          </div>
        </td>
        <td width="113">
          <div>
            <font size="5">.815</font>
          </div>
        </td>
        <td width="113">
          <div>
            <font size="5">116.75</font>
          </div>
        </td>
        <td width="113">
          <div>
            <font size="5">3716</font>
          </div>
        </td>
        <td width="113">
          <div>
            <font size="5">.943</font>
          </div>
        </td>
        <td width="113">
          <div>
            <font size="5">66.8</font>
          </div>
        </td>
      </tr>
      <tr>
        <td width="113">
          <div>
            <font size="5">Earth</font>
          </div>
        </td>
        <td width="113">
          <div>
            <font size="5">1</font>
          </div>
        </td>
        <td width="113">
          <div>
            <font size="5">1</font>
          </div>
        </td>
        <td width="113">
          <div>
            <font size="5">2107</font>
          </div>
        </td>
        <td width="113">
          <div>
            <font size="5">1</font>
          </div>
        </td>
        <td width="113">
          <div>
            <font size="5">128.4</font>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
</span>