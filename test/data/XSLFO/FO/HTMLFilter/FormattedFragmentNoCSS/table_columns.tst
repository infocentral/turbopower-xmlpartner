<span>
  <div>
    <div margin-left="0pt" margin-right="0pt" font="bold 14pt Helvetica" keep-with-next.within-column="always" keep-together.within-column="always" align="center" bgcolor="silver">Column specifiers in tables</div>
    <div keep-together.within-column="always">
      <div font="12pt Times">This is a table of 6 columns and 8 rows. Cells in all odd columns have light red background, and cells in even columns have it light blue.</div>
      <table>
        <tbody>
          <tr>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>1:1</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>1:2</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>1:3</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>1:4</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>1:5</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>1:6</div>
            </td>
          </tr>
          <tr>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>2:1</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>2:2</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>2:3</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>2:4</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>2:5</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>2:6</div>
            </td>
          </tr>
          <tr>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>3:1</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>3:2</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>3:3</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>3:4</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>3:5</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>3:6</div>
            </td>
          </tr>
          <tr>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>4:1</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>4:2</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>4:3</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>4:4</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>4:5</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>4:6</div>
            </td>
          </tr>
          <tr>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>5:1</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>5:2</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>5:3</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>5:4</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>5:5</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>5:6</div>
            </td>
          </tr>
          <tr>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>6:1</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>6:2</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>6:3</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>6:4</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>6:5</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>6:6</div>
            </td>
          </tr>
          <tr>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>7:1</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>7:2</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>7:3</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>7:4</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>7:5</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>7:6</div>
            </td>
          </tr>
          <tr>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>8:1</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>8:2</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>8:3</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>8:4</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>8:5</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>8:6</div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div white-space-collapse="false" line-height="6pt" border-top="1.5pt ridge silver"></div>
    <div keep-together.within-column="always">
      <div font="12pt Times">Table size is the same - 6 columns by 8 rows. Cells in columns 1 and 4 should have light green background (specified as default for the whole table-body); columns 2 and 5 have light red background, and columns 3 and 6 have light blue background.</div>
      <table>
        <tbody bgcolor="#80FF80">
          <tr>
            <td align="start" border="0.5pt solid black" width="67">
              <div>1:1</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>1:2</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>1:3</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67">
              <div>1:4</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>1:5</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>1:6</div>
            </td>
          </tr>
          <tr>
            <td align="start" border="0.5pt solid black" width="67">
              <div>2:1</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>2:2</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>2:3</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67">
              <div>2:4</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>2:5</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>2:6</div>
            </td>
          </tr>
          <tr>
            <td align="start" border="0.5pt solid black" width="67">
              <div>3:1</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>3:2</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>3:3</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67">
              <div>3:4</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>3:5</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>3:6</div>
            </td>
          </tr>
          <tr>
            <td align="start" border="0.5pt solid black" width="67">
              <div>4:1</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>4:2</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>4:3</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67">
              <div>4:4</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>4:5</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>4:6</div>
            </td>
          </tr>
          <tr>
            <td align="start" border="0.5pt solid black" width="67">
              <div>5:1</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>5:2</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>5:3</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67">
              <div>5:4</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>5:5</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>5:6</div>
            </td>
          </tr>
          <tr>
            <td align="start" border="0.5pt solid black" width="67">
              <div>6:1</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>6:2</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>6:3</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67">
              <div>6:4</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>6:5</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>6:6</div>
            </td>
          </tr>
          <tr>
            <td align="start" border="0.5pt solid black" width="67">
              <div>7:1</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>7:2</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>7:3</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67">
              <div>7:4</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>7:5</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>7:6</div>
            </td>
          </tr>
          <tr>
            <td align="start" border="0.5pt solid black" width="67">
              <div>8:1</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>8:2</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>8:3</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67">
              <div>8:4</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#FF8080">
              <div>8:5</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#8080FF">
              <div>8:6</div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div white-space-collapse="false" line-height="6pt" border-top="1.5pt ridge silver"></div>
    <div keep-together.within-column="always">
      <div font="12pt Times">
        <div>Table size is, again, the same - 6 columns by 8 rows. Column specifiers prescribe that cells in the second column be spanned 2 columns, and cells in the 4th column be spanned three columns.</div>
        <div>The cell disposition in the row is as follows:</div>
        <table width="100%">
          <tr>
            <td width="10%" end-indent="label-end()">
              <div>-</div>
            </td>
            <td width="*" start-indent="body-start()">
              <div>Cells in rows 1 and 5 use the default span as inherited from the column specifiers: first cell should span 1 column, second cell should span 2 columns, and third cell should start from the 4th column and span 3 columns (up to the end of the row);</div>
            </td>
          </tr>
          <tr>
            <td width="10%" end-indent="label-end()">
              <div>-</div>
            </td>
            <td width="*" start-indent="body-start()">
              <div>Cells in rows 2 and 6 have explicit indicationnumber-columns-spanned="1", and thus should not span; there should be 6 cells in each of these rows;</div>
            </td>
          </tr>
          <tr>
            <td width="10%" end-indent="label-end()">
              <div>-</div>
            </td>
            <td width="*" start-indent="body-start()">
              <div>Cells in rows 3 and 7 havenumber-columns-spannedset to 2, and thus aren't touched by the column specifiers: there should be 3 cells per row, each spanning two columns;</div>
            </td>
          </tr>
          <tr>
            <td width="10%" end-indent="label-end()">
              <div>-</div>
            </td>
            <td width="*" start-indent="body-start()">
              <div>Rows 4 and 8 contain only two cells - one in the first column and another one in the last column. What is interesting is the way of filling the empty space between them: there should be at least an empty cell spanning rows 2-3. As for the rest, the behaviour cannot be predicted exactly: there's no room to fit a 3-column-spanning empty cell into the 4th row. Renderer engine may either truncate this cell to 2-column, or split it into single one-column-spanning cells.</div>
            </td>
          </tr>
          <div>All cells starting from the 1st row should have light red background, cells starting from the 2nd row should have light blue background, and cells starting from the 4th row should have light green background. All other cells should have yellow background.</div>
        </table>
      </div>
      <table>
        <tbody bgcolor="#FFFF80">
          <tr>
            <td align="start" border="0.5pt solid black" width="80" bgcolor="#FF8080">
              <div>1:1</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#8080FF">
              <div>1:2</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#80FF80">
              <div>1:4</div>
            </td>
          </tr>
          <tr>
            <td align="start" border="0.5pt solid black" number-columns-spanned="1" width="80" bgcolor="#8080FF" colspan="1">
              <div>2:1</div>
            </td>
            <td align="start" border="0.5pt solid black" number-columns-spanned="1" width="93" bgcolor="#80FF80" colspan="1">
              <div>2:2</div>
            </td>
            <td align="start" border="0.5pt solid black" number-columns-spanned="1" width="67" colspan="1">
              <div>2:3</div>
            </td>
            <td align="start" border="0.5pt solid black" number-columns-spanned="1" colspan="1">
              <div>2:4</div>
            </td>
            <td align="start" border="0.5pt solid black" number-columns-spanned="1" colspan="1">
              <div>2:5</div>
            </td>
            <td align="start" border="0.5pt solid black" number-columns-spanned="1" colspan="1">
              <div>2:6</div>
            </td>
          </tr>
          <tr>
            <td align="start" border="0.5pt solid black" number-columns-spanned="2" width="173" bgcolor="#80FF80" colspan="2">
              <div>3:1</div>
            </td>
            <td align="start" border="0.5pt solid black" number-columns-spanned="2" width="67" colspan="2">
              <div>3:3</div>
            </td>
            <td align="start" border="0.5pt solid black" number-columns-spanned="2" colspan="2">
              <div>3:5</div>
            </td>
          </tr>
          <tr>
            <td align="start" border="0.5pt solid black" column-number="1" width="80" bgcolor="#FF8080">
              <div>4:1</div>
            </td>
            <td align="start" border="0.5pt solid black" column-number="6" width="93" bgcolor="#8080FF">
              <div>4:6</div>
            </td>
          </tr>
          <tr>
            <td align="start" border="0.5pt solid black" width="80" bgcolor="#FF8080">
              <div>5:1</div>
            </td>
            <td align="start" border="0.5pt solid black" width="93" bgcolor="#8080FF">
              <div>5:2</div>
            </td>
            <td align="start" border="0.5pt solid black" width="67" bgcolor="#80FF80">
              <div>5:4</div>
            </td>
          </tr>
          <tr>
            <td align="start" border="0.5pt solid black" number-columns-spanned="1" width="80" bgcolor="#8080FF" colspan="1">
              <div>6:1</div>
            </td>
            <td align="start" border="0.5pt solid black" number-columns-spanned="1" width="93" bgcolor="#80FF80" colspan="1">
              <div>6:2</div>
            </td>
            <td align="start" border="0.5pt solid black" number-columns-spanned="1" width="67" colspan="1">
              <div>6:3</div>
            </td>
            <td align="start" border="0.5pt solid black" number-columns-spanned="1" colspan="1">
              <div>6:4</div>
            </td>
            <td align="start" border="0.5pt solid black" number-columns-spanned="1" colspan="1">
              <div>6:5</div>
            </td>
            <td align="start" border="0.5pt solid black" number-columns-spanned="1" colspan="1">
              <div>6:6</div>
            </td>
          </tr>
          <tr>
            <td align="start" border="0.5pt solid black" number-columns-spanned="2" width="173" bgcolor="#80FF80" colspan="2">
              <div>7:1</div>
            </td>
            <td align="start" border="0.5pt solid black" number-columns-spanned="2" width="67" colspan="2">
              <div>7:3</div>
            </td>
            <td align="start" border="0.5pt solid black" number-columns-spanned="2" colspan="2">
              <div>7:5</div>
            </td>
          </tr>
          <tr>
            <td align="start" border="0.5pt solid black" column-number="1" width="80" bgcolor="#FF8080">
              <div>8:1</div>
            </td>
            <td align="start" border="0.5pt solid black" column-number="6" width="93" bgcolor="#8080FF">
              <div>8:6</div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</span>