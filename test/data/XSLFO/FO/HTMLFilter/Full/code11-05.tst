<html>
<style>
{ margin-left: 57pt; margin-top: 28pt; margin-right: 57pt; margin-bottom: 28pt;}
</style>
<body>
<table border="0" width="1134">
<tr>
</tr>
<tr><td valign="top" width="*"><span>
  <table>
    <tbody>
      <tr>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-weight: bold; font-size: 18pt">Name</div>
        </td>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-weight: bold; font-size: 18pt">Mass</div>
        </td>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-weight: bold; font-size: 18pt">Day</div>
        </td>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-weight: bold; font-size: 18pt">Radius</div>
        </td>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-weight: bold; font-size: 18pt">Density</div>
        </td>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-weight: bold; font-size: 18pt">Distance</div>
        </td>
      </tr>
      <tr>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-size: 18pt">Mercury</div>
        </td>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-size: 18pt">.0553</div>
        </td>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-size: 18pt">58.65</div>
        </td>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-size: 18pt">1516</div>
        </td>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-size: 18pt">.983</div>
        </td>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-size: 18pt">43.4</div>
        </td>
      </tr>
      <tr>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-size: 18pt">Venus</div>
        </td>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-size: 18pt">.815</div>
        </td>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-size: 18pt">116.75</div>
        </td>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-size: 18pt">3716</div>
        </td>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-size: 18pt">.943</div>
        </td>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-size: 18pt">66.8</div>
        </td>
      </tr>
      <tr>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-size: 18pt">Earth</div>
        </td>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-size: 18pt">1</div>
        </td>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-size: 18pt">1</div>
        </td>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-size: 18pt">2107</div>
        </td>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-size: 18pt">1</div>
        </td>
        <td style="border-width: 0.5mm" width="113">
          <div style="font-size: 18pt">128.4</div>
        </td>
      </tr>
    </tbody>
  </table>
</span></td>
</tr>
</table>
</body>
</html>