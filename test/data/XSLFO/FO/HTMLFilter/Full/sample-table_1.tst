<html>
<style>
{ margin-left: 57pt; margin-top: 28pt; margin-right: 57pt; margin-bottom: 28pt;}
</style>
<body>
<table border="0" width="816">
<tr>
<td valign="top" width="*" height="57"><span>
  <div style="font-size: 9pt">
    <img src="img/antenna-en.png" style="content-height: 12mm">XSL FO Sample Copyright (C) 2002 Antenna House, Inc. All rights reserved.</img>
  </div>
</span></td>
</tr>
<tr><td valign="top" width="*"><span>
  <div style="text-indent: 1em; font-family: sans-serif; font-size: 20pt; font-weight: bold; background-color: #EEEEEE; line-height: 20mm">Column width</div>
  <div style="padding-top: 2em">Column width can be set flexibly.</div>
  <div style="padding-top: 3em">In the following example, the width of 45mm, 40mm, 25mm, 60mm are set from the left.</div>
  <div style="padding-top: 1em">
    <table width="100%">
      <table>
        <tbody>
          <tr style="text-align: center; background-color: #CCBB22">
            <td style="border-style: solid; border-width: 0.2mm" width="170">
              <div>Expenses</div>
            </td>
            <td style="border-style: solid; border-width: 0.2mm" width="151">
              <div>Industry</div>
            </td>
            <td style="border-style: solid; border-width: 0.2mm" width="94">
              <div>Amount</div>
            </td>
            <td style="border-style: solid; border-width: 0.2mm" width="227">
              <div>Content</div>
            </td>
          </tr>
          <tr style="text-align: center">
            <td style="border-style: solid; border-width: 0.2mm" width="170">
              <div></div>
            </td>
            <td style="border-style: solid; border-width: 0.2mm" width="151">
              <div></div>
            </td>
            <td style="border-style: solid; border-width: 0.2mm" width="94">
              <div>Unit: billion Yen</div>
            </td>
            <td style="border-style: solid; border-width: 0.2mm" width="227">
              <div></div>
            </td>
          </tr>
          <tr>
            <td style="border-style: solid; border-width: 0.2mm; padding: 1mm" width="170">
              <div>Corporate Entertainment Expenses</div>
            </td>
            <td style="border-style: solid; border-width: 0.2mm; padding: 1mm" width="151">
              <div>Construction Industry</div>
            </td>
            <td style="border-style: solid; border-width: 0.2mm; padding: 1mm" width="94">
              <div style="text-align: right">906.4</div>
            </td>
            <td style="border-style: solid; border-width: 0.2mm; padding: 1mm" width="227">
              <div style="text-align: left">According to a survey by the National Tax Administration, the total entertainment expenses of Japanese companies in their business year came to 4.3918 trillion Yen. The top-spending industry was the construction industry.</div>
            </td>
          </tr>
        </tbody>
      </table>
    </table>
  </div>
  <div style="padding-top: 3em">In the following example, the width of 30mm, 30mm, 20mm, 70mm are set from the left.</div>
  <div style="padding-top: 1em">
    <table width="100%">
      <table style="font-family: sans-serif;  font-family: sans-serif; font-size: 7pt">
        <tbody>
          <tr style="text-align: center; background-color: #22BBCC">
            <td style="border-style: solid; border-width: 0.2mm" width="113">
              <div>Expenses</div>
            </td>
            <td style="border-style: solid; border-width: 0.2mm" width="113">
              <div>Industry</div>
            </td>
            <td style="border-style: solid; border-width: 0.2mm" width="76">
              <div>Amount</div>
            </td>
            <td style="border-style: solid; border-width: 0.2mm" width="265">
              <div>Content</div>
            </td>
          </tr>
          <tr style="text-align: center">
            <td style="border-style: solid; border-width: 0.2mm" width="113">
              <div></div>
            </td>
            <td style="border-style: solid; border-width: 0.2mm" width="113">
              <div></div>
            </td>
            <td style="border-style: solid; border-width: 0.2mm" width="76">
              <div>Unit: billion Yen</div>
            </td>
            <td style="border-style: solid; border-width: 0.2mm" width="265">
              <div></div>
            </td>
          </tr>
          <tr>
            <td style="border-style: solid; border-width: 0.2mm; padding: 1mm" width="113">
              <div>Corporate Entertainment Expenses</div>
            </td>
            <td style="border-style: solid; border-width: 0.2mm; padding: 1mm" width="113">
              <div>Construction industry</div>
            </td>
            <td style="border-style: solid; border-width: 0.2mm; padding: 1mm" width="76">
              <div style="text-align: right">906.4</div>
            </td>
            <td style="border-style: solid; border-width: 0.2mm; padding: 1mm" width="265">
              <div>According to a survey by the National Tax Administration, the total entertainment expenses of Japanese companies in their business year came to 4.3918 trillion Yen. The top-spending industry was the construction industry.</div>
            </td>
          </tr>
        </tbody>
      </table>
    </table>
  </div>
</span></td>
</tr>
</table>
</body>
</html>