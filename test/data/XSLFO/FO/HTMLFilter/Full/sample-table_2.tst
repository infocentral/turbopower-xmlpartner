<html>
<style>
{ margin-left: 57pt; margin-top: 28pt; margin-right: 57pt; margin-bottom: 28pt;}
</style>
<body>
<table border="0" width="816">
<tr>
<td valign="top" width="*" height="57"><span>
  <div style="font-size: 9pt">
    <img src="img/antenna-en.png" style="content-height: 12mm">XSL FO Sample Copyright (C) 2002 Antenna House, Inc. All rights reserved.</img>
  </div>
</span></td>
</tr>
<tr><td valign="top" width="*"><span>
  <div style="text-indent: 1em; font-family: sans-serif; font-size: 20pt; font-weight: bold; background-color: #EEEEEE; line-height: 20mm">Column span row span</div>
  <div style="padding-top: 2em">This is a sample of column span in the table.</div>
  <div style="padding-top: 2em; line-height: 10mm">
    <table width="100%" style="font-size: 12pt; font-size: 12pt">
      <table>
        <tbody>
          <tr style="text-align: center">
            <td style="border-style: solid; border-width: 0.5mm; background-color: #CCBB22" width="113">
              <div>1-1</div>
            </td>
            <td style="border-style: solid; border-width: 0.5mm; background-color: #CCBB22" width="113">
              <div>1-2</div>
            </td>
            <td style="border-style: solid; border-width: 0.5mm; background-color: #CCBB22" width="113">
              <div>1-3</div>
            </td>
            <td style="border-style: solid; border-width: 0.5mm; background-color: #CCBB22" width="113">
              <div>1-4</div>
            </td>
          </tr>
          <tr style="text-align: center">
            <td style="number-columns-spanned: 2; border-style: solid; border-width: 0.5mm; background-color: #FFBBBB" width="226" colspan="2">
              <div>number-columns-spanned="2"</div>
            </td>
            <td style="border-style: solid; border-width: 0.5mm; background-color: #CCBB22" width="113">
              <div>2-3</div>
            </td>
            <td style="border-style: solid; border-width: 0.5mm; background-color: #CCBB22" width="113">
              <div>2-4</div>
            </td>
          </tr>
          <tr style="text-align: center">
            <td style="number-columns-spanned: 3; border-style: solid; border-width: 0.5mm; background-color: #FFCCCC" width="339" colspan="3">
              <div>number-columns-spanned="3"</div>
            </td>
            <td style="border-style: solid; border-width: 0.5mm; background-color: #CCBB22" width="113">
              <div>3-4</div>
            </td>
          </tr>
          <tr style="text-align: center">
            <td style="number-columns-spanned: 4; border-style: solid; border-width: 0.5mm; background-color: #FFDDDD" width="452" colspan="4">
              <div>number-columns-spanned="4"</div>
            </td>
          </tr>
          <tr style="text-align: center">
            <td style="border-style: solid; border-width: 0.5mm; background-color: #CCBB22" width="113">
              <div>4-1</div>
            </td>
            <td style="number-columns-spanned: 2; border-style: solid; border-width: 0.5mm; background-color: #FFEEEE" width="226" colspan="2">
              <div>number-columns-spanned="2"</div>
            </td>
            <td style="border-style: solid; border-width: 0.5mm; background-color: #CCBB22" width="113">
              <div>4-4</div>
            </td>
          </tr>
        </tbody>
      </table>
    </table>
  </div>
  <div style="padding-top: 4em">This is a sample of row span in the table.</div>
  <div style="padding-top: 2em; line-height: 10mm; font-size: 12pt">
    <table width="100%">
      <table>
        <tbody>
          <tr style="text-align: center">
            <td style="border-style: solid; border-width: 0.5mm; background-color: #CCBB22" width="113">
              <div>1-1</div>
            </td>
            <td style="number-rows-spanned: 2; border-style: solid; border-width: 0.5mm; background-color: #BBFFBB" width="113" rowspan="2">
              <div>number-rows-spanned="2"</div>
            </td>
            <td style="number-rows-spanned: 5; border-style: solid; border-width: 0.5mm; background-color: #CCFFCC" width="113" rowspan="5">
              <div>number-rows-spanned="5"</div>
            </td>
            <td style="border-style: solid; border-width: 0.5mm; background-color: #CCBB22" width="113">
              <div>1-4</div>
            </td>
          </tr>
          <tr style="text-align: center">
            <td style="border-style: solid; border-width: 0.5mm; background-color: #CCBB22" width="113">
              <div>2-1</div>
            </td>
            <td style="number-rows-spanned: 3; border-style: solid; border-width: 0.5mm; background-color: #DDFFDD" width="113" rowspan="3">
              <div>number-rows-spanned="3"</div>
            </td>
          </tr>
          <tr style="text-align: center">
            <td style="border-style: solid; border-width: 0.5mm; background-color: #CCBB22" width="113">
              <div>3-1</div>
            </td>
            <td style="border-style: solid; border-width: 0.5mm; background-color: #CCBB22" width="113">
              <div>3-2</div>
            </td>
          </tr>
          <tr style="text-align: center">
            <td style="border-style: solid; border-width: 0.5mm; background-color: #CCBB22" width="113">
              <div>4-1</div>
            </td>
            <td style="border-style: solid; border-width: 0.5mm; background-color: #CCBB22" width="113">
              <div>4-2</div>
            </td>
          </tr>
          <tr style="text-align: center">
            <td style="border-style: solid; border-width: 0.5mm; background-color: #CCBB22" width="113">
              <div>5-1</div>
            </td>
            <td style="border-style: solid; border-width: 0.5mm; background-color: #CCBB22" width="113">
              <div>5-2</div>
            </td>
            <td style="border-style: solid; border-width: 0.5mm; background-color: #CCBB22" width="113">
              <div>5-4</div>
            </td>
          </tr>
        </tbody>
      </table>
    </table>
  </div>
</span></td>
</tr>
</table>
</body>
</html>