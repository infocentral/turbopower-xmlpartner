<html>
<style>
{ margin-left: 57pt; margin-top: 28pt; margin-right: 57pt; margin-bottom: 28pt;}
</style>
<body>
<table border="0" width="816">
<tr>
<td valign="top" width="*" height="57"><span>
  <div style="font-size: 9pt">
    <img src="img/antenna-en.png" style="content-height: 12mm">XSL FO Sample Copyright (C) 2002 Antenna House, Inc. All rights reserved.</img>
  </div>
</span></td>
</tr>
<tr><td valign="top" width="*"><span>
  <div style="text-indent: 2em; font-family: sans-serif; font-size: 20pt; font-weight: bold; background-color: #EEEEEE; line-height: 20mm">Padding and border</div>
  <div style="padding-top: 2em">This shows an example of border and padding setting in a table. The painted area shows border and the space between the border and the text shows padding.</div>
  <div style="padding-top: 2em">
    <table width="100%" style="font-size: 12pt; font-size: 12pt">
      <table>
        <tbody>
          <tr style="text-align: center">
            <td style="border-style: ridge; border-color: orange; border-width: 0.2mm; padding: 0mm; background-color: #FFFFFF">
              <div>border-width="0.2mm" padding="0mm"</div>
            </td>
            <td style="border-style: ridge; border-color: orange; border-width: 0.2mm; padding: 3mm; background-color: #FFFFFF">
              <div>border-width="0.2mm" padding="3mm"</div>
            </td>
          </tr>
        </tbody>
      </table>
    </table>
    <table width="100%" style="padding-top: 2em;  padding-top: 2em; font-size: 12pt">
      <table>
        <tbody>
          <tr style="text-align: center">
            <td style="border-style: ridge; border-color: orange; border-width: 1.5mm; padding: 0mm; background-color: #FFFFFF">
              <div>border-width="1.5mm" padding="0mm"</div>
            </td>
            <td style="border-style: ridge; border-color: orange; border-width: 1.5mm; padding: 3mm; background-color: #FFFFFF">
              <div>border-width="1.5mm" padding="3mm"</div>
            </td>
          </tr>
        </tbody>
      </table>
    </table>
    <table width="100%" style="padding-top: 2em;  padding-top: 2em; font-size: 12pt">
      <table>
        <tbody>
          <tr style="text-align: center">
            <td style="border-style: ridge; border-color: orange; border-width: 5.0mm; padding: 0mm; background-color: #FFFFFF">
              <div>border-width="5.0mm" padding="0mm"</div>
            </td>
            <td style="border-style: ridge; border-color: orange; border-width: 5.0mm; padding: 3mm; background-color: #FFFFFF">
              <div>border-width="5.0mm" padding="3mm"</div>
            </td>
          </tr>
        </tbody>
      </table>
    </table>
    <table width="100%" style="padding-top: 2em;  padding-top: 2em; font-size: 12pt">
      <table>
        <tbody>
          <tr style="text-align: center">
            <td style="border-style: ridge; border-color: orange; border-width: 2.0mm; padding: 0mm; background-color: #FFFFFF">
              <div>border-width="2.0mm" padding="0mm"</div>
            </td>
          </tr>
        </tbody>
      </table>
    </table>
    <table width="100%" style="padding-top: 2em;  padding-top: 2em; font-size: 12pt">
      <table>
        <tbody>
          <tr style="text-align: center">
            <td style="border-style: ridge; border-color: orange; border-width: 2.0mm; padding: 1mm; background-color: #FFFFFF">
              <div>border-width="2.0mm" padding="1mm"</div>
            </td>
          </tr>
        </tbody>
      </table>
    </table>
    <table width="100%" style="padding-top: 2em;  padding-top: 2em; font-size: 12pt">
      <table>
        <tbody>
          <tr style="text-align: center">
            <td style="border-style: ridge; border-color: orange; border-width: 2.0mm; padding: 2mm; background-color: #FFFFFF">
              <div>border-width="2.0mm" padding="2mm"</div>
            </td>
          </tr>
        </tbody>
      </table>
    </table>
    <table width="100%" style="padding-top: 2em;  padding-top: 2em; font-size: 12pt">
      <table>
        <tbody>
          <tr style="text-align: center">
            <td style="border-style: ridge; border-color: orange; border-width: 2.0mm; padding: 5mm; background-color: #FFFFFF">
              <div>border-width="2.0mm" padding="5mm"</div>
            </td>
          </tr>
        </tbody>
      </table>
    </table>
  </div>
</span></td>
</tr>
</table>
</body>
</html>