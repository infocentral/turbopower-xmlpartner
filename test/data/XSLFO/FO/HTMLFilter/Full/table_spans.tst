<html>
<style>
{}
</style>
<body>
<table border="0" width="">
<tr>
<td valign="top" width="*" height="67"><span>
  <table width="100%" style="font: 10pt Helvetica;   font: 10pt Helvetica; provisional-distance-between-starts: 5in; provisional-label-separation: 0in">
    <tr>
      <td width="10%" style="end-indent: label-end()">
        <div style="text-align: left; font-weight: bold">Tables with cells spanning multiple rows/columns</div>
      </td>
      <td width="*" style="start-indent: body-start()">
        <div style="text-align: right">Page1</div>
      </td>
    </tr>
  </table>
</span></td>
</tr>
<tr><td valign="top" width="*"><span>
  <div>
    <div style="margin-left: 0pt; margin-right: 0pt; font: bold 14pt Helvetica; t; padding-top: discard; padding-bottom: 6pt; keep-with-next.within-column: always; keep-together.within-column: always; text-align: center; padding: 3pt; background-color: silver">Tables with cells spanning multiple rows/columns</div>
    <div style="keep-together.within-column: always">
      <div style="font: 12pt Times; padding-top: 6pt; padding-bottom: 6pt">This table has six columns and five rows. In each row, there are only two cells: the left one spans the number of columns equal to the row number, and the right one spans throughout the rest of the table columns. All left cells are red, and all right cells are blue. The whole thing should look like a rectangle split by a diagonal line from NW to SE; the SW triangle is red and the NE triangle is blue.</div>
      <table style="margin-left: 0pt;     margin-left: 0pt; margin-right: 0pt; color: black; border: medium double black; border-collapse: separate">
        <tbody>
          <tr>
            <td style="starts-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 1; background-color: #FFC0C0" colspan="1">
              <div>1 to 1</div>
            </td>
            <td style="ends-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 5; background-color: #C0C0FF" colspan="5">
              <div>2 to 6</div>
            </td>
          </tr>
          <tr>
            <td style="starts-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 2; background-color: #FFC0C0" colspan="2">
              <div>1 to 2</div>
            </td>
            <td style="ends-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 4; background-color: #C0C0FF" colspan="4">
              <div>3 to 6</div>
            </td>
          </tr>
          <tr>
            <td style="starts-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 3; background-color: #FFC0C0" colspan="3">
              <div>1 to 3</div>
            </td>
            <td style="ends-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 3; background-color: #C0C0FF" colspan="3">
              <div>4 to 6</div>
            </td>
          </tr>
          <tr>
            <td style="starts-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 4; background-color: #FFC0C0" colspan="4">
              <div>1 to 4</div>
            </td>
            <td style="ends-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 2; background-color: #C0C0FF" colspan="2">
              <div>5 to 6</div>
            </td>
          </tr>
          <tr>
            <td style="starts-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 5; background-color: #FFC0C0" colspan="5">
              <div>1 to 5</div>
            </td>
            <td style="ends-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 1; background-color: #C0C0FF" colspan="1">
              <div>6 to 6</div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style="white-space-collapse: false; line-height: 6pt; padding-top: 6pt; border-top: 1.5pt ridge silver"></div>
    <div style="keep-together.within-column: always">
      <div style="font: 12pt Times; padding-top: 6pt; padding-bottom: 6pt">Same as above, but an extra yellow cell is inserted between the red and the blue ones in the row. The table thus becomes 7 columns wide. The width of these added cells is set to 24 points; therefore, the first and the last column in the table should expand to absorb the rest of the page width.</div>
      <table style="margin-left: 0pt;     margin-left: 0pt; margin-right: 0pt; color: black; border: medium double black; border-collapse: separate">
        <tbody>
          <tr>
            <td style="starts-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 1; background-color: #FFC0C0" colspan="1">
              <div>1 1</div>
            </td>
            <td style="background-color: yellow; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt" width="32">
              <div>2</div>
            </td>
            <td style="ends-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 5; background-color: #C0C0FF" colspan="5">
              <div>3 7</div>
            </td>
          </tr>
          <tr>
            <td style="starts-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 2; background-color: #FFC0C0" width="32" colspan="2">
              <div>1 2</div>
            </td>
            <td style="background-color: yellow; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt">
              <div>3</div>
            </td>
            <td style="ends-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 4; background-color: #C0C0FF" colspan="4">
              <div>4 7</div>
            </td>
          </tr>
          <tr>
            <td style="starts-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 3; background-color: #FFC0C0" width="32" colspan="3">
              <div>1 3</div>
            </td>
            <td style="background-color: yellow; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt">
              <div>4</div>
            </td>
            <td style="ends-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 3; background-color: #C0C0FF" colspan="3">
              <div>5 7</div>
            </td>
          </tr>
          <tr>
            <td style="starts-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 4; background-color: #FFC0C0" width="32" colspan="4">
              <div>1 4</div>
            </td>
            <td style="background-color: yellow; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt">
              <div>5</div>
            </td>
            <td style="ends-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 2; background-color: #C0C0FF" colspan="2">
              <div>6 7</div>
            </td>
          </tr>
          <tr>
            <td style="starts-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 5; background-color: #FFC0C0" width="32" colspan="5">
              <div>1 5</div>
            </td>
            <td style="background-color: yellow; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt">
              <div>6</div>
            </td>
            <td style="ends-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 1; background-color: #C0C0FF" colspan="1">
              <div>7 7</div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style="white-space-collapse: false; line-height: 6pt; padding-top: 6pt; border-top: 1.5pt ridge silver"></div>
    <div style="keep-together.within-column: always">
      <div style="font: 12pt Times; padding-top: 6pt; padding-bottom: 6pt">Same as above, but instead of a yellow cell, the two cells in each row are separated by blank space (no cell occupies it). The gray background of the table should be visible in those places. The cells are positioned usingcolumn-numberattribute.</div>
      <table style="margin-left: 0pt;      margin-left: 0pt; margin-right: 0pt; color: black; border: medium double black; border-collapse: separate; background-color: silver">
        <tbody>
          <tr>
            <td style="starts-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 1; background-color: #FFC0C0" colspan="1">
              <div>1 1</div>
            </td>
            <td style="ends-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 5; column-number: 3; background-color: #C0C0FF" width="32" colspan="5">
              <div>3 7</div>
            </td>
          </tr>
          <tr>
            <td style="starts-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 2; background-color: #FFC0C0" width="32" colspan="2">
              <div>1 2</div>
            </td>
            <td style="ends-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 4; column-number: 4; background-color: #C0C0FF" colspan="4">
              <div>4 7</div>
            </td>
          </tr>
          <tr>
            <td style="starts-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 3; background-color: #FFC0C0" width="32" colspan="3">
              <div>1 3</div>
            </td>
            <td style="ends-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 3; column-number: 5; background-color: #C0C0FF" colspan="3">
              <div>5 7</div>
            </td>
          </tr>
          <tr>
            <td style="starts-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 4; background-color: #FFC0C0" width="32" colspan="4">
              <div>1 4</div>
            </td>
            <td style="ends-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 2; column-number: 6; background-color: #C0C0FF" colspan="2">
              <div>6 7</div>
            </td>
          </tr>
          <tr>
            <td style="starts-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 5; background-color: #FFC0C0" width="32" colspan="5">
              <div>1 5</div>
            </td>
            <td style="ends-row: true; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; number-columns-spanned: 1; column-number: 7; background-color: #C0C0FF" colspan="1">
              <div>7 7</div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style="white-space-collapse: false; line-height: 6pt; padding-top: 6pt; border-top: 1.5pt ridge silver"></div>
    <div style="keep-together.within-column: always">
      <div style="font: 12pt Times; padding-top: 6pt; padding-bottom: 6pt">This table has six rows and five columns. In each column, there are only two cells: the upper spans the number of rows equal to the column number, and the lower spans throughout the rest of the table rows. All top cells are red, and all bottom cells are blue. The whole thing should look similar to the first example rotated 90 grad. All column widths are set to 48 points.</div>
      <table style="margin-left: 0pt;     margin-left: 0pt; margin-right: 0pt; color: black; border: medium double black; border-collapse: separate">
        <tbody>
          <tr style="height: 24pt">
            <td style="number-rows-spanned: 1; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #FFC0C0" width="64" rowspan="1">
              <div>1 1</div>
            </td>
            <td style="number-rows-spanned: 2; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #FFC0C0" width="64" rowspan="2">
              <div>1 2</div>
            </td>
            <td style="number-rows-spanned: 3; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #FFC0C0" width="64" rowspan="3">
              <div>1 3</div>
            </td>
            <td style="number-rows-spanned: 4; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #FFC0C0" width="64" rowspan="4">
              <div>1 4</div>
            </td>
            <td style="number-rows-spanned: 5; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #FFC0C0" width="64" rowspan="5">
              <div>1 5</div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="number-rows-spanned: 5; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #C0C0FF" width="64" rowspan="5">
              <div>2 6</div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="number-rows-spanned: 4; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #C0C0FF" width="64" rowspan="4">
              <div>3 6</div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="number-rows-spanned: 3; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #C0C0FF" width="64" rowspan="3">
              <div>4 6</div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="number-rows-spanned: 2; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #C0C0FF" width="64" rowspan="2">
              <div>5 6</div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="number-rows-spanned: 1; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #C0C0FF" width="64" rowspan="1">
              <div>6 6</div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style="white-space-collapse: false; line-height: 6pt; padding-top: 6pt; border-top: 1.5pt ridge silver"></div>
    <div style="keep-together.within-column: always">
      <div style="font: 12pt Times; padding-top: 6pt; padding-bottom: 6pt">Same as above except that the lower cell in each column spans the number of rows equal to the column number, and the upper spans throughout the rest of the table rows. Should look like the previous example, but with the diagonal line directed from SW to NE.</div>
      <table style="margin-left: 0pt;     margin-left: 0pt; margin-right: 0pt; color: black; border: medium double black; border-collapse: separate">
        <tbody>
          <tr style="height: 24pt">
            <td style="number-rows-spanned: 5; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #FFC0C0" width="64" rowspan="5">
              <div>1 5</div>
            </td>
            <td style="number-rows-spanned: 4; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #FFC0C0" width="64" rowspan="4">
              <div>1 4</div>
            </td>
            <td style="number-rows-spanned: 3; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #FFC0C0" width="64" rowspan="3">
              <div>1 3</div>
            </td>
            <td style="number-rows-spanned: 2; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #FFC0C0" width="64" rowspan="2">
              <div>1 2</div>
            </td>
            <td style="number-rows-spanned: 1; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #FFC0C0" width="64" rowspan="1">
              <div>1 1</div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="number-rows-spanned: 5; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #C0C0FF" width="64" rowspan="5">
              <div>2 6</div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="number-rows-spanned: 4; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #C0C0FF" width="64" rowspan="4">
              <div>3 6</div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="number-rows-spanned: 3; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #C0C0FF" width="64" rowspan="3">
              <div>4 6</div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="number-rows-spanned: 2; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #C0C0FF" width="64" rowspan="2">
              <div>5 6</div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="number-rows-spanned: 1; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #C0C0FF" width="64" rowspan="1">
              <div>6 6</div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style="white-space-collapse: false; line-height: 6pt; padding-top: 6pt; border-top: 1.5pt ridge silver"></div>
    <div style="keep-together.within-column: always">
      <div style="font: 12pt Times; padding-top: 6pt; padding-bottom: 6pt">The next table looks like a previous one with an extra yellow cell inserted between the red and the blue ones in the column. The table thus becomes 7 rows long.</div>
      <table style="margin-left: 0pt;     margin-left: 0pt; margin-right: 0pt; color: black; border: medium double black; border-collapse: separate">
        <tbody>
          <tr style="height: 24pt">
            <td style="number-rows-spanned: 5; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #FFC0C0" width="64" rowspan="5">
              <div>1 5</div>
            </td>
            <td style="number-rows-spanned: 4; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #FFC0C0" width="64" rowspan="4">
              <div>1 4</div>
            </td>
            <td style="number-rows-spanned: 3; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #FFC0C0" width="64" rowspan="3">
              <div>1 3</div>
            </td>
            <td style="number-rows-spanned: 2; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #FFC0C0" width="64" rowspan="2">
              <div>1 2</div>
            </td>
            <td style="number-rows-spanned: 1; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #FFC0C0" width="64" rowspan="1">
              <div>1 1</div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="background-color: yellow; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt" width="64">
              <div>2</div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: yellow" width="64">
              <div>3</div>
            </td>
            <td style="number-rows-spanned: 5; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #C0C0FF" width="64" rowspan="5">
              <div>3 7</div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: yellow" width="64">
              <div>4</div>
            </td>
            <td style="number-rows-spanned: 4; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #C0C0FF" width="64" rowspan="4">
              <div>4 7</div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: yellow" width="64">
              <div>5</div>
            </td>
            <td style="number-rows-spanned: 3; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #C0C0FF" width="64" rowspan="3">
              <div>5 7</div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: yellow" width="64">
              <div>6</div>
            </td>
            <td style="number-rows-spanned: 2; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #C0C0FF" width="64" rowspan="2">
              <div>6 7</div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="number-rows-spanned: 1; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #C0C0FF" width="64" rowspan="1">
              <div>7 7</div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style="white-space-collapse: false; line-height: 6pt; padding-top: 6pt; border-top: 1.5pt ridge silver"></div>
    <div style="keep-together.within-column: always">
      <div style="font: 12pt Times; padding-top: 6pt; padding-bottom: 6pt">Same as above, but instead of a yellow cell, the two cells in each column are separated by blank space (no cell occupies it). The cells are positioned usingcolumn-numberattribute.</div>
      <table style="margin-left: 0pt;      margin-left: 0pt; margin-right: 0pt; color: black; border: medium double black; border-collapse: separate; background-color: silver">
        <tbody>
          <tr style="height: 24pt">
            <td style="number-rows-spanned: 5; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #FFC0C0" width="64" rowspan="5">
              <div>1 5</div>
            </td>
            <td style="number-rows-spanned: 4; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #FFC0C0" width="64" rowspan="4">
              <div>1 4</div>
            </td>
            <td style="number-rows-spanned: 3; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #FFC0C0" width="64" rowspan="3">
              <div>1 3</div>
            </td>
            <td style="number-rows-spanned: 2; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #FFC0C0" width="64" rowspan="2">
              <div>1 2</div>
            </td>
            <td style="number-rows-spanned: 1; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #FFC0C0" width="64" rowspan="1">
              <div>1 1</div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="text-align: left; border: 0.5pt solid black; padding: 6pt" width="64">
              <div></div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="number-rows-spanned: 5; column-number: 5; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #C0C0FF" width="64" rowspan="5">
              <div>3 7</div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="number-rows-spanned: 4; column-number: 4; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #C0C0FF" width="64" rowspan="4">
              <div>4 7</div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="number-rows-spanned: 3; column-number: 3; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #C0C0FF" width="64" rowspan="3">
              <div>5 7</div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="number-rows-spanned: 2; column-number: 2; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #C0C0FF" width="64" rowspan="2">
              <div>6 7</div>
            </td>
          </tr>
          <tr style="height: 24pt">
            <td style="number-rows-spanned: 1; column-number: 1; border: 0.5pt solid black; text-align: center; vertical-align: middle; padding: 6pt; background-color: #C0C0FF" width="64" rowspan="1">
              <div>7 7</div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style="white-space-collapse: false; line-height: 6pt; padding-top: 6pt; border-top: 1.5pt ridge silver"></div>
    <div style="keep-together.within-column: always">
      <div style="font: 12pt Times; padding-top: 6pt; padding-bottom: 6pt">The next table is 7 x 7. It should look like this:
        <table width="100%" style="padding-top: 6pt;  padding-top: 6pt; padding-bottom: 6pt">
          <tr>
            <td width="10%" style="end-indent: label-end()">
              <div>-</div>
            </td>
            <td width="*" style="start-indent: body-start()">
              <div>first/last column and first/last row are occupied by singleton (1 x 1) cells with blue background, forming a blue border around the table;</div>
            </td>
          </tr>
          <tr>
            <td width="10%" style="end-indent: label-end()">
              <div>-</div>
            </td>
            <td width="*" style="start-indent: body-start()">
              <div>inside it, four red cells (two 3 x 2 and two 2 x 3) are located that occupy all space except for the central grid unit;</div>
            </td>
          </tr>
          <tr>
            <td width="10%" style="end-indent: label-end()">
              <div>-</div>
            </td>
            <td width="*" style="start-indent: body-start()">
              <div>the cell [4:4] in the very center is left blank (not present in the table); its color is determined by the table background and should be light gray.</div>
            </td>
          </tr>
        </table>
      </div>
      <table style="color: black;   color: black; border-collapse: separate; background-color: silver">
        <tbody>
          <tr style="height: 36pt">
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF" width="48">
              <div>1:1</div>
            </td>
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF" width="48">
              <div>1:2</div>
            </td>
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF" width="48">
              <div>1:3</div>
            </td>
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF" width="48">
              <div>1:4</div>
            </td>
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF" width="48">
              <div>1:5</div>
            </td>
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF" width="48">
              <div>1:6</div>
            </td>
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF" width="48">
              <div>1:7</div>
            </td>
          </tr>
          <tr style="height: 36pt">
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF; column-number: 1" width="48">
              <div>2:1</div>
            </td>
            <td style="border: 1.5pt solid red; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #FFC0C0; number-columns-spanned: 3; number-rows-spanned: 2" width="144" colspan="3" rowspan="2">
              <div>2:2 to 3:4</div>
            </td>
            <td style="border: 1.5pt solid red; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #FFC0C0; number-columns-spanned: 2; number-rows-spanned: 3" width="96" colspan="2" rowspan="3">
              <div>2:5 to 4:6</div>
            </td>
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF; column-number: 7" width="48">
              <div>2:7</div>
            </td>
          </tr>
          <tr style="height: 36pt">
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF; column-number: 1" width="48">
              <div>3:1</div>
            </td>
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF; column-number: 7" width="48">
              <div>3:7</div>
            </td>
          </tr>
          <tr style="height: 36pt">
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF; column-number: 1" width="48">
              <div>4:1</div>
            </td>
            <td style="border: 1.5pt solid red; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #FFC0C0; number-columns-spanned: 2; number-rows-spanned: 3" width="96" colspan="2" rowspan="3">
              <div>4:2 to 6:3</div>
            </td>
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF; column-number: 7" width="48">
              <div>4:7</div>
            </td>
          </tr>
          <tr style="height: 36pt">
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF; column-number: 1" width="48">
              <div>5:1</div>
            </td>
            <td style="border: 1.5pt solid red; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #FFC0C0; number-columns-spanned: 3; number-rows-spanned: 2" width="144" colspan="3" rowspan="2">
              <div>5:4 to 6:6</div>
            </td>
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF; column-number: 7" width="48">
              <div>5:7</div>
            </td>
          </tr>
          <tr style="height: 36pt">
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF; column-number: 1" width="48">
              <div>6:1</div>
            </td>
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF; column-number: 7" width="48">
              <div>6:7</div>
            </td>
          </tr>
          <tr style="height: 36pt">
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF" width="48">
              <div>7:1</div>
            </td>
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF" width="48">
              <div>7:2</div>
            </td>
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF" width="48">
              <div>7:3</div>
            </td>
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF" width="48">
              <div>7:4</div>
            </td>
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF" width="48">
              <div>7:5</div>
            </td>
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF" width="48">
              <div>7:6</div>
            </td>
            <td style="border: 1.5pt solid blue; text-align: center; vertical-align: middle; padding: 12pt 6pt; background-color: #C0C0FF" width="48">
              <div>7:7</div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</span></td>
</tr>
<tr><td valign="top" width="*"><span>
  <table width="100%" style="font: 9pt Times;   font: 9pt Times; provisional-distance-between-starts: 3in; provisional-label-separation: 0in">
    <tr>
      <td width="10%" style="end-indent: label-end()">
        <div style="text-align: left; font-weight: bold">(c)
          <a href="url(http://www.renderx.com/)" style="color: #0000C0; text-decoration: underline">RenderX2000</a>
        </div>
      </td>
      <td width="*" style="start-indent: body-start()">
        <div style="text-align: right; font-style: italic; color: #606060">XSL Formatting Objects Test Suite</div>
      </td>
    </tr>
  </table>
</span></td>
</tr>
</table>
</body>
</html>