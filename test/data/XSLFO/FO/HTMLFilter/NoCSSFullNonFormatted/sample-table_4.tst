<html>
<body>
<table border="0" width="816">
<tr>
<td valign="top" width="*" height="57"><span><div><font size="2"><img src="img\antenna-en.png" content-height="12mm">XSL FO Sample Copyright (C) 2002 Antenna House, Inc. All rights reserved.</img></font></div></span></td>
</tr>
<tr><td valign="top" width="*"><span><div><font face="sans-serif"><font size="6"><b bgcolor="#EEEEEE" line-height="20mm">Text positioning in a table cell</b></font></font></div><div>Text in a table cell can be set to the upper side, or to the lower side.</div><div><font face="sans-serif" line-height="7mm"><table><font size="2" width="100%"><font size="2"><table border="1"><tbody><tr align="center"><td bgcolor="#CCCCFF" width="189"><div>display-align="before"</div></td><td bgcolor="#EECCFF" width="189"><div>display-align="after"</div></td><td bgcolor="#EEEEFF" width="189"><div>display-align="center"</div></td></tr><tr height="378" align="justify"><td valign="top" bgcolor="#CCCCFF" width="189"><div>Flower festival at Yasukuni Shrine. Cherry trees are in bloom along the street by the Chidorigafuchi moat as far as Yasukuni Shrine. Taiko drum-beating is presented at Yasukuni Shrine in the evening.</div></td><td valign="bottom" bgcolor="#EECCFF" width="189"><div>Flower festival at Yasukuni Shrine. Cherry trees are in bloom along the street by the Chidorigafuchi moat as far as Yasukuni Shrine. Taiko drum-beating is presented at Yasukuni Shrine in the evening.</div></td><td valign="middle" bgcolor="#EEEEFF" width="189"><div>Flower festival at Yasukuni Shrine. Cherry trees are in bloom along the street by the Chidorigafuchi moat as far as Yasukuni Shrine. Taiko drum-beating is presented at Yasukuni Shrine in the evening.</div></td></tr></tbody></table></font></font></table></font></div></span></td>
</tr>
</table>
</body>
</html>