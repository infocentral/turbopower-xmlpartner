<html>
<body>
<span><div><font size="5"><font face="sans-serif" line-height="24pt" bgcolor="blue" color="white" align="center" padding-top="3pt">Psalm 23</font></font></div><div><font size="5"><font face="sans-serif" line-height="20pt" align="start" padding-top="3pt">Scottish Psalter<div>1650</div></font></font></div><div><font size="3"><font face="sans-serif" line-height="15pt" align="start"><div><div>The LORD's my shepherd, I'll not want:</div><div>He makes me down to lie</div><div>In pastures green; He leadeth me</div><div>The quiet waters by.</div></div><div><div>Goodness and mercy all my life</div><div>Shall surely follow me;</div><div>And in God's house for evermore</div><div>My dwelling place shall be.</div></div></font></font></div></span>
</body>
</html>