<span>
  <fo:sequence-specification>
    <fo:sequence-specifier-single master-name="page"></fo:sequence-specifier-single>
  </fo:sequence-specification>
  <div>
    <b>
      <font size="7" line-height="48pt">
        <font face="sans-serif">
            Name: 
            Mercury</font>
      </font>
    </b>
  </div>
  <div>
    <font size="7" line-height="48pt">
      <font face="sans-serif">
            Mass (Earth = 1): 
            .0553</font>
    </font>
  </div>
  <div>
    <font size="7" line-height="48pt">
      <font face="sans-serif">
            Day (Earth = 1): 
            58.65</font>
    </font>
  </div>
  <div>
    <font size="7" line-height="48pt">
      <font face="sans-serif">
            Radius (in miles): 
            1516</font>
    </font>
  </div>
  <div>
    <font size="7" line-height="48pt">
      <font face="sans-serif">
            Density (Earth = 1): 
            .983</font>
    </font>
  </div>
  <div>
    <font size="7" line-height="48pt">
      <font face="sans-serif">
            Distance (million miles): 
            43.4</font>
    </font>
  </div>
  <div>
    <b>
      <font size="7" line-height="48pt">
        <font face="sans-serif">
            Name: 
            Venus</font>
      </font>
    </b>
  </div>
  <div>
    <font size="7" line-height="48pt">
      <font face="sans-serif">
            Mass (Earth = 1): 
            .815</font>
    </font>
  </div>
  <div>
    <font size="7" line-height="48pt">
      <font face="sans-serif">
            Day (Earth = 1): 
            116.75</font>
    </font>
  </div>
  <div>
    <font size="7" line-height="48pt">
      <font face="sans-serif">
            Radius (in miles): 
            3716</font>
    </font>
  </div>
  <div>
    <font size="7" line-height="48pt">
      <font face="sans-serif">
            Density (Earth = 1): 
            .943</font>
    </font>
  </div>
  <div>
    <font size="7" line-height="48pt">
      <font face="sans-serif">
            Distance (million miles): 
            66.8</font>
    </font>
  </div>
  <div>
    <b>
      <font size="7" line-height="48pt">
        <font face="sans-serif">
            Name: 
            Earth</font>
      </font>
    </b>
  </div>
  <div>
    <font size="7" line-height="48pt">
      <font face="sans-serif">
            Mass (Earth = 1): 
            1</font>
    </font>
  </div>
  <div>
    <font size="7" line-height="48pt">
      <font face="sans-serif">
            Day (Earth = 1): 
            1</font>
    </font>
  </div>
  <div>
    <font size="7" line-height="48pt">
      <font face="sans-serif">
            Radius (in miles): 
            2107</font>
    </font>
  </div>
  <div>
    <font size="7" line-height="48pt">
      <font face="sans-serif">
            Density (Earth = 1): 
            1</font>
    </font>
  </div>
  <div>
    <font size="7" line-height="48pt">
      <font face="sans-serif">
            Distance (million miles): 
            128.4</font>
    </font>
  </div>
</span>