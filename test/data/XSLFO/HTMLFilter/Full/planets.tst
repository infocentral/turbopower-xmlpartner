<html>
<style>
{ margin-left: 57pt; margin-top: 28pt; margin-right: 57pt; margin-bottom: 28pt;}
</style>
<body>
<table border="0" width="1134">
<tr>
</tr>
<tr><td valign="top" width="*"><span>
  <fo:sequence-specification>
    <fo:sequence-specifier-single master-name="page"></fo:sequence-specifier-single>
  </fo:sequence-specification>
  <div style="font-weight: bold; font-size: 36pt; line-height: 48pt; font-family: sans-serif">
            Name: 
            Mercury</div>
  <div style="font-size: 36pt; line-height: 48pt; font-family: sans-serif">
            Mass (Earth = 1): 
            .0553</div>
  <div style="font-size: 36pt; line-height: 48pt; font-family: sans-serif">
            Day (Earth = 1): 
            58.65</div>
  <div style="font-size: 36pt; line-height: 48pt; font-family: sans-serif">
            Radius (in miles): 
            1516</div>
  <div style="font-size: 36pt; line-height: 48pt; font-family: sans-serif">
            Density (Earth = 1): 
            .983</div>
  <div style="font-size: 36pt; line-height: 48pt; font-family: sans-serif">
            Distance (million miles): 
            43.4</div>
  <div style="font-weight: bold; font-size: 36pt; line-height: 48pt; font-family: sans-serif">
            Name: 
            Venus</div>
  <div style="font-size: 36pt; line-height: 48pt; font-family: sans-serif">
            Mass (Earth = 1): 
            .815</div>
  <div style="font-size: 36pt; line-height: 48pt; font-family: sans-serif">
            Day (Earth = 1): 
            116.75</div>
  <div style="font-size: 36pt; line-height: 48pt; font-family: sans-serif">
            Radius (in miles): 
            3716</div>
  <div style="font-size: 36pt; line-height: 48pt; font-family: sans-serif">
            Density (Earth = 1): 
            .943</div>
  <div style="font-size: 36pt; line-height: 48pt; font-family: sans-serif">
            Distance (million miles): 
            66.8</div>
  <div style="font-weight: bold; font-size: 36pt; line-height: 48pt; font-family: sans-serif">
            Name: 
            Earth</div>
  <div style="font-size: 36pt; line-height: 48pt; font-family: sans-serif">
            Mass (Earth = 1): 
            1</div>
  <div style="font-size: 36pt; line-height: 48pt; font-family: sans-serif">
            Day (Earth = 1): 
            1</div>
  <div style="font-size: 36pt; line-height: 48pt; font-family: sans-serif">
            Radius (in miles): 
            2107</div>
  <div style="font-size: 36pt; line-height: 48pt; font-family: sans-serif">
            Density (Earth = 1): 
            1</div>
  <div style="font-size: 36pt; line-height: 48pt; font-family: sans-serif">
            Distance (million miles): 
            128.4</div>
</span></td>
</tr>
</table>
</body>
</html>