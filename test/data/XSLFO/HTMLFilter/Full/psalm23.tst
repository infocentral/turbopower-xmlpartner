<html>
<style>
{ margin-left: 71pt; margin-top: 28pt; margin-right: 71pt; margin-bottom: 57pt;}
</style>
<body>
<table border="0" width="567">
<tr>
<td valign="top" width="*" height="113"><span>
  <div style="text-align: right; font-size: 10pt; font-family: serif; line-height: 14pt">Psalm 23 - p. 1</div>
</span></td>
</tr>
<tr><td valign="top" width="*"><span>
  <div style="font-size: 18pt; font-family: sans-serif; line-height: 24pt; padding-bottom: 15pt; background-color: blue; color: white; text-align: center; padding-top: 3pt">Psalm 23</div>
  <div style="font-size: 16pt; font-family: sans-serif; line-height: 20pt;  padding-bottom: 10pt; text-align: left; padding-top: 3pt">Scottish Psalter
    <div>1650</div>
  </div>
  <div style="font-size: 12pt; font-family: sans-serif; line-height: 15pt; padding-bottom: 3pt; text-align: left">
    <div style="padding-top: 8pt; padding-bottom: 8pt">
      <div>The LORD's my shepherd, I'll not want:</div>
      <div>He makes me down to lie</div>
      <div>In pastures green; He leadeth me</div>
      <div>The quiet waters by.</div>
    </div>
    <div style="padding-top: 8pt; padding-bottom: 8pt">
      <div>Goodness and mercy all my life</div>
      <div>Shall surely follow me;</div>
      <div>And in God's house for evermore</div>
      <div>My dwelling place shall be.</div>
    </div>
  </div>
</span></td>
</tr>
</table>
</body>
</html>