<?xml version='1.0'?>

<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:fo="http://www.w3.org/1999/XSL/Format"
  >

<xsl:template match="/">
<fo:root>
<fo:layout-master-set>
    
<fo:simple-page-master master-name="first"
  page-height="21cm" 
  page-width="15cm"
  margin-top="1cm" 
  margin-bottom="2cm" 
  margin-left="2.5cm" 
  margin-right="2.5cm">
  <fo:region-body margin-top="3cm"/>
  <fo:region-before extent="3cm"/>
  <fo:region-after extent="1.5cm"/>
</fo:simple-page-master>

<fo:simple-page-master master-name="rest"
  page-height="21cm" 
  page-width="15cm"
  margin-top="1cm" 
  margin-bottom="2cm" 
  margin-left="2.5cm" 
  margin-right="2.5cm">
  <fo:region-body margin-top="2.5cm"/>
  <fo:region-before extent="2.5cm"/>
  <fo:region-after extent="1.5cm"/>
</fo:simple-page-master>

<fo:page-sequence-master master-name="defaultPSM" >
  <fo:repeatable-page-master-alternatives>
    <fo:conditional-page-master-reference master-name="first"
      page-position="first" />
    <fo:conditional-page-master-reference master-name="rest"
      page-position="rest" />
    <!-- recommended fallback procedure -->
    <fo:conditional-page-master-reference master-name="rest" />
  </fo:repeatable-page-master-alternatives>
</fo:page-sequence-master>

</fo:layout-master-set>

<fo:page-sequence master-name="first">

<fo:static-content flow-name="xsl-region-before">
  <fo:block text-align="end" 
    font-size="10pt" 
    font-family="serif" 
    line-height="14pt" >
    <xsl:value-of select="/Psalm/Title"/> - p. <fo:page-number/>
  </fo:block>
</fo:static-content> 

<fo:flow flow-name="xsl-region-body">
  <fo:block font-size="18pt" 
    font-family="sans-serif" 
    line-height="24pt"
    space-after.optimum="15pt"
    background-color="blue"
    color="white"
    text-align="center"
    padding-top="3pt">
    <xsl:apply-templates select="Psalm/Title"/>
  </fo:block>

  <fo:block font-size="16pt" 
    font-family="sans-serif" 
    line-height="20pt"
    space-before.optimum="10pt"
    space-after.optimum="10pt"
    text-align="start"
    padding-top="3pt">
    <xsl:apply-templates select="Psalm/Source"/>
    <fo:block>
      <xsl:apply-templates select="Psalm/Date"/>
    </fo:block>
  </fo:block>

  <fo:block font-size="12pt" 
    font-family="sans-serif" 
    line-height="15pt"
    space-after.optimum="3pt"
    text-align="start">
    <xsl:apply-templates select="Psalm/Verse"/>
  </fo:block>
</fo:flow>
<!-- This code gives an error in the Antenna House XSL Formatter. I am not sure why.
<fo:static-content flow-name="xsl-region-after">
  <fo:block text-align="end" 
    font-size="10pt" 
    font-family="serif" 
    line-height="14pt" >
    <xsl:value-of select="/Psalm/Title"/> - p. <fo:page-number/>
  </fo:block>
</fo:static-content> 
-->
</fo:page-sequence>

</fo:root>
</xsl:template>

<xsl:template match="Psalm/Title">
  <xsl:value-of select="."/>
</xsl:template>

<xsl:template match="Psalm/Verse">
  <fo:block space-before="8pt" space-after="8pt">
    <xsl:apply-templates select="Line"/>
  </fo:block>
</xsl:template>
 
<xsl:template match="Psalm/Source">
  <xsl:value-of select="."/>
</xsl:template>

<xsl:template match="Psalm/Date">
  <xsl:value-of select="."/>
</xsl:template>

<xsl:template match="Line">
  <fo:block>
    <xsl:value-of select="."/>
  </fo:block>
</xsl:template>

</xsl:stylesheet>
