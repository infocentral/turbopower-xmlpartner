{\rtf\ansi\deff0{\fonttbl{\f0\froman Times New Roman;}{\f1\fnil sans-serif;}}{\colortbl\red0\green0\blue0;\red0\green0\blue255;\red255\green255\blue255;}{\stylesheet}
\margb1134\margr1417\margt567\margl1417\paperh11906\paperw8504\nolead\paperh11906\paperw8504\margb1134\margr1417\margt2280\margl1417\widowctrl\ftnbj\aenddoc\noxlattoyen\expshrtn\noultrlspc\dntblnsbdb\nospaceforul\hyphcaps0\formshade\horzdoc\dgmargin\dghspace180\dgvspace180\dghorigin1800\dgvorigin1440\dghshow1\dgvshow1 \jexpand\viewkind1\viewscale75\pgbrdrhead\pgbrdrfoot\splytwnine\ftnlytwnine\htmautsp\nolnhtadjtbl\useltbaln\alntblind\lytcalctblwd\lyttblrtgr\lnbrkrule \fet0\sectd \linex0\endnhere\sectlinegrid360\sectdefaultcl{\*\pnseclvl1 \pnucrm\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl2\pnucltr\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl3\pndec\pnstart1\pnindent720\pnhang{\pntxta .}}{\*\pnseclvl4\pnlcltr\pnstart1\pnindent720\pnhang{\pntxta )}}{\*\pnseclvl5 \pndec\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl6\pnlcltr\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl7\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}{\*\pnseclvl8\pnlcltr\pnstart1\pnindent720\pnhang {\pntxtb (}{\pntxta )}}{\*\pnseclvl9\pnlcrm\pnstart1\pnindent720\pnhang{\pntxtb (}{\pntxta )}}\pard\plain \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \fs24\lang1033\langfe1033\cgrid\langnp1033\langfenp1033 {\fs20\lang1024\langfe1024\noproof{\shp{\*\shpinst\shptop-1701\shpleft-15\shpright5663\shpbottom0\shpfhdr0\shpbxcolumn\shpbxignore\shpbypara\shpbyignore\shpwr3\shpwrk0\shpfblwtxt0\shpz0\shplid1028{\sp{\sn shapeType}{\sv 202}}{\sp{\sn fFlipH}{\sv 0}}{\sp{\sn fFlipV}{\sv 0}}{\sp{\sn lTxid}{\sv 65536}}{\sp{\sn fLine}{\sv 0}}{\sp{\sn fLayoutInCell}{\sv 1}}{\sp{\sn dxTextLeft}{\sv 0}}{\sp{\sn dyTextTop}{\sv 0}}{\sp{\sn dxTextRight}{\sv 0}}{\sp{\sn dyTextBottom}{\sv 0}}{\sp{\sn fLine}{\sv 0}}{\shptxt \pard\plain \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0\f2\fs20\cbpat2\cf0\b0 \i0 \qr Psalm 23 - p. \f2\fs20\cbpat2\cf0\b0 \i0 1\f2\fs20\cbpat2\cf0\b0 \i0 
{\fs22\par}}}}{\shprslt{\*\do\dobxcolumn\dobypara\dodhgt8192\dptxbx\dptxlrtb{\dptxbxtext\pard\plain \li0\ri0\widctlpar\aspalpha\aspnum\faauto\adjustright\rin0\lin0\itap0 \dptxbxmar0\f2\fs20\cbpat2\cf0\b0 \i0 Psalm 23 - p. \f2\fs20\cbpat2\cf0\b0 \i0 1\f2\fs20\cbpat2\cf0\b0 \i0 
{\fs22\par}}\dpx-15\dpy1701\dpxsize5648\dpysize1701\dpfillfgcr255\dpfillfgcg255\dpfillfgcb255\dpfillbgcr255\dpfillbgcg255\dpfillbgcb255\dpfillpat1\dplinehollow}}}\f1\fs36\cbpat1\cf2\b0 \i0 \qc Psalm 23\f1\fs36\cbpat1\cf2\b0 \i0 
{\fs42\par}\f3\fs20\cbpat2\cf0\b0 \i0 
{\fs34\par}\f3\fs20\cbpat2\cf0\b0 \i0 
{\fs22\par}\f1\fs32\cbpat2\cf0\b0 \i0 \ql Scottish Psalter\f1\fs32\cbpat2\cf0\b0 \i0 
{\fs36\par}\f1\fs32\cbpat2\cf0\b0 \i0 \ql 1650\f1\fs32\cbpat2\cf0\b0 \i0 
{\fs36\par}\f3\fs20\cbpat2\cf0\b0 \i0 
{\fs22\par}\f1\fs24\cbpat2\cf0\b0 \i0 
{\fs20\par}\f1\fs24\cbpat2\cf0\b0 \i0 \ql The LORD's my shepherd, I'll not want:\f1\fs24\cbpat2\cf0\b0 \i0 
{\fs28\par}\f1\fs24\cbpat2\cf0\b0 \i0 \ql He makes me down to lie\f1\fs24\cbpat2\cf0\b0 \i0 
{\fs28\par}\f1\fs24\cbpat2\cf0\b0 \i0 \ql In pastures green; He leadeth me\f1\fs24\cbpat2\cf0\b0 \i0 
{\fs28\par}\f1\fs24\cbpat2\cf0\b0 \i0 \ql The quiet waters by.\f1\fs24\cbpat2\cf0\b0 \i0 
{\fs28\par}\f1\fs24\cbpat2\cf0\b0 \i0 
{\fs20\par}\f1\fs24\cbpat2\cf0\b0 \i0 
{\fs20\par}\f1\fs24\cbpat2\cf0\b0 \i0 \ql Goodness and mercy all my life\f1\fs24\cbpat2\cf0\b0 \i0 
{\fs28\par}\f1\fs24\cbpat2\cf0\b0 \i0 \ql Shall surely follow me;\f1\fs24\cbpat2\cf0\b0 \i0 
{\fs28\par}\f1\fs24\cbpat2\cf0\b0 \i0 \ql And in God's house for evermore\f1\fs24\cbpat2\cf0\b0 \i0 
{\fs28\par}\f1\fs24\cbpat2\cf0\b0 \i0 \ql My dwelling place shall be.\f1\fs24\cbpat2\cf0\b0 \i0 
{\fs28\par}\f1\fs24\cbpat2\cf0\b0 \i0 
{\fs20\par}\f3\fs20\cbpat2\cf0\b0 \i0 
{\fs8\par}}