<?xml version="1.0" encoding="ISO-8859-1"?>
<fo:root>
  <fo:layout-master-set>
    <fo:simple-page-master master-name="page" page-height="297mm" page-width="210mm" margin-top="20mm" margin-bottom="10mm" margin-left="25mm" margin-right="25mm">
      <fo:region-body margin-top="0mm" margin-bottom="15mm" margin-left="0mm" margin-right="0mm"/>
      <fo:region-after extent="10mm"/>
    </fo:simple-page-master>
  </fo:layout-master-set>
  <fo:page-sequence master-name="page">
    <fo:static-content flow-name="xsl-region-after">
      <fo:block>Page 
        <fo:page-number/>
      </fo:block>
    </fo:static-content>
    <fo:flow flow-name="xsl-region-body">
      <fo:block id="x1.1.1">1. Chapter</fo:block>
      <fo:block>Text</fo:block>
      <fo:block id="x1.2.1">2. Chapter</fo:block>
      <fo:block>For a description of X seepage 
        <fo:page-number-citation refid="x1.1.1"/>.</fo:block>
    </fo:flow>
  </fo:page-sequence>
</fo:root>