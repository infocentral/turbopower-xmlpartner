<?xml version="1.0" encoding="UTF-8"?>
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <fo:layout-master-set>
        <fo:simple-page-master margin-right="20mm" margin-left="20mm" 
            margin-bottom="10mm" margin-top="10mm" page-width="300mm" 
            page-height="400mm" master-name="page">

            <fo:region-body margin-right="0mm" margin-left="0mm" 
            margin-bottom="10mm" margin-top="0mm"/>

            <fo:region-after extent="10mm"/>
        </fo:simple-page-master>
    </fo:layout-master-set>

    <fo:page-sequence master-name="page">
        <!-- Added for fop -->
        <fo:sequence-specification>
            <fo:sequence-specifier-single master-name="page"/>
        </fo:sequence-specification>
        <!-- Added for fop -->

        <fo:flow>
            <fo:list-block 
                provisional-distance-between-starts="15mm"
                provisional-label-separation="5mm">

                 <fo:list-item line-height="20mm">
                    <fo:list-item-label>
                        <fo:block font-family="sans-serif" 
                            font-size="36pt">
                            1.
                        </fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body>
                        <fo:block font-family="sans-serif" 
                            font-size="36pt">
                            Tic.
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
                <fo:list-item line-height="20mm">
                    <fo:list-item-label>
                        <fo:block font-family="sans-serif" 
                            font-size="36pt">
                            2.
                        </fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body>
                        <fo:block font-family="sans-serif" 
                            font-size="36pt">
                            Tac.
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
                 <fo:list-item line-height="20mm">
                    <fo:list-item-label>
                        <fo:block font-family="sans-serif" 
                            font-size="36pt">
                            3.
                        </fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body>
                        <fo:block font-family="sans-serif" 
                            font-size="36pt">
                            Toe.
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </fo:list-block>
        </fo:flow>
    </fo:page-sequence>
</fo:root>