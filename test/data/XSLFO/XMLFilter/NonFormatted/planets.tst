<?xml version="1.0" encoding="ISO-8859-1"?>
<fo:root><fo:layout-master-set><fo:simple-page-master master-name="page" page-height="400mm" page-width="300mm" margin-top="10mm" margin-bottom="10mm" margin-left="20mm" margin-right="20mm"><fo:region-body margin-top="0mm" margin-bottom="10mm" margin-left="0mm" margin-right="0mm"/><fo:region-after extent="10mm"/></fo:simple-page-master></fo:layout-master-set><fo:page-sequence master-name="page"><fo:sequence-specification><fo:sequence-specifier-single master-name="page"/></fo:sequence-specification><fo:flow><fo:block font-weight="bold" font-size="36pt" line-height="48pt" font-family="sans-serif">
            Name: 
            Mercury</fo:block><fo:block font-size="36pt" line-height="48pt" font-family="sans-serif">
            Mass (Earth = 1): 
            .0553</fo:block><fo:block font-size="36pt" line-height="48pt" font-family="sans-serif">
            Day (Earth = 1): 
            58.65</fo:block><fo:block font-size="36pt" line-height="48pt" font-family="sans-serif">
            Radius (in miles): 
            1516</fo:block><fo:block font-size="36pt" line-height="48pt" font-family="sans-serif">
            Density (Earth = 1): 
            .983</fo:block><fo:block font-size="36pt" line-height="48pt" font-family="sans-serif">
            Distance (million miles): 
            43.4</fo:block><fo:block font-weight="bold" font-size="36pt" line-height="48pt" font-family="sans-serif">
            Name: 
            Venus</fo:block><fo:block font-size="36pt" line-height="48pt" font-family="sans-serif">
            Mass (Earth = 1): 
            .815</fo:block><fo:block font-size="36pt" line-height="48pt" font-family="sans-serif">
            Day (Earth = 1): 
            116.75</fo:block><fo:block font-size="36pt" line-height="48pt" font-family="sans-serif">
            Radius (in miles): 
            3716</fo:block><fo:block font-size="36pt" line-height="48pt" font-family="sans-serif">
            Density (Earth = 1): 
            .943</fo:block><fo:block font-size="36pt" line-height="48pt" font-family="sans-serif">
            Distance (million miles): 
            66.8</fo:block><fo:block font-weight="bold" font-size="36pt" line-height="48pt" font-family="sans-serif">
            Name: 
            Earth</fo:block><fo:block font-size="36pt" line-height="48pt" font-family="sans-serif">
            Mass (Earth = 1): 
            1</fo:block><fo:block font-size="36pt" line-height="48pt" font-family="sans-serif">
            Day (Earth = 1): 
            1</fo:block><fo:block font-size="36pt" line-height="48pt" font-family="sans-serif">
            Radius (in miles): 
            2107</fo:block><fo:block font-size="36pt" line-height="48pt" font-family="sans-serif">
            Density (Earth = 1): 
            1</fo:block><fo:block font-size="36pt" line-height="48pt" font-family="sans-serif">
            Distance (million miles): 
            128.4</fo:block></fo:flow></fo:page-sequence></fo:root>