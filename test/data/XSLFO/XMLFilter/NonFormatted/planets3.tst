<?xml version="1.0" encoding="UTF-8"?>
<fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <fo:layout-master-set>
        <fo:simple-page-master margin-right="20mm" 
            margin-left="20mm" margin-bottom="20mm" 
            margin-top="20mm" page-width="300mm" 
            page-height="400mm" master-name="page">

            <fo:region-body margin-right="0mm" margin-left="0mm" 
                margin-bottom="20mm" margin-top="0mm"/>

            <fo:region-after extent="20mm"/>

        </fo:simple-page-master>
    </fo:layout-master-set>

    <fo:page-sequence master-name="page">

        <!-- Added for fop -->
        <fo:sequence-specification>
            <fo:sequence-specifier-single master-name="page"/>
        </fo:sequence-specification>
        <!-- Added for fop -->

        <fo:flow>
            <fo:table width="12cm" table-layout="fixed">
                <fo:table-column column-number="1" column-width="25mm">
                </fo:table-column>
                <fo:table-column column-number="2" column-width="25mm">
                </fo:table-column>
                <fo:table-column column-number="3" column-width="25mm">
                </fo:table-column>
                <fo:table-body>
                     <fo:table-row line-height="20mm">
                         <fo:table-cell column-number="1">
                             <fo:block font-family="sans-serif" 
                                 font-size="36pt">
                                 Tic
                             </fo:block>
                         </fo:table-cell>
                         <fo:table-cell column-number="2">
                             <fo:block font-family="sans-serif" 
                                 font-size="36pt">
                                 Tac
                             </fo:block>
                         </fo:table-cell>
                         <fo:table-cell column-number="3">
                             <fo:block font-family="sans-serif" 
                                 font-size="36pt">
                                 Toe
                             </fo:block>
                         </fo:table-cell>
                     </fo:table-row>
                     <fo:table-row line-height="20mm">
                         <fo:table-cell column-number="1">
                             <fo:block font-family="sans-serif" 
                                 font-size="36pt">
                                 Tic
                             </fo:block>
                         </fo:table-cell>
                         <fo:table-cell column-number="2">
                             <fo:block font-family="sans-serif" 
                                 font-size="36pt">
                                 Tac
                             </fo:block>
                         </fo:table-cell>
                         <fo:table-cell column-number="3">
                             <fo:block font-family="sans-serif" 
                                 font-size="36pt">
                                 Toe
                             </fo:block>
                         </fo:table-cell>
                     </fo:table-row>
                     <fo:table-row line-height="20mm">
                         <fo:table-cell column-number="1">
                             <fo:block font-family="sans-serif" 
                                 font-size="36pt">
                                 Tic
                             </fo:block>
                         </fo:table-cell>
                         <fo:table-cell column-number="2">
                             <fo:block font-family="sans-serif" 
                                 font-size="36pt">
                                 Tac
                             </fo:block>
                         </fo:table-cell>
                         <fo:table-cell column-number="3">
                             <fo:block font-family="sans-serif" 
                                 font-size="36pt">
                                 Toe
                             </fo:block>
                         </fo:table-cell>
                     </fo:table-row>
               </fo:table-body>
            </fo:table>
        </fo:flow>
    </fo:page-sequence>
</fo:root>