<xsl:stylesheet version="1.0"
                      xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="item | special-item">
  <xsl:number count="item | special-item"/>
  <xsl:text> </xsl:text>
  <xsl:value-of select="."/><br/>
</xsl:template>

</xsl:stylesheet>

