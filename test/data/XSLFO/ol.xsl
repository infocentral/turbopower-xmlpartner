<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version='1.0'>

    <xsl:template match="ol">
        <fo:root>

            <fo:layout-master-set>
                 <fo:simple-page-master master-name="page"
                     page-height="400mm" page-width="300mm"
                     margin-top="10mm" margin-bottom="10mm"
                     margin-left="20mm" margin-right="20mm">

                     <fo:region-body
                       margin-top="0mm" margin-bottom="10mm"
                       margin-left="0mm" margin-right="0mm"/>

                     <fo:region-after extent="10mm"/>
                 </fo:simple-page-master>
             </fo:layout-master-set>

             <fo:page-sequence master-name="page">
                 <fo:flow>
                   <fo:list-block provisional-distance-between-starts="15mm"
                      provisional-label-separation="5mm">
                     <xsl:apply-templates/>
                   </fo:list-block>
                 </fo:flow>
             </fo:page-sequence>

        </fo:root>
    </xsl:template>

<xsl:template match="ol/item">
  <fo:list-item>
    <fo:list-item-label start-indent="5mm" end-indent="label-end()">
      <fo:block>
        <xsl:number format="a."/>
      </fo:block>
    </fo:list-item-label>
    <fo:list-item-body start-indent="body-start()">
      <fo:block>
        <xsl:apply-templates/>
      </fo:block>
    </fo:list-item-body>
  </fo:list-item>
</xsl:template>

</xsl:stylesheet>


