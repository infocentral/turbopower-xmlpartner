<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.1">

<xsl:template match="/">
  <xsl:document href="test.xml" method="xml">
    <xsl:fallback>
      <xsl:message terminate="yes">
        xsl:document not supported by this XSL processor.
      </xsl:message>
    </xsl:fallback>
  </xsl:document>
</xsl:template>

</xsl:stylesheet>