<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.1">

<xsl:attribute-set name="A" use-attribute-sets="B">
  <xsl:attribute name="A1">A1</xsl:attribute>
</xsl:attribute-set>

<xsl:attribute-set name="B" use-attribute-sets="A">
  <xsl:attribute name="B1">B1</xsl:attribute>
</xsl:attribute-set>

<xsl:template match="/">
  <root xsl:use-attribute-sets="A"/>
</xsl:template>

</xsl:stylesheet>
