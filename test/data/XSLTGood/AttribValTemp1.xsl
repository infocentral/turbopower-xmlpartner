<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
version="1.0">
   <xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>
   <!-- Variables -->
   <xsl:variable name="viewertype">
      <xsl:value-of select="/*/PARAMS/PARAM[@name='viewertype']/@value"/>
   </xsl:variable>
   <!-- VIEWERS tag -->
   <xsl:template match="VIEWERS">
      <DOCUMENT title="{$viewertype} viewers" source="cmd_manage_viewers">
         <MENU>
            <OPTION text="Remote control"
                    shorttext="Remote"
                    url="/ovremotecontrol/menu.xml?cmd=ovinternalpage.get"
                    source="cmd_remote_controller"
            />
         </MENU>
         <!-- VIEWER tag -->
         <xsl:for-each select="VIEWER">
            <xsl:sort select="@viewerurl"/>
            <LINK text="Edit {@viewerurl}"
                  shorttext="{@viewerurl}"
                  title="Edit"
url="/?cmd=listprofilesinviewer&amp;viewerurl={@viewerurl}&amp;viewertype={$
viewertype}"
                  source="cmd_edit_viewer"
            />
         </xsl:for-each>
      </DOCUMENT>
   </xsl:template>

</xsl:stylesheet>
