<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">


  <xsl:decimal-format name="european" decimal-separator="," grouping-separator="."/>


  <xsl:template match="/">

    <xsl:value-of select="format-number(47954.79,'##.##0,00','european')"/>
    <test attrib="{{"/>
</xsl:template>

</xsl:stylesheet>