<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/TestData">
  <xsl:apply-templates select="//Content" mode="ShowAVT"/>
</xsl:template>

	<!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  Template: ShowAVT
  Description: shows how the position funciotn doesn't work in an AVT situation
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
<xsl:template match="Content" mode="ShowAVT">
	<font size="{position()}">Font Size
		<xsl:value-of select="position()"/>
	</font>
	<br/>
</xsl:template>

</xsl:stylesheet>