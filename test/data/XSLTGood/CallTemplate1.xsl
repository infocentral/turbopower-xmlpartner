<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/Document">
 Top 3 Elements Sum
 <br/>
 <xsl:call-template name="RecursiveSum">
  <xsl:with-param name="set-of-data" select="//Record"/>
  <xsl:with-param name="LineNo" select="3"/>
  <xsl:with-param name="FieldName" select="'Value'"/>
 </xsl:call-template>
</xsl:template>

<xsl:template name="RecursiveSum">
 <xsl:param name="set-of-data"/>
 <xsl:param name="sum">0</xsl:param>
 <xsl:param name="LineNo"/>
 <xsl:param name="FieldName"/>
 <xsl:choose>
  <xsl:when test="number($LineNo) &gt; 0">
   <xsl:for-each select="$set-of-data">
    <xsl:sort select="number(attribute::*[name()=$FieldName])"
data-type="number" order="descending"/>
    <xsl:if  test="position()  = number($LineNo)  ">
     <xsl:text> Element </xsl:text>
     <xsl:value-of select="$LineNo"/>
     <xsl:text> </xsl:text>
     <xsl:value-of select="@Value"/><br/>

     <xsl:call-template name="RecursiveSum">
      <xsl:with-param name="set-of-data" select="$set-of-data"/>
      <xsl:with-param name="sum">
       <xsl:value-of select="number($sum) +
number(attribute::*[name()=$FieldName])"/>
      </xsl:with-param>
      <xsl:with-param name="LineNo" select="number($LineNo) - 1"/>
      <xsl:with-param name="FieldName" select="$FieldName"/>
     </xsl:call-template>

    </xsl:if>
   </xsl:for-each>
  </xsl:when>
  <xsl:otherwise>
   <xsl:value-of select="$sum"/>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>

</xsl:stylesheet>
