<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/Document">
 <xsl:for-each select="//Document/Record">
  <xsl:variable name="n" select="@Name"/>
  <xsl:variable name="v" select="@Value"/>
  <xsl:call-template name="Sample">
   <xsl:with-param name="nm" select="$n"/>
   <xsl:with-param name="vl">
    <xsl:call-template name="MultiplyByTwo">
     <xsl:with-param name="num"  select="$v"/>
    </xsl:call-template>
   </xsl:with-param>
  </xsl:call-template>
 </xsl:for-each>
</xsl:template>

<xsl:template name="Sample">
 <xsl:param name="nm"/>
 <xsl:param name="vl"/>

 <xsl:text>Name:</xsl:text>
 <xsl:value-of select="$nm"/>
 <br/>
 <xsl:text>Value:</xsl:text>
 <xsl:value-of select="$vl"/>
 <br/>
 <br/>
</xsl:template>

<xsl:template name="MultiplyByTwo">
 <xsl:param name="num"/>
 <xsl:value-of select="number($num) * 2 "/>

</xsl:template>

</xsl:stylesheet>