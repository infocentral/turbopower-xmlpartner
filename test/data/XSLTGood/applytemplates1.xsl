<?xml version="1.0"?>
<xsl:stylesheet version="1.0">

<xsl:template match="/">
  <xsl:apply-templates select="one">
    <!-- This is a comment -->
    <xsl:with-param name="testvar">
      <xsl:if test="1 = 1">foo</xsl:if>
    </xsl:with-param>
  </xsl:apply-templates>
</xsl:template>

<xsl:template match="one">
  <xsl:value-of select="."/>
</xsl:template>

</xsl:stylesheet>