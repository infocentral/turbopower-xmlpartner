<?xml version="1.0"?>
<xsl:stylesheet version="1.0">

<xsl:template match="booklist">
  <xsl:apply-templates select="book">
    <xsl:sort select="author"/>
    <xsl:sort select="@sales" order="descending" data-type="number"/>
  </xsl:apply-templates>
</xsl:template>

<xsl:template match="book">
  <h2>
  <xsl:value-of select="title"/>
  </h2>
  <xsl:value-of select="author"/>
  <xsl:text> - </xsl:text>
  <xsl:value-of select="@sales"/>
  <p/>
</xsl:template>

</xsl:stylesheet>
