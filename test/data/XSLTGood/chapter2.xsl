<?xml version="1.0"?>
<xsl:stylesheet version="1.0">
  <xsl:template match="title">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template match="title/chapter">
    <xsl:value-of select="."/>
  </xsl:template>
</xsl:stylesheet>