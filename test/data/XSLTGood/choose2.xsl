<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
        <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
        <!-- Leave all the nodes upper than <body> node as is -->
        <xsl:template match="node()">
                <xsl:copy>
                        <xsl:copy-of select="@*"/>
                        <xsl:apply-templates/>
                </xsl:copy>
        </xsl:template>

        <xsl:template match="node()" mode="inside">
                <xsl:copy>
                        <xsl:copy-of select="@*"/>
                        <xsl:for-each select="child::node()">
                                <xsl:choose>
                                        <xsl:when test="self::text()">
                                                &lt;<xsl:value-of select="self::text()"/>&gt;
                                        </xsl:when>
                                        <xsl:otherwise>
                                                <xsl:apply-templates select="." mode="inside"/>
                                        </xsl:otherwise>
                                </xsl:choose>
                        </xsl:for-each>
                </xsl:copy>
        </xsl:template>

        <xsl:template match="body">
                <xsl:apply-templates select="." mode="inside"/>
        </xsl:template>
</xsl:stylesheet>
