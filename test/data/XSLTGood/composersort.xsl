<?xml version="1.0"?>
<xsl:stylesheet version="1.0">

  <xsl:template match="/">
    <html>
       <xsl:apply-templates select="catalog"/>
    </html>
  </xsl:template>

  <xsl:template match="catalog">
    <head>
       <title><xsl:value-of select="category"/></title>
    </head>
    <body>
       <h1><xsl:value-of select="category"/></h1>
       <xsl:apply-templates select="composer">
         <xsl:sort select="name/last_name"/>
         <xsl:sort select="name/first_name"/>
         <xsl:sort select="name/middle_name"/>
       </xsl:apply-templates>
       <hr/>
       Copyright <xsl:value-of select="copyright"/><br/>
       Last Modified: <xsl:value-of select="last_updated"/><br/>
       <xsl:apply-templates select="maintainer"/>
    </body>
  </xsl:template>

  <xsl:template match="composer">
    <h2><xsl:value-of select="name"/></h2>
    <xsl:apply-templates select="../composition[@composer=current()/@id]"/>
  </xsl:template>

  <xsl:template match="composition">
    <h3><xsl:value-of select="title"/></h3>

    <ul>
     <xsl:if test="string(date)">
       <li><xsl:value-of select="date"/></li>
     </xsl:if>
     <xsl:if test="string(length)">
       <li><xsl:value-of select="length"/></li>
     </xsl:if>
     <xsl:if test="string(instruments)">
       <li><xsl:value-of select="instruments"/></li>
     </xsl:if>
     <xsl:if test="string(publisher)">
       <li><xsl:value-of select="publisher"/></li>
     </xsl:if>
    </ul>

    <p><xsl:apply-templates select="description"/></p>

  </xsl:template>


</xsl:stylesheet>