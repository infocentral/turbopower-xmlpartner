<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

  <!-- Verify that xsl:copy-of does not fail if zero nodes match its select attribute -->

  <xsl:output method = "xml" indent = "yes" />

  <xsl:template match = "/" >
    <QQQ >
      <xsl:copy-of select = "//XXX" />
    </QQQ>
  </xsl:template>
</xsl:stylesheet>