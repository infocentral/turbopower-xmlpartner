<?xml version="1.0"?>
<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">

<xsl:template match="/">
        <xsl:copy-of select="HTML_DATA/node()" />
        <xsl:text disable-output-escaping="yes">
Disabled&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;es
caping </xsl:text>
        <xsl:text>
          nbsp in
        </xsl:text>
        <![CDATA[&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]]>
        <xsl:text>CData section</xsl:text>
</xsl:template>

</xsl:transform>
