<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

<!-- format-number examples pulled from Kay's XSLT Programmer's Reference -->

<xsl:output method = "text" />

<xsl:decimal-format
  decimal-separator=","
  grouping-separator="."/>

<xsl:template match = "/" >
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number('1234.5','#.##0,00')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number('123.456','#.##0,00')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number('100000','#.##0,00')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number('-59','#.##0,00')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number(1 div 0,'#.##0,00')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number('1234','###0,0###')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number('1234.5','###0,0###')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number('.00035','###0,0###')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number('0.25','#00%')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number('0.736','#00%')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number('1','#00%')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number('-42','#00%')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number('-3.12','#,00;(#,00)')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number('-3.12','#,00;#,00CR')" />
</xsl:template>

</xsl:stylesheet>