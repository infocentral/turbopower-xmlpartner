<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

<!-- format-number examples pulled from Kay's XSLT Programmer's Reference -->

<xsl:output method = "text" />

<xsl:decimal-format
  zero-digit="a"
  minus-sign="~"/>

<xsl:template match = "/" >
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number('10','aa')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number('12.34','##.##')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number('-9999999','#,###,###')" />
</xsl:template>

</xsl:stylesheet>