<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

<!-- format-number examples pulled from Kay's XSLT Programmer's Reference -->

<xsl:output method = "text" />

<xsl:decimal-format
  NaN="Not Applicable"
  infinity="Out of Range"/>

<xsl:template match = "/" >
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number('a','#.##')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number('1 div 0','##.##')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number('-1 div 0','#,###,###')" />
</xsl:template>

</xsl:stylesheet>