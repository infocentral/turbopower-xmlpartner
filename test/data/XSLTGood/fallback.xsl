<?xml version="1.0"?>
<xsl:stylesheet version="1.0">

<xsl:template match="boilerplate">
  <div id="boilerplate" xsl:version="6.1">
    <xsl:copy-to-output href="boilerplate.xhtml">
      <xsl:fallback>The xsl:fallback element was invoked.</xsl:fallback>
    </xsl:copy-to-output>
  </div>
</xsl:template>

</xsl:stylesheet>