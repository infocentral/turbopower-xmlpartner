<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
      <head>
        <title>Test Page</title>
      </head>
      <body>
        <xsl:for-each select='//value'>
          <p>
            <xsl:text>Test Value : </xsl:text>
            <xsl:value-of select="format-number(number(.),
'###,###,##0.00')"/>
          </p>
        </xsl:for-each>
        <xsl:for-each select='//value'>
          <p>
            <xsl:text>Test Value (no group sep) : </xsl:text>
            <xsl:value-of select="format-number(number(.),
'########0.00')"/>
          </p>
        </xsl:for-each>
        <xsl:for-each select='//value'>
          <p>
            <xsl:text>Test Value (no decimals) : </xsl:text>
            <xsl:value-of select="format-number(number(.),
'########0')"/>
          </p>
        </xsl:for-each>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
