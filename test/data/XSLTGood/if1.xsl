<?xml version="1.0"?>
<xsl:stylesheet version="1.0">
  <xsl:template match="/ | doc">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="section">
<xsl:text>
Section</xsl:text>
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="para">
    <p>&lt;<xsl:text/>
    <xsl:text disable-output-escaping="yes">&lt;paragraph</xsl:text></p>
    <xsl:if test="position()=last()">
      <hr/>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>