<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template match="*">
    <xsl:if test="not(@percent) or (string(number(@percent)) = 'Nan') or (number(@percent) &lt; 0) or (number(@percent) > 100)">
      <xsl:text>
    
      percent attribute must be a number between 0 and 100
    </xsl:text>
    </xsl:if>
    <xsl:if test="@percent and (number(@percent) >= 0) and (number(@percent) &lt;= 100)">
      <xsl:text>
    
      percent attribute is valid
    </xsl:text>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>