<xsl:transform select="title" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/">
    <xsl:apply-templates/>
  </xsl:template>
  <xsl:template match="book">
    <xsl:value-of select="title"/>
    <xsl:text>
    by </xsl:text>
    <xsl:for-each select="author">
      <xsl:value-of select="."/>
      <xsl:if test="position()!=last()">
        <xsl:text>, </xsl:text>
      </xsl:if>
      <xsl:if test="position()=last()-1">
        <xsl:text>and </xsl:text>
      </xsl:if>
    </xsl:for-each>
  </xsl:template>
</xsl:transform>