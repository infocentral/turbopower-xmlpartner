<xsl:transform
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 version="1.0"
>

<xsl:key name="author-name" match="book" use="author"/>

<xsl:param name="author" select="'John Vlissides'"/>

<xsl:template match="/">
<xsl:copy-of select="key('author-name', translate($author, '_', ' '))"/>
</xsl:template>

</xsl:transform>

