<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" encoding="utf-8" indent="yes"/>

<xsl:key name="object_key" match="standard" use="a"/>

<xsl:template match="*">
      <xsl:apply-templates
select="standard[count(.|key('object_key',a)[1])=1]"/>
</xsl:template>

<xsl:template match="standard">
  <answer>This is : <xsl:value-of select="a"/></answer>
</xsl:template>

</xsl:stylesheet>