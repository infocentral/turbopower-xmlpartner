<xsl:transform
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 version="1.0"
>

<xsl:template match="/">
  <html><body>
    <h1>Stylesheet Module Structure</h1>
    <ul>
    <xsl:apply-templates select="*/xsl:include | */xsl:import"/>
    </ul>
  </body></html>
</xsl:template>

<xsl:template match="xsl:include | xsl:import">
    <li><xsl:value-of select="concat(local-name(),'s ',@href)"/>
    <xsl:variable name="module" select="document(@href)"/>
    <ul>
    <xsl:apply-templates select="$module/*/xsl:include | $module/*/xsl:import"/>
    </ul>
    </li>
</xsl:template>

</xsl:transform>

