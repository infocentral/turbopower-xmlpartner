<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0"
  xmlns:date="http://www.jclark.com/xt/java.util.Date"
>

<xsl:template match="/"
  xmlns = "urn:acme-com;gregorian">
  <date><xsl:value-of select="$today"/></date>
</xsl:template>

<xsl:param name="today" select="'2000-13-18'"/>

</xsl:stylesheet>