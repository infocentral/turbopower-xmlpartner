<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
 <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
 <xsl:template match="node()">
  <xsl:value-of select="name()"/>:<xsl:value-of select="node()"/>
 </xsl:template>
 <xsl:template match="/">
 <html>
 <body>
  <xsl:apply-templates select="/ForeFather/GreatGrandparent//."/>
 </body>
 </html>
 </xsl:template>
</xsl:stylesheet>