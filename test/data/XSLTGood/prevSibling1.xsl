<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="text" indent="no" encoding="UTF-8"/>
  <xsl:preserve-space elements="*" />

  <xsl:template match="/">
     <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="sec">
    <xsl:call-template name="foo">
      <xsl:with-param name="nodelist" select="node()"/>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="foo">
    <xsl:param name="nodelist" select="."/>
    <xsl:for-each select="$nodelist">
      <xsl:if test="not(preceding-sibling::node())">
        <xsl:text>foo </xsl:text>
      </xsl:if>
      <xsl:apply-templates select="."/>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>