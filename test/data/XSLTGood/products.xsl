<?xml version="1.0"?>
<xsl:stylesheet version="1.0">
  <xsl:template match="/">
    <products>
      <xsl:for-each select="products/product">
         <xsl:sort select="sum(region/@sales)"
                                      data-type="number"
                                      order="descending"/>
         <product name="{@name}" sales="{format-number(sum(region/@sales), '$####0.00')}"/>
      </xsl:for-each>
    </products>
  </xsl:template>
</xsl:stylesheet>

