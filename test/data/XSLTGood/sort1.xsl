<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
        
  <xsl:template match="/">
                
    <html>
      <body>
                        
        <table>
                                
          <xsl:apply-templates select="/xCard/vCard">
                                        
            <xsl:sort select="N/FAMILY"/>
                                        
            <xsl:sort select="N/GIVEN"/>
                                        
            <xsl:sort select="ORG/ORGNAME"/>
                                </xsl:apply-templates>
                        </table>
                </body>
    </html>
        </xsl:template>
        
  <xsl:template match="vCard">
                
    <tr>
                        
      <td>
        <xsl:value-of select="N/FAMILY"/>
      </td>
                        
      <td>
        <xsl:value-of select="N/GIVEN"/>
      </td>
                        
      <td>
        <xsl:value-of select="ORG/ORGNAME"/>
      </td>
                </tr>
        </xsl:template>
</xsl:stylesheet>