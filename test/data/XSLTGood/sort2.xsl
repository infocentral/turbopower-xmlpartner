<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/TestData">
  <root>
  <xsl:apply-templates select="Content">
          <xsl:sort select="." order="ascending"/>
  </xsl:apply-templates>
  </root>
</xsl:template>

        <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  Template: Content Node
  Description:
  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
        <xsl:template match="Content">
            <content>
                        <xsl:value-of select="position()"/> :
                        <xsl:value-of select="."/>
            </content>
        </xsl:template>
</xsl:stylesheet>