<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl" version="1.0">
  <xsl:template match="/">
    <xsl:value-of select="sum(/one/value)"/>
    <xsl:text>
</xsl:text>
    <xsl:value-of select="sum(/one/value/text())"/>
  </xsl:template>
</xsl:stylesheet>
