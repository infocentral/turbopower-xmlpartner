<xsl:stylesheet version="1.0"
     xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="template1.xsl"/>

<xsl:template match="/">
<html><body>
    <xsl:apply-templates/>
    <table bgcolor="#cccccc" border="1" cellpadding="5">
    <tr>
        <td><b>Date</b></td>
        <td><b>Venue</b></td>
        <td><b>Composer</b></td>
        <td><b>Work</b></td>
        <td><b>Role</b></td>
    </tr>
    <xsl:apply-templates mode="index"/>
    </table>
</body></html>
</xsl:template>

<xsl:template match="performance" mode="index">
    <tr>
    <td><xsl:value-of select="date"/>&#xa0;</td>
    <td><xsl:value-of select="venue"/>&#xa0;</td>
    <td><xsl:value-of select="composer"/>&#xa0;</td>
    <td><xsl:value-of select="work"/>&#xa0;</td>
    <td><xsl:value-of select="role"/>&#xa0;</td>
    </tr>
</xsl:template>


</xsl:stylesheet>
