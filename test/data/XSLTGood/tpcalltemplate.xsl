<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0">

<xsl:template name="RPadDots">
    <xsl:param name="aString"/>
    <xsl:param name="aLength"/>
    <xsl:variable name="30dots">
      <xsl:text>..............................</xsl:text>
    </xsl:variable>
    <xsl:for-each select="$aString">
      <xsl:value-of
       select="substring(concat(.,$30dots),1,$aLength)"/>
      <xsl:text>
</xsl:text>
    </xsl:for-each>
</xsl:template>

<xsl:variable name="padNames">
  <xsl:call-template name="RPadDots">
    <xsl:with-param name="aString"
      select="//contact/@name"/>
    <xsl:with-param name="aLength" select="30"/>
  </xsl:call-template>
</xsl:variable>

<xsl:template match='/'>
  <xsl:value-of select="$padNames"/>
</xsl:template>

</xsl:stylesheet>
