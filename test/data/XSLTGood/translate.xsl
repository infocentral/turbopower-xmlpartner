<?xml version = "1.0" encoding = "UTF-8"?>
<xsl:stylesheet version = "1.0" xmlns:xsl =
"http://www.w3.org/1999/XSL/Transform">
 <xsl:template match = "*">
  <xsl:element name = "result">
   <xsl:apply-templates select = "@width" mode = "multiply"/>
  </xsl:element>
 </xsl:template>
 <xsl:template match = "@*" mode = "multiply">
  <xsl:variable name = "convert" select = "translate(., '-0123456789.in',
'-0123456789.')"/>
  <xsl:value-of select = "$convert * 10"/>
 </xsl:template>
</xsl:stylesheet>