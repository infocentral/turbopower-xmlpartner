<xsl:transform
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 version="1.0"
>

<xsl:variable name="teams" select="//team[not(.=preceding::team)]"/>
<xsl:variable name="matches" select="//match"/>

<xsl:template match="results">
<html><body>
   <h1>Results of Group <xsl:value-of select="@group"/></h1>

   <table cellpadding="5">
      <tr>
        <td>Team</td>
        <td>Played</td>
        <td>Won</td>
        <td>Drawn</td>
        <td>Lost</td>
        <td>For</td>
        <td>Against</td>
     </tr>
   <xsl:for-each select="$teams">
        <xsl:variable name="this" select="."/>
        <xsl:variable name="test" select="//match[team=current()]/team/@score"/>
        <xsl:variable name="played" select="count($matches[team=$this])"/>
        <xsl:variable name="won"
            select="count($matches[team[.=$this]/@score &gt; team[.!=$this]/@score])"/>
        <xsl:variable name="lost"
            select="count($matches[team[.=$this]/@score &lt; team[.!=$this]/@score])"/>
        <xsl:variable name="drawn"
            select="count($matches[team[.=$this]/@score = team[.!=$this]/@score])"/>
        <xsl:variable name="for"
            select="sum($matches/team[.=current()]/@score)"/>
        <xsl:variable name="against"
            select="sum($matches[team=current()]/team/@score) - $for"/>

        <tr>
        <td><xsl:value-of select="."/></td>
        <td><xsl:value-of select="$played"/></td>
        <td><xsl:value-of select="$won"/></td>
        <td><xsl:value-of select="$drawn"/></td>
        <td><xsl:value-of select="$lost"/></td>
        <td><xsl:value-of select="$for"/></td>
        <td><xsl:value-of select="$against"/></td>
        </tr>
   </xsl:for-each>
   </table>
</body></html>
</xsl:template>

</xsl:transform>
