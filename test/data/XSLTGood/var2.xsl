<?xml version='1.0'?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0"
  xmlns:fo="http://www.w3.org/XSL/Format/1.0">


<xsl:template match="*">
  <xsl:variable name="namenode" select="@name"/>
  <xsl:choose>
    <xsl:when test="$namenode='sean' or (@name='scott')">Found a match: <xsl:value-of select='@name'/><xsl:text>&#10;</xsl:text>
    </xsl:when>
  </xsl:choose>
  <xsl:apply-templates/>
</xsl:template>

</xsl:stylesheet>