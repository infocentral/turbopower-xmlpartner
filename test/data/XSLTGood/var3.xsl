<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
version="1.0">
   <xsl:template match="/data">
      <data>
         <xsl:variable name="temp" select="." />

         <xsl:for-each select="one">
         	<el>
            <xsl:value-of select="$temp/@name" /><!--Here is the problem-->
    	    	<xsl:text> </xsl:text>
    	    	<xsl:value-of select="." />
            </el>
         </xsl:for-each>
      </data>
   </xsl:template>
</xsl:stylesheet>