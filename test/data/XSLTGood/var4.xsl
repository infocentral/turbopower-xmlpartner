<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/Document">

 <xsl:variable name="NofItemPerPage" select="10"/>
       <xsl:variable name="NofItemPerLine" select="4"/>

 <xsl:for-each select="Record[ (position()  mod 10) = 1 ]" >

  <xsl:text>First Element  : (</xsl:text>
  <xsl:value-of select="self::Record[1]/@Value"/><xsl:text> )</xsl:text><br/>

  <xsl:text>  Var Value  :</xsl:text>
  <xsl:value-of select="number($NofItemPerPage)"/><br/>

  <xsl:for-each select="(self::Record | following-sibling::Record[position()
&lt; 10 ])[position() mod 4 = 1] ">
  </xsl:for-each>
  <br/>
 </xsl:for-each>

</xsl:template>

</xsl:stylesheet>