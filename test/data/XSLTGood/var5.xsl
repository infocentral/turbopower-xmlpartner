<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/Document">
 <xsl:variable name="NofItemPerPage" select="10"/>
 <xsl:variable name="NofItemPerLine" select="4"/>


 <xsl:for-each select="Record[ (position()  mod number($NofItemPerPage)) =
1 ]" >
  <xsl:text>First Element  : (</xsl:text><xsl:value-of
select="self::Record[1]/@Value"/><xsl:text> )</xsl:text><br/>

  <xsl:variable name="PageNo" select="position() mod number($NofItemPerPage)"/>
  <xsl:text>Page No :</xsl:text>
  <xsl:value-of select="$PageNo"/><br/>

  <xsl:for-each select="(self::Record | following-sibling::Record[position()
&lt; number($NofItemPerPage) ])[position() mod number($NofItemPerLine) = 1]
">
   <xsl:value-of select="self::Record[1]/@Value"/>
   <xsl:text> - </xsl:text>
   <xsl:value-of select="following-sibling::Record[1]/@Value"/>
   <xsl:text> - </xsl:text>
   <xsl:value-of select="following-sibling::Record[2]/@Value"/>
   <xsl:text> - </xsl:text>
   <xsl:value-of select="following-sibling::Record[3]/@Value"/>
   <br/>
  </xsl:for-each>
  <br/>
 </xsl:for-each>

</xsl:template>

</xsl:stylesheet>