<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

  <!-- Zvon 18:4 -->

  <xsl:output method = "text" />

  <xsl:template match = "/" >
    <xsl:text >Text: </xsl:text>
    <xsl:apply-templates select = "//BBB" >
      <xsl:sort data-type = "text" />
    </xsl:apply-templates>
    <xsl:text >
Number: </xsl:text>
    <xsl:apply-templates select = "//BBB" >
      <xsl:sort data-type = "number" />
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match = "BBB" >
    <xsl:value-of select = "." />
    <xsl:text > </xsl:text>
  </xsl:template>
</xsl:stylesheet>