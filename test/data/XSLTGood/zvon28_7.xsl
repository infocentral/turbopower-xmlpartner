<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

  <!-- Zvon 28:7 -->

  <xsl:output method = "xml" indent = "yes" />

  <xsl:template match = "AAA" >
    <xsl:copy >
      <xsl:apply-templates />
    </xsl:copy>
  </xsl:template>

  <xsl:template match = "node()" >
    <xsl:copy >
      <xsl:apply-templates select = "node()" />
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>