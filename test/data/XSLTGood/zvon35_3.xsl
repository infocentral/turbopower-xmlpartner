<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <!-- Zvon 35:3 -->
  <xsl:include href="zvon33_3.xsl"/>
  <xsl:output method="text"/>
  <xsl:template match="/">
    <xsl:apply-templates select="//BBB"/>
  </xsl:template>
</xsl:stylesheet>