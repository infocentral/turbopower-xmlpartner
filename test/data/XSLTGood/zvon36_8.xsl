<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <!-- Zvon 36:8 -->
  <xsl:output method="text"/>
  <xsl:template match="BBB">
    <xsl:text>

BBB: </xsl:text>
    <xsl:number/>
    <xsl:apply-templates select="CCC"/>
  </xsl:template>
  <xsl:template match="CCC">
    <xsl:text>
CCC: </xsl:text>
    <xsl:number/>
  </xsl:template>
</xsl:stylesheet>