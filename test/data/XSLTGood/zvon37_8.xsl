<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >
<!-- Zvon 37:8 -->

<xsl:output method = "text" />

<xsl:template match = "/" >
  <xsl:apply-templates select = "//BBB" />
</xsl:template>

<xsl:template match = "BBB" >
  <xsl:text >

BBB: </xsl:text>
  <xsl:number value = "position()" format = "1...." />
  <xsl:apply-templates select = "CCC|DDD" />
</xsl:template>

<xsl:template match = "CCC" >
  <xsl:text >
  CCC: </xsl:text>
  <xsl:number value = "position()+10" format = "1" />
</xsl:template>

<xsl:template match = "DDD" >
  <xsl:text >
  DDD: </xsl:text>
  <xsl:number value = "position()+100" format = "00001" />
  <xsl:apply-templates select = "EEE" />
</xsl:template>

<xsl:template match = "EEE" >
  <xsl:text >
    EEE: </xsl:text>
  <xsl:number value = "position()+1000" format = "0000001" />
</xsl:template>

</xsl:stylesheet>