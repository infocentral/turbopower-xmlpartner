<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

<!-- Zvon 38:8 -->

<xsl:output method = "text" />

<xsl:template match = "/" >
  <xsl:apply-templates select = "//BBB" />
  <xsl:text >
</xsl:text>
  <xsl:apply-templates select = "//CCC" />
  <xsl:text >
</xsl:text>
  <xsl:apply-templates select = "//DDD" />
  <xsl:text >
</xsl:text>
  <xsl:apply-templates select = "//EEE" />
</xsl:template>

<xsl:template match = "BBB" >
  <xsl:text >
BBB: </xsl:text>
  <xsl:number value = "position()" format = "a -" />
</xsl:template>

<xsl:template match = "CCC" >
  <xsl:text >
CCC: </xsl:text>
  <xsl:number value = "position()+ 23" format = "A :::" />
</xsl:template>

<xsl:template match = "DDD" >
  <xsl:text >
DDD: </xsl:text>
  <xsl:number value = "position()" format = "i" />
</xsl:template>

<xsl:template match = "EEE" >
  <xsl:text >
EEE: </xsl:text>
  <xsl:number value = "position()+47" format = "I" />
</xsl:template>

</xsl:stylesheet>