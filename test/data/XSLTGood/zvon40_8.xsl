<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

<!-- Zvon 40:8 -->

<xsl:output method = "text" />

<xsl:template match = "/" >
  <xsl:apply-templates select = "//*" />
</xsl:template>

<xsl:template match = "*" >
  <xsl:number level = "multiple" count = "//*" format = "1.01.001.0001   " />
  <xsl:value-of select = "name()" />
  <xsl:text >
</xsl:text>
</xsl:template>

</xsl:stylesheet>