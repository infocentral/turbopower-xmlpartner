<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

<!-- Zvon 41:9 -->

<xsl:output method = "text" />

<xsl:template match = "/" >
  <xsl:apply-templates select = "//AAA" />
</xsl:template>

<xsl:template match = "*" >
  <xsl:number value = "1000000000 div (position())" grouping-separator = "|"
              grouping-size = "4" />
  <xsl:text >
</xsl:text>
</xsl:template>

</xsl:stylesheet>