<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

<!-- Zvon 44:2 -->

<xsl:output method = "text" />
<xsl:variable name = "a" >12</xsl:variable>

<xsl:template match = "/" >
  <xsl:variable name = "b" >23</xsl:variable>
  <xsl:value-of select = "concat($a,' + ',$b,' = ', $a+$b)" />
</xsl:template>

</xsl:stylesheet>