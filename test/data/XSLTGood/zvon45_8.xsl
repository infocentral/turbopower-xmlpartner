<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

<!-- Zvon 45:8 -->

<xsl:output method = "text" />

<xsl:variable name = "a" select = "//CCC" />

<xsl:variable name = "b" >
  <xsl:value-of select = "//CCC" />
</xsl:variable>

<xsl:template match = "/" >
  <xsl:apply-templates select = "$a" />
  <xsl:text >
#######################################
</xsl:text>
  <xsl:value-of select = "$b" />
</xsl:template>

<xsl:template match = "CCC" >
  <xsl:text >
CCC: </xsl:text>
  <xsl:value-of select = "." />
</xsl:template>

</xsl:stylesheet>