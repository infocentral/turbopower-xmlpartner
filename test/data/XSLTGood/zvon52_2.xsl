<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <!-- Zvon 52:2 -->
  <xsl:output method="text"/>
  <xsl:template match="/">
    <xsl:if test="true()">
      <xsl:value-of select="true()"/>
    </xsl:if>
    <xsl:text>
######################
</xsl:text>
    <xsl:if test="not(false())">
      <xsl:value-of select="false()"/>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>