<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

  <!-- Zvon 54:12 -->

  <xsl:output method = "text" />

  <xsl:key name = "ooo" match = "CCC"
            use = "parent::BBB/@bb" />

  <xsl:template match = "/" >
    <xsl:apply-templates select = "key('ooo','2')" />
  </xsl:template>

  <xsl:template match = "CCC" >
    <xsl:text >
</xsl:text>
    <xsl:value-of select = "." />
  </xsl:template>

</xsl:stylesheet>