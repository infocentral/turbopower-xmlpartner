<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >

<!-- Zvon 59:13 -->
<xsl:output method = "text" />

<xsl:template match = "/" >
  <xsl:apply-templates select = "//CCC" />
</xsl:template>

<xsl:template match = "CCC" >
  <xsl:text >
</xsl:text>
  <xsl:number value = "position()" format = "1......  " />
  <xsl:choose >
    <xsl:when test = "lang('eng')" >GB</xsl:when>
    <xsl:when test = "lang('cs')" >Czech Republic</xsl:when>
    <xsl:when test = "lang('de')" >Germany</xsl:when>
    <xsl:when test = "lang('sk')" >Slovakia</xsl:when>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>