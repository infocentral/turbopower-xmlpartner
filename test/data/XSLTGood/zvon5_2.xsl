<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

  <!-- Zvon 5:2 -->

  <xsl:output method = "xml" indent = "yes" />

  <xsl:attribute-set name = "set1" >
    <xsl:attribute name = "a" >1</xsl:attribute>
    <xsl:attribute name = "b" >2</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name = "set2" >
    <xsl:attribute name = "cc" >33</xsl:attribute>
    <xsl:attribute name = "dd" >44</xsl:attribute>
  </xsl:attribute-set>

  <xsl:template match = "/" >
    <xsl:element name = "QQQ" use-attribute-sets = "set1 set2" >
      <xsl:attribute name = "xxx" >555</xsl:attribute>
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>