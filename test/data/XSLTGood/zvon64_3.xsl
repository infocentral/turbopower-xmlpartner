<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

  <!-- Zvon 64:3 -->

  <xsl:template match = "*" >
    <xsl:element name = "{name()}" >
      <xsl:attribute name = "myID" >
	<xsl:value-of select = "generate-id()" />
      </xsl:attribute>
      <xsl:apply-templates />
    </xsl:element>
  </xsl:template>

</xsl:stylesheet>