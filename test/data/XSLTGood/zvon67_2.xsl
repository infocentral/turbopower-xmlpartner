<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

<!-- Zvon 67:2 -->

<xsl:output method = "text" />
<xsl:variable name = "A" >122.2640</xsl:variable>

<xsl:template match = "/" >
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number($A,'#.#####')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number($A,'#.##')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number($A,'#.#')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number($A,'#')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number($A,'######.######')" />
</xsl:template>

</xsl:stylesheet>