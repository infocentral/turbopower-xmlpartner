<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

<!-- Zvon 68:2 -->

<xsl:output method = "text" />
<xsl:variable name = "A" >122.2340</xsl:variable>

<xsl:template match = "/" >
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number($A,'0.00000')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number($A,'0.00')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number($A,'0')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number($A,'00000.000000')" />
</xsl:template>

</xsl:stylesheet>