<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

<!-- Zvon 69:2 pending -->

<xsl:output method = "text" />
<xsl:variable name = "A" >122234058.23454</xsl:variable>

<xsl:template match = "/" >
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number($A,'####,####')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number($A,'#,#')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number($A,'###,###.###')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "format-number($A,'000000000,000.000000')" />
</xsl:template>

</xsl:stylesheet>