<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

  <!-- Zvon 71:2 -->

  <xsl:output method = "text" />
  <xsl:variable name = "A" >0.2548</xsl:variable>

  <xsl:template match = "/" >
    <xsl:text >
</xsl:text>
    <xsl:value-of select = "format-number($A,'xxx  #.##')" />
    <xsl:text >
</xsl:text>
    <xsl:value-of select = "format-number($A,'value: %#.##')" />
    <xsl:text >
</xsl:text>
    <xsl:value-of select = "format-number($A,'xxx 000.000 yyy')" />
    <xsl:text >
</xsl:text>
    <xsl:value-of select = "format-number($A,'#.###  zzz')" />
  </xsl:template>
</xsl:stylesheet>