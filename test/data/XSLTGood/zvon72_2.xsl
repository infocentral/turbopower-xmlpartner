<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

  <!-- Zvon 72:2 -->

  <xsl:output method = "text" />

  <xsl:variable name = "A" >-12.48</xsl:variable>

  <xsl:template match = "/" >
    <xsl:text >
</xsl:text>
    <xsl:value-of select = "format-number($A,'#.##')" />
    <xsl:text >
</xsl:text>
    <xsl:value-of select = "format-number($A,'%#.##')" />
    <xsl:text >
</xsl:text>
    <xsl:value-of select = "format-number($A,'000.000')" />
    <xsl:text >
</xsl:text>
    <xsl:value-of select = "format-number($A,'0.0')" />
  </xsl:template>
</xsl:stylesheet>