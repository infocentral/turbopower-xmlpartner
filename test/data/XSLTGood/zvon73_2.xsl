<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" xmlns:alias = "alias-namespace" version = "1.0" >

  <!-- Zvon 73:2 -->

  <xsl:output method = "xml" indent = "yes" />

  <xsl:namespace-alias stylesheet-prefix = "alias" result-prefix = "xsl" />

  <xsl:template match = "/" >
    <alias:stylesheet version = "1.0" >
      <alias:template match = "/" >
      <alias:text >Hello, world!</alias:text>
      </alias:template>
    </alias:stylesheet>
  </xsl:template>
</xsl:stylesheet>