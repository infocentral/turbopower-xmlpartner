<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

<!-- Zvon 77:2 -->

<xsl:output method = "text" />
<xsl:variable name = "A" >http://zvon.org</xsl:variable>

<xsl:template match = "/" >
  <xsl:text >
substring-before zvon. : </xsl:text>
  <xsl:value-of select = "substring-before($A,'zvon.')" />
  <xsl:text >
substring-after zvon. : </xsl:text>
  <xsl:value-of select = "substring-after($A,'zvon.')" />
</xsl:template>

</xsl:stylesheet>