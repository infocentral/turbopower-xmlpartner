<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<!-- Zvon 83:18 -->
<xsl:output method = "text" />

<xsl:template match = "/" >
  <xsl:apply-templates select = "//BBB" />
  <xsl:apply-templates select = "//CCC" />
</xsl:template>

<xsl:template match = "BBB | CCC" >
  <xsl:if test = "position()=1" >
    <xsl:text >
    </xsl:text>
    <xsl:value-of select = "name()" />
    <xsl:text > : </xsl:text>
  </xsl:if>
  <xsl:value-of select = "." />
  <xsl:if test = "not(position()=last())" >
    <xsl:text >, </xsl:text>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>