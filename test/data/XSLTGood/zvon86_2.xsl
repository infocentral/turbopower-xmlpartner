<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

<!-- Zvon 86:2 -->

<xsl:output method = "text" />
<xsl:variable name = "A" >http://zvon.org</xsl:variable>

<xsl:template match = "/" >
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "starts-with($A,'http')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "starts-with($A,'zvon')" />
  <xsl:text >
</xsl:text>
  <xsl:value-of select = "starts-with($A,'HTTP')" />
</xsl:template>

</xsl:stylesheet>