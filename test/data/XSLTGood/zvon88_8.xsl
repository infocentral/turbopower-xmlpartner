<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >
<xsl:output method = "text" />

<!-- Zvon 88:8 -->
<xsl:template match = "/" >
  <xsl:text >
  </xsl:text>
  <xsl:apply-templates select = "//CCC[.='c21']/ancestor-or-self::*" mode = "print" />
  <xsl:text >
  </xsl:text>
  <xsl:apply-templates select = "//DDD[1]/EEE[1]/ancestor-or-self::*" mode = "print" />
</xsl:template>

<xsl:template match = "*" mode = "print" >
  <xsl:value-of select = "concat(name(),'{',position(),'}')" />
  <xsl:if test = "not(position()=last())" >
    <xsl:text > - </xsl:text>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>