<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" >

<!-- Zvon 90:8 -->
<xsl:output method = "text" />

<xsl:template match = "/" >
  <xsl:text >
  </xsl:text>
  <xsl:apply-templates select = "//BBB[1]/child::*" mode = "print" />
  <xsl:text >
  </xsl:text>
  <xsl:apply-templates select = "//BBB[3]/child::*" mode = "print" />
</xsl:template>

<xsl:template match = "*" mode = "print" >
  <xsl:value-of select = "concat(name(),'{',position(),'}')" />
  <xsl:if test = "not(position()=last())" >
    <xsl:text > - </xsl:text>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>