<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/XSL/Transform/1.0" xmlns="http://www.w3.org/TR/REC-html40" result-ns="">
  <xsl:attribute-set name="tablerow">
    <xsl:attribute name="bgcolor">red</xsl:attribute>
  </xsl:attribute-set>
  <xsl:template match="data">
    <table>
      <xsl:apply-templates/>
    </table>
  </xsl:template>
  <xsl:template match="row">
    <tr xsl:use-attribute-sets="tablerow">
      <xsl:apply-templates/>
    </tr>
  </xsl:template>
  <xsl:template match="column">
    <xsl:element name="td" use-attribute-sets="tablerow">
      <xsl:text>cell # </xsl:text>
      <xsl:value-of select="position()"/><xsl:text> = </xsl:text>
      <xsl:value-of select="."/>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>