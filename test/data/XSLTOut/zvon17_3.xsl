<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

  <!-- Zvon 17:3 -->

  <xsl:output method = "text" />

  <xsl:template match = "/" >
    <xsl:text >Ascending: </xsl:text>
    <xsl:apply-templates select = "//BBB" >
      <xsl:sort />
    </xsl:apply-templates>
    <xsl:text >
Descending: </xsl:text>
    <xsl:apply-templates select = "//BBB" >
      <xsl:sort order = "descending" />
    </xsl:apply-templates>
    <xsl:text >
Lower-first: </xsl:text>
    <xsl:apply-templates select = "//BBB" >
      <xsl:sort case-order = "lower-first" />
    </xsl:apply-templates>
    <xsl:text >
Upper-first: </xsl:text>
    <xsl:apply-templates select = "//BBB" >
      <xsl:sort case-order = "upper-first" />
    </xsl:apply-templates>
  </xsl:template>

  <xsl:template match = "BBB" >
    <xsl:value-of select = "." />
    <xsl:text > </xsl:text>
  </xsl:template>
</xsl:stylesheet>