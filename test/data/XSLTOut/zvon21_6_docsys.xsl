<xsl:stylesheet xmlns:xsl = "http://www.w3.org/1999/XSL/Transform" version = "1.0" >

  <!-- Zvon 21:6, modified to emit document type system ID -->

  <xsl:output method = "xml" indent = "yes" doctype-system="system.dtd"/>

  <xsl:template match = "/" >
    <AAA >
      <QQQ >
      <xsl:attribute name = "nnn:qqq" namespace = "http://zvon.org/xslt" >111</xsl:attribute>
      </QQQ>
      <xsl:element name = "{//el}" >
      <xsl:attribute name = "{//attr}" namespace = "{//ns}" >
        <xsl:value-of select = "//val" />
      </xsl:attribute>
      </xsl:element>
    </AAA>
  </xsl:template>
</xsl:stylesheet>