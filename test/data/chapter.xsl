<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version='1.0'>

<xsl:template match="doc">
  <fo:root>
    <fo:layout-master-set>
      <fo:simple-page-master master-name="page"
        page-height="297mm" page-width="210mm"
        margin-top="20mm" margin-bottom="10mm"
        margin-left="25mm" margin-right="25mm">
        <fo:region-body
          margin-top="0mm" margin-bottom="15mm"
          margin-left="0mm" margin-right="0mm"/>
        <fo:region-after extent="10mm"/>
      </fo:simple-page-master>
    </fo:layout-master-set>
    <fo:page-sequence master-name="page">
      <fo:static-content flow-name="xsl-region-after">
        <fo:block>
          <xsl:text>Page </xsl:text>
          <fo:page-number/>
        </fo:block>
      </fo:static-content>
      <fo:flow flow-name="xsl-region-body">
        <xsl:apply-templates/>
      </fo:flow>
    </fo:page-sequence>
  </fo:root>
</xsl:template>

<xsl:template match="chapter/title">
  <fo:block id="{generate-id(.)}">
    <xsl:number level="multiple" count="chapter" format="1. "/>
    <xsl:apply-templates/>
  </fo:block>
</xsl:template>

<xsl:template match="p">
  <fo:block>
    <xsl:apply-templates/>
  </fo:block>
</xsl:template>

<xsl:template match="ref">
  <xsl:text>page </xsl:text>
  <fo:page-number-citation refid="{generate-id(id(@refid)/title)}"/>
</xsl:template>

</xsl:stylesheet>






