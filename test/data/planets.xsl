<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    version='1.0'>

    <xsl:template match="PLANETS">
        <fo:root>

            <fo:layout-master-set>
                 <fo:simple-page-master master-name="page"
                     page-height="400mm" page-width="300mm"
                     margin-top="10mm" margin-bottom="10mm"
                     margin-left="20mm" margin-right="20mm">

                     <fo:region-body
                       margin-top="0mm" margin-bottom="10mm"
                       margin-left="0mm" margin-right="0mm"/>

                     <fo:region-after extent="10mm"/>
                 </fo:simple-page-master>
             </fo:layout-master-set>

             <fo:page-sequence master-name="page">

                 <!-- Added for fop -->
                 <fo:sequence-specification>
                     <fo:sequence-specifier-single master-name="page"/>
                 </fo:sequence-specification>
                 <!-- Added for fop -->

                 <fo:flow>
                     <xsl:apply-templates/>
                 </fo:flow>
             </fo:page-sequence>

        </fo:root>
    </xsl:template>

    <xsl:template match="PLANET/NAME">
        <fo:block font-weight="bold" font-size="36pt" 
            line-height="48pt" font-family="sans-serif">
            Name: 
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="PLANET/MASS">
        <fo:block font-size="36pt" line-height="48pt" 
            font-family="sans-serif">
            Mass (Earth = 1): 
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="PLANET/DAY">
        <fo:block font-size="36pt" line-height="48pt" font-family="sans-serif">
            Day (Earth = 1): 
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="PLANET/RADIUS">
        <fo:block font-size="36pt" line-height="48pt" font-family="sans-serif">
            Radius (in miles): 
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="PLANET/DENSITY">
        <fo:block font-size="36pt" line-height="48pt" font-family="sans-serif">
            Density (Earth = 1): 
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="PLANET/DISTANCE">
        <fo:block font-size="36pt" line-height="48pt" font-family="sans-serif">
            Distance (million miles): 
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

</xsl:stylesheet>